<?php
/* @var $this GroupController */
/* @var $data Group */

if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}

?>

    <div class="center">
        <h1 class="page-title sharp align-center">Фотографии группы <? echo $model->name; ?></h1>
        <? if ($user_id!=0) { ?>
        <form method="post" action="<? echo Yii::app()->createUrl('site/index').'/photo/upload'; ?>" id="photo-form" enctype="multipart/form-data">
            <div style="display:none"><input type="hidden" name="YII_CSRF_TOKEN" value="<? echo Yii::app()->request->csrfToken;?>"></div>
                Добавить <input type="hidden" name="Photo[link]" value="" id="ytPhoto_link"><input class="search input-border" type="file" id="Photo_link" name="Photo[link]">
                <select name="Photo[album_id]"
                    <option><? echo Yii::t('var', 'Выберите альбом');?></option>
                    <?
                    $dataReader = Photoalbum::model()->getGroupAlbums($model->id);
                    foreach ($dataReader as $row) {
                        echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                    }
                    ?>
                <input type="hidden" id="Photo_user_id" name="Photo[user_id]" value="<? echo $user_id;?>">
                <input type="hidden" id="Photo_date" name="Photo[date]" value="<? echo date('Y-m-d');?>">
                <button class="search-btn input-border" name="search-btn" type="submit" style="background: none repeat scroll 0 0 #3B9FE8;height: 29px;text-transform: uppercase;width: 100px;border: 1px solid #D5D2D2;border-radius: 4px 4px 4px 4px;">
                    <span class="white">Загрузить</span>
                </button>
        </form>
        <? } ?>
        <? $i = 1;
        $dataReader = Photoalbum::model()->getGroupAlbums($model->id);
        foreach($dataReader as $row) {
            ?>
            <div class="photo-container f-left<? if ($i>3) echo ' hide'; ?>">
                <a href="<? echo Yii::app()->createUrl('site/index').'/photoalbum/'.$row['id']; ?>"><img src="/images/group/no-pic.png" alt="photo-group" /></a>
                <div class="photo-info align-center"><span class="photo-title">Встреча1</span><br /><span class="photo-date">10.05.13</span></div>
            </div>
            <?
            $i++;
        } ?>
        <a class="show-all align-center" title="Показать все фотоальбомы группы" href="#" onclick="showAllAlbums()"><? echo Yii::t('var', 'показать все альбомы');?></a>

        <h2 class="latest-photos-title"><? echo Yii::t('var', 'Последние фото со встреч');?>:</h2>
        <div class="latest-photos">
            <?
            $dataReader = Photoalbum::model()->getGroupPhotos($model->id);
            foreach($dataReader as $row) {
                echo '<a class="fancybox" href="/images/album/'.$row['album_id'].'/'.$row['link'].'"><img src="/images/album/'.$row['album_id'].'/'.$row['link'].'" alt="photo"></a>';
            } ?>
        </div>

    </div>

<script>
    $(document).ready(function() {

        $("#fileupload").validate({
            rules: {
                files: "required",
                album_id: "required"
            },
            messages: {
                files: "Обязательное поле",
                album_id: "Обязательное поле"
            },
            submitHandler: function(form) {
                $.ajax({
                    type: 'GET',
                    url: '<? echo Yii::app()->createUrl('group/photos').'/'.$model->id; ?>',
                    data: $('#fileupload').serialize(),
                    success: function(data){
                        window.location.reload();
                    }
                });
            }
        });


        $("img.photo:nth-child(2n)").css({'display':'none'});

    });
</script>