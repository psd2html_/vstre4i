<?php
/* @var $this GroupController */
/* @var $model Group */

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".$model->background." !important; }</style>";

$this->breadcrumbs=array(
	'Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
	array('label'=>'Update Group', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Group', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
if (isset(Yii::app()->user->service)) {
    $user_id = User::model()->getId(Yii::app()->user->id,Yii::app()->user->service);
} else {
    $user_id = Yii::app()->user->id;
}
if (!Yii::app()->user->id) {
    $user_id = 0;
}
$isgroupadmin = Group::model()->isGroupAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) $picture = $model->picture; else $picture = '/images/group/no-pic.png';
                ?>
                <img src="<? echo $picture; ?>" style="width:185.5px;height:192px;">
            </div>
            <? if ($isgroupadmin == 1) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.$model->id ?>'" title="настройки" class="complain align-center">Настройки</a>
            <? }else { ?>
                <? if ($user_id!=0) { ?>
                    <a mid="<? echo $model->id ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на группу" class="complain align-center">пожаловаться на группу</a>
                <? } ?>
            <? } ?>

        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><? echo City::model()->getCity($model->city_id).', '.Country::model()->getCountryByCityID($model->city_id);?></li>
                    <li>Группа создана: <? echo date('d.m.Y',strtotime($model->date_created));?></li>
                    <li>Участники: <? echo Member::model()->getGroupMembersCount($model->id);?></li>
                    <li>Будущие события: <? echo Group::model()->getFutureMeetsCount($model->id);?></li>
                    <li>Прошедшие события: <? echo Group::model()->getPastMeetsCount($model->id);?></li>
                    <li><span style="float:left;display:block;">Календарь группы: </span><span class="calend-show"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-show').on('click',function(){
                                $('.calend').show();
                            })
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <ul>
                        <? $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
                        foreach($dataReader as $row) {
                            echo '<li><a href="#" title="#">'.$row['name'].'</a></li>';
                            $i++;
                        } ?>
                    </ul>
                </div>
                <div class="organizers">Организаторы:
                    <ul>
                        <?
                        $dataReader = Member::model()->getModeratorsList($model->id);
                        foreach($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.$row['user_id'].'">'.$row['name'].'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <select id="country-select">
            <option value="choose">Свяжитесь с нами</option>
            <option value="all">(057) 025 25 25</option>
            <option value="program">skype: vstre4i</option>
            <option value="education">e-mail: vstre4i@mail.ru</option>
        </select>
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center"><?php echo $model->name; if ($isgroupadmin == 1) echo ' (вы - администратор)'; ?></h2>
            <div class="content-block">
                <p class="group-description"><?php echo $model->description;?></p>
                <a href="#" title="читать далее" class="f-right read-more">читать далее</a>
                <script>
                    $(document).ready(function(){
                        var text = $('p.group-description').html();
                        if (text.length < 100) $('a.read-more').remove();
                    });
                </script>
            </div>
        </div>
        <? if ($user_id!=0) { ?>
        <div class="content-block-wrap">
            <div class="content-block">
                <div class="slider align-center">
                    <h4 class="align-center">Ваши друзья с Facebook/Vkontakte в данной группе</h4>
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">

                            <?
                            if (User::model()->getUserServiceID(User::model()->getUserId())>0 && User::model()->getUserService(User::model()->getUserId())=='facebook') {
                                require 'facebook/src/facebook.php';

                                $facebook = new Facebook(array(
                                    'appId' => '303928756406490',
                                    'secret' => '8caedbb43189a170b926058cd3adb6bc',
                                    'cookie' => true,
                                ));

                                $friends = $facebook->api('/'.User::model()->getUserServiceID(User::model()->getUserId()).'/friends');
                                $dataReader = Member::model()->getGroupMembersFacebook($model->id);
                                $members = array();
                                foreach ($dataReader as $val) {
                                    $members[] = $val['id'];
                                }
                                $ii = 0;
                                for ($i=0;$i<count($friends);$i++) {
                                    if (in_array($friends['data'][$i]['id'],$members)) {
                                        $pic = $facebook->api('https://graph.facebook.com/'.$friends['data'][$i]['id'].'/picture');

                                        if ($ii==0) {
                                            echo '<div class="item active">';
                                        }
                                        echo '<a class="group-participant" href="'.Yii::app()->createUrl('site/index').'/user/'.User::model()->getId($friends['data'][$i]['id'],'facebook').'"><img src="'.$pic['id'].'" alt="participant" /><h5>'.$friends['data'][$i]['name'].'</h5><div class="meetup">No Meetup<br />Groups</div></a>';
                                        if ($ii%3 == 0) {
                                            echo '</div><div class="item">';
                                        }
                                        $ii++;
                                    }
                                }
                                echo '</div>';
                            }

                            ?>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left prev" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right next" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                <div id="photo"></div>
                <script src="http://vkontakte.ru/js/api/openapi.js" type="text/javascript"></script>
                <script>
                    VK.init({
                        apiId: 3305436
                    });

                    function authInfo(response) {
                        var i,id,ind;
                        var uids = new  Array();
                        var users = new  Array();

                            <?
                            $group_users_vk = array();
                            $group_users = array();
                            $dataReader = Member::model()->getGroupMembersVK($model->id);
                            foreach ($dataReader as $val) {
                                $group_users_vk[] = $val['serv_id'];
                                $group_users[] = $val['id'];

                            } ?>
                        var group_users_vk = [
                            <?
                            echo implode(',',$group_users_vk);
                            ?>
                        ];
                        var group_users = [
                            <?
                            echo implode(',',$group_users);
                            ?>
                        ];

                        if (response.session) {
                            VK.api('friends.get',{uid:5976499,fields:"uid, first_name, last_name, nickname, sex, photo_medium"},function(data) {
                                if (data.response) {
                                    for (i=0;i<data.response.length;i++) {
                                        ind = $.inArray(data.response[i].uid, group_users_vk);
                                        if (ind != -1) {
                                            $(".carousel-inner > .active").append('<a class="group-participant" href="<? echo Yii::app()->createUrl('site/index'); ?>/user/'+group_users[ind]+'"><img src="'+data.response[i].photo_medium+'" alt="participant" /><h5>'+data.response[i].first_name+' '+data.response[i].last_name+'</h5><div class="meetup">No Meetup<br />Groups</div></a>');
                                        }
                                    }
                                }
                            });
                        }
                    }

                    VK.Auth.getLoginStatus(authInfo);

                </script>
            </div>
        </div>
        <div class="content-block-wrap">
            <div class="content-block">
                <a id="to-wall" style="cursor:pointer;">Стена группы</a>
                <script>
                    $(document).ready(function(){
                        $('#to-wall').on('click',function(){
                            $('html, body').animate({
                                scrollTop: $(".wall").offset().top
                            }, 1000);
                        });
                    });
                </script>
            </div>
        </div>
        <? } ?>
        <div class="content-block-wrap calend">
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Календарь');?></h2>
            <div class="content-block">
            <?

                Yii::import('application.extensions.calendar.classes.calendar', true);

                $month = isset($_GET['m']) ? $_GET['m'] : NULL;
                $year  = isset($_GET['y']) ? $_GET['y'] : NULL;

                $calendar = Calendar::factory($month, $year);

                $events = array();

                $dataReader = Group::model()->getGroupMeetsID($model->id);
                foreach ($dataReader as $val) {
                    $events[] = $calendar->event()
                        ->condition('timestamp', strtotime($val['date_start']))
                        ->title('Hello All')
                        ->output('<a href="'.Yii::app()->createUrl('site/index').'/group/meet/'.$model->id.'/?mid='.$val['id'].'">'.date('H:i',strtotime($val['date_start'])).'<br>'.$val['name'].'</a>');
                }

                $calendar->standard('today')->standard('prev-next');

                foreach ($events as $val) {
                    $calendar->attach($val);
                }
            ?>
                <table class="calendar">
                    <thead>
                    <tr class="navigation">
                        <th class="prev-month"><a href="<?php echo htmlspecialchars($calendar->prev_month_url()) ?>"><?php echo $calendar->prev_month() ?></a></th>
                        <th colspan="5" class="current-month"><?php echo $calendar->month() ?> <?php echo $calendar->year ?></th>
                        <th class="next-month"><a href="<?php echo htmlspecialchars($calendar->next_month_url()) ?>"><?php echo $calendar->next_month() ?></a></th>
                    </tr>
                    <tr class="weekdays">
                        <?php foreach ($calendar->days() as $day): ?>
                            <th><?php echo $day ?></th>
                        <?php endforeach ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($calendar->weeks() as $week): ?>
                        <tr>
                            <?php foreach ($week as $day): ?>
                                <?php
                                list($number, $current, $data) = $day;

                                $classes = array();
                                $output  = '';

                                if (is_array($data))
                                {
                                    $classes = $data['classes'];
                                    $title   = $data['title'];
                                    $output  = empty($data['output']) ? '' : '<ul class="output"><li>'.implode('</li><li>', $data['output']).'</li></ul>';
                                }
                                ?>
                                <td class="day <?php echo implode(' ', $classes) ?>">
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                    <div class="day-content">
                                        <?php echo $output ?>
                                    </div>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <style>
                    .calendar {width:100%; border-collapse:collapse;}
                    .calendar tr.navigation th {padding-bottom:20px;}
                    .calendar th.prev-month {text-align:left;}
                    .calendar th.current-month {text-align:center; font-size:1.5em;}
                    .calendar th.next-month {text-align:right;}
                    .calendar tr.weekdays th {text-align:left;}
                    .calendar td {width:14%; height:100px; vertical-align:top; border:1px solid #CCC;}
                    .calendar td.today {background:#FFD;}
                    .calendar td.prev-next {background:#EEE;}
                    .calendar td.prev-next span.date {color:#9C9C9C;}
                    .calendar td.holiday {background:#DDFFDE;}
                    .calendar span.date {display:block; padding:4px; line-height:12px; background:#EEE;}
                    .calendar div.day-content {}
                    .calendar ul.output {margin:0; padding:0 4px; list-style:none;}
                    .calendar ul.output li {margin:0; padding:5px 0; line-height:1em; border-bottom:1px solid #CCC;}
                    .calendar ul.output li:last-child {border:0;}

                        /* Small Calendar */
                    .calendar.small {width:auto; border-collapse:separate;}
                    .calendar.small tr.navigation th {padding-bottom:5px;}
                    .calendar.small tr.navigation th a {font-size:1.5em;}
                    .calendar.small th.current-month {font-size:1em;}
                    .calendar.small tr.weekdays th {text-align:center;}
                    .calendar.small td {width:auto; height:auto; padding:4px 8px; text-align:center; border:0; background:#EEE;}
                    .calendar.small span.date {display:inline; padding:0; background:none;}
                </style>
            </div>
        </div>
        <div class="content-block-wrap">
            <h2 class="sharp align-center">Встречи</h2>
            <? if ($user_id!=0) { ?>
            <button type="button" name="add-event" class="add-event" onclick="$('#meet-proposal').arcticmodal();"><span>Предложить событие</span></button>
            <? } ?>

            <?
            $criteria = new CDbCriteria(array(
                'condition'=>'`type`="meetup"'
            ));
            $dataProvider=new CActiveDataProvider('Group',array('criteria'=>$criteria));
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'application.views.group._meets',
                'template'=>'{items}',
                'itemsCssClass'=>'clearfix port-det port-thumb'
            ));
            ?>

        </div>
        <? if ($user_id!=0) { ?>
        <div class="content-block-wrap wall">
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Стена');?></h2>
            <div class="content-block">
                <label for="comment" class="f-left sharp">Комментарии:</label>
                <div class="clear"></div>
                <?
                $dataReader = Group::model()->getMeetComments($model->id);

                foreach($dataReader as $row) {
                    echo "<div>".$row['name'].': '.$row['text']."</div>";
                }
                ?>
                <form id="comments-form" onsubmit="return false;">
                    <textarea id="comment" class="f-left" name="text"></textarea>
                    <input onclick="sentComment(<? echo $model->id.','.$user_id; ?>)" type="image" src="/images/send-comment.png" name="send-comment" class="f-right"/>
                </form>
            </div>
        </div>
        <? } ?>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
        <div class="sidebar-right" style="background: none;">
            <? if ($user_id!=0) { ?>
                <? if ($ismember == Null) { ?>
                    <button onclick="$('#exampleModal').arcticmodal();" type="button" name="enter-group" class="align-center white enter-group"><span>Вступить в группу</span></button>
                <? } else if ($ismember == 1) { ?>
                    <button onclick="leaveGroup(<? echo User::model()->getUserId().', '.$model->id;?>)" type="button" name="enter-group" class="align-center white enter-group"><span>Покинуть группу</span></button>
                <? } else { ?>
                    <img src="/images/expected.png" class="align-center white enter-group">
                <? } ?>
            <? } ?>
            <button type="button" name="invite" class="align-center white enter-group invite" onclick="$('#mailto').arcticmodal();"><span>Пригласить друга</span></button>
        </div>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp">Оплаченные группы</h2>
            <?
            $dataReader = Group::model()->getRandomPaidGroup($model->id);
            if (count($dataReader)>0)
                foreach($dataReader as $row) {
                    echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.$row['id'].'"><img src="'.(($row['picture']!=''&&$row['picture']!=NULL)?('/images/group/no-pic.pg'):('/images/group/no-pic.png')).'">'.$row['name'].'</a>';
                }
            else echo 'Проплаченные группы не найдены';
            ?>
            <div class="sidebar-right-content"></div>
        </div>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;">Участники</h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = '.$model->id.' AND confirm=1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
                <a href="<? echo Yii::app()->createUrl('site/index').'/group/members/'.$model->id; ?>" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть всех</a>
            </div>
        </div>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp">Фотографии группы</h2>
            <div class="sidebar-right-content sidebar-right-content-photo-group">
                <?
                $dataReader = Photoalbum::model()->getRandomPhoto($model->id);
                echo '<img src="/images/album/'.$dataReader['album_id'].'/'.$dataReader['link'].'" style="width:150px;height:150px;">';
                ?>
            </div>
            <a href="<? echo Yii::app()->createUrl('site/index').'/group/photos/'.$model->id; ?>" title="просмотреть все фотографии группы" class="show-all-participants align-center" style="width: 160px; height: auto;">просмотреть все фотографии группы</a>
        </div>
    </aside>
</div>





<script>
    function joinGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/joinGroup');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/leaveGroup');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<!--  <p>Ваши друзья из социальных сетей</p>
        --><?/*
        include_once 'facebook/facebook.php';
        $facebook = new Facebook(array(
            'appId' => '591018247595529',
            'secret' => '96f74c40b923be3eb8536699065f89d7',
            'cookie' => true
        ));

        $access_token = $facebook->getAccessToken();
        $friends = $facebook->api('/me/friends?token='.$access_token);*/

/*$session = $facebook->getSession();
if ($session) {
    $uid = $facebook->getUser();
    $me = $facebook->api('/me');
}
print_r($me);*/
?>


<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
            <?
            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
            foreach($dataReader as $row) {
                echo "<tr><td>".$row['name']."</td><td><input type='text' value='' name='fields[".$row['id']."]'></td></tr>";
            }
            ?>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
        </form>
        <script src="http://jquery.bassistance.de/validate/jquery.validate.js"></script>
        <script>
            $().ready(function() {

            $("#addit-fields-form").validate({
            rules: {
                <?
                    $i = 0;
                    $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                    foreach($dataReader as $row) {
                        if ($row['required']==1) {
                            echo (($i>0)?',':'').$row['id'].': "required"';
                            $i++;
                        }
                     }
                ?>
            },
            messages: {
                <?
                        $i = 0;
                        $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                        foreach($dataReader as $row) {
                            if ($row['required']==1) {
                                echo (($i>0)?',':'').$row['id'].': "Обязательное поле"';
                                $i++;
                            }
                         }
                    ?>
            },
                submitHandler: function(form) {
                    $.ajax({
                        type: 'GET',
                        url: '<? echo Yii::app()->createUrl('member/sendRequest'); ?>',
                        data: $('#addit-fields-form').serialize()+'&group_id=<? echo $model->id; ?>&user_id=<? echo $user_id; ?>',
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr><td>Введите текст жалобы</td><td><input type="text" name="text" value=""></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="mid" value="">
            <input type="hidden" name="text_id" value="meet">
        </form>

        <script>
            $(document).ready(function() {

                $('.complain-event').on('click',function(){
                    $('input[name=mid]').val($(this).attr('mid'));
                });

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&text_id=&user_id=<? echo $user_id; ?>'+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;">Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="meet-proposal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="meet-proposal-form">
            <table>
                <tr><td>Введите место</td><td><input type="text" name="place" value=""></td></tr>
                <tr><td>Введите дату</td><td><input type="text" name="date" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="comment" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<? echo $user_id; ?>">
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script>
            $().ready(function() {

                $("#meet-proposal-form").validate({
                    rules: {
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        place: "Обязательное поле",
                        date: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('group/meetproposal'); ?>',
                            data: $('#meet-proposal-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
<script>
    function sentComment(meet_id,user_id) {
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/sentComment');?>',
            data: $('#comments-form').serialize() + "&user_id=" + user_id + "&meet_id=" + meet_id + "&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>