<?php
/* @var $this GroupKeywordController */
/* @var $model GroupKeyword */

$this->breadcrumbs=array(
	'Group Keywords'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GroupKeyword', 'url'=>array('index')),
	array('label'=>'Create GroupKeyword', 'url'=>array('create')),
	array('label'=>'View GroupKeyword', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GroupKeyword', 'url'=>array('admin')),
);
?>

<h1>Update GroupKeyword <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>