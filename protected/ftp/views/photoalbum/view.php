<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */
?>

<div class="center">
    <h1 class="page-title">Альбом <? echo $model->name; ?></h1>
    <?
    $dataReader = Photoalbum::model()->getAlbumPhotos($model->id);
    foreach($dataReader as $row) {
        echo '<a class="fancybox" href="/images/album/'.$row['album_id'].'/'.$row['link'].'"><img src="/images/album/'.$row['album_id'].'/'.$row['link'].'" style="width:128px;height:128px;float:left;"></a>';
    }
    ?>
</div>

<script>
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>