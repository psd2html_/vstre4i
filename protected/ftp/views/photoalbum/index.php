<?php
/* @var $this PhotoalbumController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Photoalbums',
);

$this->menu=array(
	array('label'=>'Create Photoalbum', 'url'=>array('create')),
	array('label'=>'Manage Photoalbum', 'url'=>array('admin')),
);
?>

<h1>Photoalbums</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
