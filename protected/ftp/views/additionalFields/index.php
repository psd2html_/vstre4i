<?php
/* @var $this AdditionalFieldsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Additional Fields',
);

$this->menu=array(
	array('label'=>'Create AdditionalFields', 'url'=>array('create')),
	array('label'=>'Manage AdditionalFields', 'url'=>array('admin')),
);
?>

<h1>Additional Fields</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
