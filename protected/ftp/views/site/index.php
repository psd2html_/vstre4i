<?php
/* @var $this SiteController */

//$this->pageTitle=Yii::app()->name;
?>

<div class="slider-container">
    <div class="center">
        <img src="/images/sliders.jpg" alt="slider" />
        <div class="transparent-block input-border">
            <h1 class="transparent-block-info f-right">Vstre4i - <? echo Yii::t('var', 'лучший сайт');?></h1>
        </div>
    </div>
</div>
<div class="center<? if (isset($_GET['view'])) if ($_GET['view'] == 'tab') echo ' index-grid'; ?>">
    <form id="search-form" action="" method="post" onsubmit="return false;">
        <div class="search-field-content">
            <h2 class="search-field-title f-left sharp"><? echo Yii::t('var', 'Поиск');?></h2>

            <div class="filter f-left" style="margin: 5px 16px 0 344px;">
                <h3 class="filter-title f-left"><? echo Yii::t('var', 'режим просмотра');?></h3>
                <div class="filter-view-list f-left" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/?view=tab'; ?>'" style="cursor:pointer"></div>
                <div class="filter-view-grid f-left" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/?view=list'; ?>'" style="cursor:pointer"></div>
            </div>

            <div class="filter f-left">
                <h3 class="filter-title f-left"><? echo Yii::t('var', 'сортировать по');?></h3>
                <div class="filter-sort f-left"></div>
                <div class="filter-div">
                    <ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content ui-corner-all sort">
                        <li class="ui-menu-item" sort="date_up">Дата создания (по возврастанию)</li>
                        <li class="ui-menu-item" sort="date_down">Дата создания (по убыванию)</li>
                        <li class="ui-menu-item" sort="next_meet">Ближайшая встреча</li>
                        <li class="ui-menu-item" sort="members">Количество участников в группе</li>
                        <li class="ui-menu-item" sort="meets">Общее количество встреч</li>
                    </ul>
                </div>
                <style>
                    .filter-div {
                        display: none;
                        margin-left: -33px;
                        margin-top: 20px;
                        position: absolute;
                        width: 295px !important;
                        z-index: 999;
                    }
                    .filter-sort,.filter-div li {
                        cursor: pointer;
                    }
                    .filter-div .ui-menu-item {
                        width: 232px;
                        margin: 5px !important;
                        padding: 5px !important;
                    }
                    .filter-div .ui-menu-item:hover {
                        background: #D1E5F1 !important;
                    }
                </style>
            </div>

            <div class="clear"></div>

            <input name="keyword" type="text" class="select-input f-left" placeholder="<? echo Yii::t('var', 'Впишите слово');?>" value="<? if (isset($_POST['search'])) echo $_POST['search']; ?>" />
            <div class="or f-left"><? echo Yii::t('var', 'и/или');?></div>
            <select id="categories" placeholder="Введите категорию" name="category">
                <option value="">Выберите категорию</option>
                <?
                $dataReader = Category::model()->getCategories();
                foreach ($dataReader as $val) {
                    echo '<option value="'.$val['id'].'">'.$val['name'].'</option>';
                }
                ?>
            </select>
            <input id="countries" class="select-input" placeholder="Введите страну" name="country"/>
            <input id="cities" class="select-input" placeholder="Введите город" name="city"/>
            <script>
                $(function() {
                    var cities = [];
                    var categories = [
                        <?
                        $dataReader = Category::model()->getCategories();
                        foreach ($dataReader as $val) {
                            echo '"'.$val['name'].'",';
                        }
                        ?>
                    ];
                    var countries = [
                        <?
                        $dataReader = Country::model()->getCountries();
                        foreach ($dataReader as $val) {
                            echo '"'.$val['name'].'",';
                        }
                        ?>
                    ];

                    $( "#countries" ).autocomplete({
                        source: countries
                    }).on('keyup',function(){
                            $.ajax({
                                type: 'GET',
                                url: '/city/getCityList',
                                data: {country_id: $( this ).val()},
                                success: function(data){
                                    var cities = data.split(',');
                                    $( "#cities" ).autocomplete({
                                        source: cities
                                    });
                                }
                            });
                            $( "#cities" ).autocomplete({
                                source: countries
                            });
                    });

                    $('.filter-sort').on('click',function(){
                        if ($('.filter-div').css('display')=='none') $('.filter-div').show();
                        else $('.filter-div').hide();
                    });

                    $('ul.sort').on('click',function(){
                        $('.filter-div').hide();
                    })

                    $('.ui-menu-item').on('mouseover',function(){
                        $(this).addClass('ui-state-focus');
                    });

                    $('.ui-menu-item').on('mouseout',function(){
                        $(this).removeClass('ui-state-focus');
                    });
                });
            </script>

            <button type="button" name="search-btn-orange" class="search-btn-orange f-right" onclick="getGroupsClick();"><span class="white"><? echo Yii::t('var', 'искать');?></span></button>
        </div>
    <div class="items"></div>
    </form>
</div>
<script>

    $(document).ready(function(){
        $.ajax({
            type: 'GET',
            url: '/ajax/groupListClick',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
            success: function(data){
                $('.items').html(data);
            }
        });

        $(document).on('click','.keywords',function(){
            if ($(this).hasClass('active'))
                $(this).removeClass('active').attr('name','');
            else $(this).addClass('active').attr('name','keywords[]');
            $.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });
        });

        $('.sort .ui-menu-item').on('click',function(){
            $.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()+'&order='+$(this).attr('sort')<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });
        });

        $(document).on('click','.reset-btn-orange',function(){
            $('.keywords.active').removeClass('active');
            $('input.keywords').attr('name','');
            $.ajax({
                type: 'GET',
                url: '/ajax/getFilteredGroups',
                data: $('#search-form').serialize()<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
                success: function(data){
                    $('.items .items').html(data);
                }
            });
        });

    });

    function getGroupsClick(){
        $.ajax({
            type: 'GET',
            url: '/ajax/groupListClick',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
            success: function(data){
                $('.items').html(data);
            }
        });
    }

    function getFilteredGroups(){
        $.ajax({
            type: 'GET',
            url: '/ajax/getFilteredGroups',
            data: $('#search-form').serialize()<?
            if (isset($_GET['view']))
                if ($_GET['view'] == 'list') echo "+'&view=list'"; else echo "+'&view=tab'";
            else echo "+'&view=list'";
            ?>,
            success: function(data){
                $('.items').html(data);
            }
        });
    }

</script>
