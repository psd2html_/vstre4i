<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - F.A.Q';
$this->breadcrumbs=array(
    'F.A.Q',
);
?>




<div class="center faq">

    <form class="f-left">
        <label for="search-answer" class="title f-left">Найти ответы:</label>
        <div class="clear"></div>
        <input name="search" type="text" id="search-answer" class="search-answer search f-left input-border" value="<? if (isset($_GET['search'])) echo $_GET['search']; ?>" />
        <button name="search-answer-btn" class="search-answer-btn input-border f-left"><span>Искать</span></button>
    </form>

    <div class="clear"></div>
    <h2 class="faq-container-title sharp">Поиск вопросов по категориям:</h2>
    <div id="faq-container" class="faq-container white-block">
        <ul>
            <?php
            $criteria = new CDbCriteria(array(/*'condition' => "ID=2",*/));
            $dataProvider=new CActiveDataProvider('FaqCategory', array(
                'criteria' => $criteria,
                'pagination'=>array(
                    'pageSize'=>100,
                ),
            ));
            ?>
            <?
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'application.views.faq._cat',
                'template'=>'{items}{pager}',
                'ajaxUpdate'=>false,
            ));
            ?>
        </ul>
    </div>

    <h2 class="faq-container-title sharp">Популярные вопросы:</h2>
    <ol type="1">
        <?php
        if (isset($_GET['search'])) {
            if (isset($_GET['cat'])) $criteria = new CDbCriteria(array('condition' => "question LIKE '%".$_GET['search']."%' AND cat_id=".$_GET['cat']));
            else $criteria = new CDbCriteria(array('condition' => "question LIKE '%".$_GET['search']."%'"));
        } else $criteria = new CDbCriteria(array(/*'condition' => "ID=2",*/));
        $dataProvider=new CActiveDataProvider('Faq', array(
            'criteria' => $criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
        ?>
        <?
        $this->widget('zii.widgets.CListView', array(
            'dataProvider'=>$dataProvider,
            'itemView'=>'application.views.faq._view',
            'template'=>'{items}{pager}',
            'ajaxUpdate'=>false,
        ));
        ?>
    </ol>
</div>

