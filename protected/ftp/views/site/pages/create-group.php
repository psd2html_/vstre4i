<div class="center">
<form id="create-group-form" class="b_input" method="get" action="<? echo Yii::app()->createUrl('group/createGroup'); ?>">
    <div class="step rounded" id="step1">

        <h2><? echo Yii::t('var', 'Шаг 1: Укажите страну и город, где будут проходить встречи.');?></h2>
        <?php

        if (Yii::app()->language != 'ru') {
            $criteria = new CDbCriteria();
            $criteria->alias = 'country';
            $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
            $criteria->join = 'LEFT JOIN `translate` ON `country`.`id` = `translate`.`row_id`';
            $criteria->condition = "`translate`.`table_name`='country' AND `translate`.`language`='".Yii::app()->language."'";
            $models = Country::model()->findAll($criteria);
        } else {
            $models = Country::model()->findAll();
        }

        $list = CHtml::listData($models,
            'id', 'name');
        echo CHtml::DropDownList('country','',$list,
            array('prompt'=>'Выберите страну',
                'onchange'=> CHtml::ajax(array('type'=>'GET',
                    'url'=>Yii::app()->createUrl('city/getCities'),
                    'data' => array('country_id' => 'js:$(this).val()'),
                    'update'=> '#subcat_0'          ))
            )
        );
        ?>
        <select id="subcat_0" name="city"></select>
        <div class="break"></div>
        <a id="step1_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>

    <div class="step" id="step2">

        <h2><? echo Yii::t('var', 'Шаг 2: Укажите категорию и ключевые слова.');?></h2>
        <? echo Yii::t('var', 'Вы можете выбрать <strong>до 10 ключевых слов</strong>, которые лучшим образом описывают Вашу группу.');?>
        <div class="break"></div>
        <div style="width: 902px" class="item">
            <span><? echo Yii::t('var', 'Ваши ключевые слова');?></span>
            <div class="keywords"></div>
        </div>


        <div class="item">
            <span><? echo Yii::t('var', 'Выберите категорию');?></span>
            <?php

            if (Yii::app()->language != 'ru') {
                $criteria = new CDbCriteria();
                $criteria->alias = 'category';
                $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
                $criteria->join = 'LEFT JOIN `translate` ON `category`.`id` = `translate`.`row_id`';
                $criteria->condition = "`translate`.`table_name`='category' AND `translate`.`language`='".Yii::app()->language."'";
                $models = Category::model()->findAll($criteria);
            } else {
                $models = Category::model()->findAll();
            }

            $list = CHtml::listData($models,
                'id', 'name');
            echo CHtml::DropDownList('category','',$list,
                array('prompt'=>'Выберите категорию',
                    'onchange'=> CHtml::ajax(array('type'=>'GET',
                        'url'=>Yii::app()->createUrl('keyword/getKeywords'),
                        'data' => array('cat_id' => 'js:$(this).val()'),
                        'update'=> '#keywords'          ))
                )
            );
            ?>
        </div>
        <div class="clear"></div>
        <div class="arrow-up"></div>
        <div class="list_container" id="keywords">

        </div>
        <input type="hidden" name="keywords" value="">
        <div class="break"></div>
        <a id="step2_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Я закончил выбор');?></span>
        </a>


    </div>


    <div class="step" id="step3">
        <h2><? echo Yii::t('var', 'Шаг 3: Укажите название вашей группы.');?></h2>
        <div class="item">
            <span><? echo Yii::t('var', 'Как будет называться группа');?></span>
            <input type="text" name="name">
        </div>

        <div class="break"></div>
        <a id="step3_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>



    <div class="step" id="step4">
        <h2><? echo Yii::t('var', 'Шаг 4: Введите описание группы.');?></h2>
        <div class="item">
            <span><? echo Yii::t('var', 'Какие цели встреч, кому интересно и т.д.');?></span>
            <textarea cols="90" rows="10" name="description"></textarea>
        </div>

        <div class="break"></div>
        <a id="step4_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>

    <div class="step" id="step6">
        <h2><? echo Yii::t('var', 'Шаг 5: Зарегистрируйтесь или войдите в систему.');?></h2>


        <div class="sep"></div>
        <? if (Yii::app()->user->isGuest) { ?>
        <div class="item">

            <table style="display: none" id="log">
                <tbody><tr>
                    <td class="info" colspan="2"><? echo Yii::t('var', 'Вход в систему. Ещё не зарегистрированы?');?>
                        <a id="reg_b"><? echo Yii::t('var', 'Создать аккаунт!');?></a></td>

                </tr>
                <tr>
                    <td width="200"><? echo Yii::t('var', 'Email');?></td>
                    <td><input type="text"></td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Пароль');?></td>
                    <td><input type="text"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="checkbox"><? echo Yii::t('var', 'Запомнить меня');?></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="<? echo Yii::t('var', 'Войти');?>"></td>
                </tr>
                </tbody></table>

            <table id="reg">
                <tbody><tr>
                    <td class="info" colspan="2"><? echo Yii::t('var', 'Регистрация в системе. Уже зарегистрированы?');?>
                        <a id="log_b"><? echo Yii::t('var', 'Войти');?></a></td>
                </tr>
                <tr>
                    <td width="200"><? echo Yii::t('var', 'Email');?></td>
                    <td><input type="text"></td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Пароль');?></td>
                    <td><input type="text" class="error_input"></td>
                </tr>

                <tr>
                    <td><? echo Yii::t('var', 'Подтвердите пароль');?></td>
                    <td><input type="text" class="good_input"></td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="<? echo Yii::t('var', 'Регистрация');?>"></td>
                </tr>
                </tbody>
            </table>
        </div>
        <? } ?>
        <div class="break"></div>
        <a class="button button-blue">
            <span class="create-group" style="margin-top: 0px;"><? echo Yii::t('var', 'Создать группу');?></span>
        </a>
    </div>
</form>
</div>




<script>
    jQuery(document).ready(function(){
        jQuery('span.create-group').on('click',function(){
            jQuery.ajax({
                type: 'GET',
                url: jQuery('#create-group-form').attr('action'),
                data: jQuery('#create-group-form').serialize()+"&user_id="+<? echo User::model()->getUserId();?>,
                success: function(data){
                    window.location.href='<? echo Yii::app()->createUrl('site/index'); ?>/group/'+data;
                }
            });
        });
    });
</script>

<script type="text/javascript" src="http://preview.kazkibergetic.kz/vstre4i/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="http://preview.kazkibergetic.kz/vstre4i/js/jquery.leanModal.min.js"></script>
<script type="text/javascript" src="http://preview.kazkibergetic.kz/vstre4i/js/jquery-ui-1.10.1.custom.js"></script>
<script type="text/javascript" src="http://preview.kazkibergetic.kz/vstre4i/js/chosen.jquery.js"></script>
<script>
    $(document).ready(function() {

        $('#keywords').on('click','.keyword-item',
            function(){
                var text = $('.keywords').html();
                if (text == '') {
                    $('.keywords').html($('.keywords').html()+'<div>'+$(this).text()+'</div>'+'<div class="del-item">&nbsp[x]</div>');
                    $('input[name=keywords]').val($('input[name=keywords]').val()+$(this).attr('kid'));
                } else {
                    $('.keywords').html($('.keywords').html()+'<div>'+'<div>,&nbsp&nbsp</div>'+$(this).text()+'</div>'+'<div class="del-item">&nbsp[x]</div>');
                    $('input[name=keywords]').val($('input[name=keywords]').val()+','+$(this).attr('kid'));
                }
            });

        $('#loginza_auth_form').click(function(){

            alert("dsads");
            //$("#lean_overlay").fadeOut(200);
        });

        $(".drop_down").chosen();

        $('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });

        $('.step_b').click(function(){

            var nextStep = $(this).parent('.step').next('.step');
            nextStep.show();
            //$("#lean_overlay").fadeOut(200);
            $(this).hide();




            var targetOffset = nextStep.offset().top;
            $('html,body').animate({scrollTop: targetOffset}, 1000);


        });


        $(".add_meet #reg_b").click(function(){


            $(".add_meet #log").hide();
            $(".add_meet #reg").show();



        });

        $(".add_meet #log_b").click(function(){

            $(".add_meet #reg").hide();
            $(".add_meet #log").show();

        });






        $(".anystring").keyup(function(){

            var val = $(this).val();

            if(val.length > 2)
            {

                $(this).removeClass("error_input");
                $(this).addClass("good_input");


            }
            else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }


        });


        $(".numstring").keyup(function(){

            var val = $(this).val();

            if(val.length > 6)
            {
                if(isStrinValid(val))
                {
                    $(this).removeClass("error_input");
                    $(this).addClass("good_input");
                }
                else {
                    $(this).removeClass("good_input");
                    $(this).addClass("error_input");
                }
            }
            else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }


        });


        function isStrinValid(string)
        {
            var pattern = new RegExp(/^[A-Za-z][A-Za-z0-9]+$/);
            return pattern.test(string);
        }



        $(".email").keyup(function(){

            var email = $(this).val();

            if(email != 0)
            {
                if(isValidEmailAddress(email))
                {
                    $(this).removeClass("error_input");
                    $(this).addClass("good_input");
                }
                else {
                    $(this).removeClass("good_input");
                    $(this).addClass("error_input");
                }
            } else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }


        });



        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }




        var header = document.querySelector('.search_form');
        var origOffsetY = header.offsetTop;

        function onScroll(e) {
            window.scrollY >= origOffsetY+100 ? header.classList.add('sticky') :
                header.classList.remove('sticky');
        }

        document.addEventListener('scroll', onScroll);

    });
</script>