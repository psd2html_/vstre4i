
<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('country/admin')?>" title="Страны">Страны</a></li>
    <li  class="active"><a href="<?php echo CController::createUrl('city/admin')?>" title="Города">Города</a></li>
</ul>
<div class="content-block">
    <?php
/* @var $this CityController */
/* @var $model City */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create City', 'url'=>array('create')),
);
    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations'),
    ));

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#city-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array('name'=>'country','value'=>'$data->country->name'),
		array('name'=>'name','value' => '$data->name'),
        array(
            'name'=>'ua',
            'value'=>'$data->translate?$data->translate[0]->value:NULL',
        ),
        array(
            'name'=>'kz',
            'value'=>'$data->translate?$data->translate[1]->value:NULL'
        ),
        array(
            'name'=>'be',
            'value'=>'$data->translate?$data->translate[2]->value:NULL'
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>