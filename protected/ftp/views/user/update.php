<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>

<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><?php echo $model->name; ?></h1>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>