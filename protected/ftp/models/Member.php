<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property string $member_type
 * @property integer $confirm
 */
class Member extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, user_id, group_id, date', 'required'),
			array('id, user_id, group_id, confirm', 'numerical', 'integerOnly'=>true),
			array('member_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, group_id, member_type, confirm, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'group_id' => 'Group',
			'member_type' => 'Member Type',
			'confirm' => 'Confirm',
            'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('member_type',$this->member_type,true);
		$criteria->compare('confirm',$this->confirm);
        $criteria->compare('date',$this->date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getGroupMembersCount($id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT count(*) FROM `member` WHERE `group_id`=$id AND `confirm`=1");
        return $value=$command->queryScalar();
    }
    public function isMember($id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `confirm` FROM `member` WHERE `group_id`=$group_id AND `user_id`=$id");
        return $value=$command->queryScalar();
    }
    public function sendRequest($array,$group_id,$user_id){
        $connection = Yii::app()->db;
        $command = $connection->createCommand("INSERT INTO `member` VALUES ('',$user_id,$group_id,'',0,'".date('Y-m-d')."')");
        $rowCount=$command->execute();
        foreach ($array as $key => $value) {
            $command = $connection->createCommand("INSERT INTO `group_additional_fields` VALUES ('',$user_id,$group_id,$key,'".$value."')");
            $rowCount=$command->execute();
        }
    }
    public function getAdminID($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `user_id` FROM `group_admin` WHERE `group_id`=$group_id");
        return $value=$command->queryScalar();
    }

    /*
     * @deprecated
     * */
    public function getModeratorsList($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `member`.`user_id` as `user_id`,`user`.`name` as `name` FROM `member`,`user` WHERE `user`.`id`=`member`.`user_id` AND `member`.`group_id`=$group_id AND `member`.`member_type`='moder'");
        return $value=$command->query();
    }
    public function getGroupMembers($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `user`.`id` as `id`,`user`.`name` as `name`,`user`.`avatar` as `avatar` FROM `member`,`user` WHERE `user`.`id`=`member`.`user_id` AND `member`.`group_id`=$group_id AND `member`.`confirm`=1");
        return $value=$command->query();
    }
    public function getGroupMembersVK($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `user`.`id` as `id`,`user`.`serv_id` as `serv_id`/*,`user`.`name` as `name`,`user`.`avatar` as `avatar`*/ FROM `member`,`user` WHERE `user`.`id`=`member`.`user_id` AND `member`.`group_id`=$group_id AND `member`.`confirm`=1 AND `user`.`service`='vkontakte'");
        return $value=$command->query();
    }
    public function getGroupMembersFacebook($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `user`.`serv_id` as `id`,`user`.`name` as `name`,`user`.`avatar` as `avatar` FROM `member`,`user` WHERE `user`.`id`=`member`.`user_id` AND `member`.`group_id`=$group_id AND `member`.`confirm`=1 AND `user`.`service`='facebook'");
        return $value=$command->query();
    }

    /*
     * @deprecated
     * */
    public function appointModerator($user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("UPDATE `member` SET `member_type`='moder' WHERE `user_id`=$user_id AND `group_id`=$group_id");
        return $value=$command->query();
    }
    public function disrankModerator($user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("UPDATE `member` SET `member_type`='' WHERE `user_id`=$user_id AND `group_id`=$group_id");
        return $value=$command->query();
    }

    /*
     * @deprecated
     * */
    public function isModerator($user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id` FROM `member` WHERE `group_id`=$group_id AND `user_id`=$user_id AND `confirm`=1 AND `member_type`='moder'");
        return $value=$command->queryScalar();
    }
    public function kickMember($user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("UPDATE `member` SET `confirm`=2 WHERE `user_id`=$user_id AND `group_id`=$group_id");
        return $value=$command->query();
    }
    public function getRequestsCount($meet_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT count(*) FROM `member` WHERE `group_id`=$meet_id AND confirm=1");
        $value = $command->queryScalar();
        return $value;
    }
    public function isMeetMember($meet_id,$user_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `confirm` FROM `member` WHERE `group_id`=$meet_id AND `user_id`=$user_id");
        $value = $command->queryScalar();
        return $value;
    }
    public function acceptRequest($request_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("UPDATE `member` SET `confirm`=1 WHERE `id`=$request_id");
        return $value=$command->query();
    }
    public function refuseRequest($request_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("UPDATE `member` SET `confirm`=2 WHERE `id`=$request_id");
        return $value=$command->query();
    }
    public function removeFromBlacklist($request_id,$user_id,$group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("DELETE FROM `group_additional_fields` WHERE `user_id`=$user_id AND `group_id`=$group_id");
        $command->execute();
        $command = $connection->createCommand("DELETE FROM `member` WHERE `id`=$request_id");
        return $rowCount=$command->execute();
    }
}