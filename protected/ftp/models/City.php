<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $parent
 * @property string $language
 */
class City extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return City the static model class
	 */


    public $value;
    public $ua;
    public $kz;
    public $be;
    public $ru;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country_id, name', 'required'),
			array('country_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, country_id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id', 'on'=>'translate.table_name ="city"'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id')

        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_id' => 'Country',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array('country','translate');
        $criteria->together = true;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.country_id',$this->country_id);
		$criteria->compare('t.name',$this->name,true);
        $criteria->compare('translate.language', $this->ua);
        $criteria->compare('translate.language', $this->kz);
        $criteria->compare('translate.language', $this->be);

		$criteria->compare('id',$this->id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes'=>array(
                    'ua'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'be'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'kz'=>array(
                        'asc'=>'translate.language',
                        'desc'=>'translate.language DESC',
                    ),
                    'name' => array(
                        'asc'=>'t.name',
                        'desc'=>'t.name DESC',
                    ),
                    'country' => array(
                        'asc'=>'country.name',
                        'desc' => 'country.name DESC'
                    ),
                    'id' => array(
                        'asc' => 't.id',
                        'desc' => 't.id DESC'
                    )
                ),
		)));
	}
    public function getCity($id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `name` FROM `city` WHERE `id`=$id");
        return $value=$command->queryScalar();
    }
    public function getCityID($city)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `id` FROM `city` WHERE `name`='".$city."'");
        $value=$command->queryScalar();
        if ($value == NULL) $value=0;
        return $value;
    }
    public function getAllGroupCities()
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("
            SELECT DISTINCT `city`.`name` AS `name`, `city`.`id` AS `id`, `country`.`name` AS `country`
            FROM `city`
            INNER JOIN `group` ON `group`.`city_id` = `city`.`id`
            LEFT JOIN `country` ON `country`.`id` = `city`.`country_id`
            ORDER BY `country`.`name`,`city`.`name`
        ");
        $dataReader = $command->query();
        $result = '';
        foreach($dataReader as $row) {
            $result .= '{ label: "'.$row['name'].'", category: "'.$row['country'].'" },';
        }
        return $result;
    }
    public function getCityList($country_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `city`.`id` as `id`,`city`.`name` as `name` FROM `city`,`country` WHERE `country`.`id`=`city`.`country_id` AND `country`.`name`='".$country_id."'");
        $value=$command->query();
        return $value;
    }
}