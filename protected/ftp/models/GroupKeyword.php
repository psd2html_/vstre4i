<?php

/**
 * This is the model class for table "group_keyword".
 *
 * The followings are the available columns in table 'group_keyword':
 * @property integer $id
 * @property integer $group_id
 * @property integer $field_id
 */
class GroupKeyword extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GroupKeyword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group_keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, field_id', 'required'),
			array('group_id, field_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, field_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'field_id' => 'Field',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('field_id',$this->field_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getGroupKeywords($group_id)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT `keyword`.`id` as `id`,`keyword`.`name` as `name` FROM `group_keyword`,`keyword` WHERE `group_keyword`.`group_id`=$group_id AND `keyword`.`id`=`group_keyword`.`field_id`");
        return $result=$command->query();
    }
    public function getAllKeywords()
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("
            SELECT DISTINCT `keyword`.`name` AS `name` , `keyword`.`id` AS `id` , `category`.`name` AS `cat`
            FROM `keyword`
            INNER JOIN `group_keyword` ON `keyword`.`id` = `group_keyword`.`field_id`
            LEFT JOIN `category` ON `keyword`.`cat_id` = `category`.`id`
            ORDER BY `category`.`name`
            LIMIT 0 , 30
        ");
        $dataReader = $command->query();
        $result = '';
        foreach($dataReader as $row) {
            $result .= '{ label: "'.$row['name'].'", category: "'.$row['cat'].'" },';
        }
        return $result;
    }
    public function getGroupsKeywordsList($groups)
    {
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT distinct `keyword`.`id`,`keyword`.`name`
                                                FROM `group`,`group_keyword`,`keyword`
                                                WHERE `group`.`id`=`group_keyword`.`group_id`
                                                AND `group_keyword`.`field_id`=`keyword`.`id`
                                                AND `group`.`id` IN (".implode(',', $groups).")");
        return $result=$command->query();
    }
}