<?php

/**
 * This is the model class for table "group_keyword".
 *
 * The followings are the available columns in table 'group_keyword':
 * @property integer $id
 * @property integer $group_id
 * @property integer $field_id
 */
class GroupKeyword extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GroupKeyword the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group_keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group_id, field_id', 'required'),
			array('group_id, field_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, group_id, field_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'group_id' => 'Group',
			'field_id' => 'Field',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('field_id',$this->field_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getGroupKeywords($groupId, $allFields=true)
    {
        $db = Yii::app()->db;
		if($allFields===true){
			$keywords = $db->createCommand()
				->select('keyword.*')
				->from($this->tableName())
				->join('keyword', 'keyword.id = group_keyword.field_id')
				->where('group_keyword.group_id=:groupId', array('groupId'=>$groupId))
				->queryAll();

		}else{
			$keywords = $db->createCommand()
				->select('keyword.name')
				->from($this->tableName())
				->join('keyword', 'keyword.id = group_keyword.field_id')
				->where('group_keyword.group_id=:groupId', array('groupId'=>$groupId))
				->queryColumn();
		}
		return $keywords;
		
    }

    public function getAllKeywords()
    {
        $db = Yii::app()->db;

        $dataReader = $db->createCommand()
            ->selectDistinct('keyword.name, keyword.id, category.name AS cat')
            ->from('keyword')
            ->join('group_keyword', 'keyword.id = group_keyword.field_id')
            ->leftJoin('category', 'keyword.cat_id = category.id')
            ->order('category.name')
            ->limit(30, 0)
            ->queryAll();

        $result = '';
        foreach($dataReader as $row) {
            $result .= '{ label: "'.CHtml::encode($row['name']).'", category: "'.CHtml::encode($row['cat']).'" },';
        }
        return $result;
    }

    public function getGroupsKeywordsList($groupIds)
    {
        $db = Yii::app()->db;

        foreach ($groupIds as $key => $groupId) {
            $groupIds[$key] = (int)$groupId;
        }

        $keywords = $db->createCommand()
            ->selectDistinct('keyword.id, keyword.name')
            ->from('group')
            ->join('group_keyword', 'group.id = group_keyword.group_id')
            ->join('keyword', 'group_keyword.field_id = keyword.id')
            ->where('group.id IN (' . implode(',', $groupIds) . ')')
            ->queryAll();

        return $keywords;
    }
}
