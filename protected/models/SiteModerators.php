<?php

/**
 * This is the model class for table "menus".
 *
 * The followings are the available columns in table 'menus':
 * @property integer $id
 * @property string $name
 * @property string $language
 */
class SiteModerators extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SiteModerators the static model class
	 */

    public $ua = false;
    public $kz = false;
    public $be = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_moderators';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			//array('title', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			//array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'translate' => array(
                self::HAS_MANY,
                'Translate',
                'row_id',
                'condition'=>'translate.table_name="admin_moderators"'
            ),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User Id',
		);
	}


    public function getModerators()
    {
        $db = Yii::app()->db;

        $moderators = $db->createCommand()
            ->select('moderators.*, user.id AS user_id, '
                . 'user.name AS user_name, user.avatar AS user_avatar')
            ->from($this->tableName() . ' AS moderators')
            ->join('user', 'user.id = moderators.user_id')
            ->queryAll();

        return $moderators;
    }

    public function getRequests()
    {
        $db = Yii::app()->db;

        $requests = $db->createCommand()
            ->select('requests.*, user.id AS user_id, '
                . 'user.name AS user_name, user.avatar AS user_avatar')
            ->from('admin_moderators_requests AS requests')
            ->join('user', 'user.id = requests.target_id')
            ->queryAll();

        return $requests;
    }

    public function getRequest($targetId)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from('admin_moderators_requests')
            ->where('target_id = ' . (int)$targetId)
            ->queryRow();

        return $request;
    }

    public function makeRequest($targetId)
    {
        $db = Yii::app()->db;
        $userId = User::model()->getUserId();

        $columns = array(
            'target_id' => (int)$targetId,
            'sender_id' => $userId,
        );

        $db->createCommand()
            ->insert('admin_moderators_requests', $columns);

        $id = $db->getLastInsertId();
        if ($id) {
            $token = md5($id . $targetId . $userId . time());
            $db->createCommand()
                ->update(
                    'admin_moderators_requests',
                    array('token'=>$token),
                    'id = ' . (int)$id
                );
            return $token;
        } else {
            return 0;
        }
    }

    public function satisfyRequest($targetId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->insert(
                $this->tableName(),
                array('user_id' => (int)$targetId)
            );

        if ($success) {
            $success = $this->deleteRequest($targetId);
        }

        return $success;
    }

    public function denyRequest($targetId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete('admin_moderators_requests', 'target_id = ' . (int)$targetId);

        return $success;
    }

    public function deleteRequest($targetId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete('admin_moderators_requests', 'target_id = ' . (int)$targetId);

        return $success;
    }

    public function deleteOldRequests()
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete('admin_moderators_requests', 'date < NOW() - INTERVAL 24 HOUR');

        return $success;
    }

    public function isUserRequested($userId)
    {
        $db = Yii::app()->db;

        $this->deleteOldRequests();

        $request = $db->createCommand()
            ->select()
            ->from('admin_moderators_requests')
            ->where('target_id = ' . (int)$userId)
            ->queryRow();

        return (bool)$request;
    }

    public function fireModerator($userId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete($this->tableName(), 'user_id = ' . (int)$userId);

        return $success;
    }

    public function isSiteModerator($userId)
    {
        $db = Yii::app()->db;

        $this->deleteOldRequests();

        $isModerator = $db->createCommand()
            ->select()
            ->from($this->tableName())
            ->where('user_id = ' . (int)$userId)
            ->queryRow();

        return (bool)$isModerator;
    }
}