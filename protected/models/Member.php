<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 * @property string $member_type
 * @property integer $confirm
 */
class Member extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs. 
		return array(
			array('user_id, group_id, date', 'required'),
			array('user_id, group_id, confirm', 'numerical', 'integerOnly'=>true),
			array('member_type', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, group_id, member_type, confirm, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'group_id' => 'Group',
			'member_type' => 'Member Type',
			'confirm' => 'Confirm',
            'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('group_id',$this->group_id);
		$criteria->compare('member_type',$this->member_type,true);
		$criteria->compare('confirm',$this->confirm);
        $criteria->compare('date',$this->date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getGroupMembersCount($groupId)
    {
        $db = Yii::app()->db;

        $count = $db->createCommand()
            ->select('COUNT(*)')
            ->from($this->tableName())
            ->where('`group_id` = ' . (int)$groupId . ' AND `confirm`=1')
            ->queryScalar();

        return $count;
    }

    public function isMember($userId, $groupId)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->select('confirm')
            ->from('member')
            ->where('`group_id` = ' . (int)$groupId . ' AND `user_id` = ' . (int)$userId)
            ->queryScalar();

        return $result;
    }

    public function getConfirmType($userId, $groupId)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->select('confirm')
            ->from('member')
            ->where('`group_id` = ' . (int)$groupId . ' AND `user_id` = ' . (int)$userId)
            ->queryScalar();

        return $result;
    }

    public function sendRequest($array, $groupId, $userId)
    {
        $db = Yii::app()->db;
		
		$rowCount = $db->createCommand()
            ->select('COUNT(*)')
            ->from($this->tableName())
            ->where('user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId)
            ->queryScalar();

		if ($rowCount==0) {
            $memberColumns = array(
                'user_id' => (int)$userId,
                'group_id' => (int)$groupId,
                'member_type' => '',
                'confirm' => 0,
                'date' => date('Y-m-d'),
            );

            $rowCount = $db->createCommand()
                ->insert($this->tableName(), $memberColumns);

			if (!empty($array))
				foreach ($array as $key => $value) {
                    $addFieldsColumns = array(
                        'user_id'  => (int)$userId,
                        'group_id' => (int)$groupId,
                        'field_id' => $key,
                        'value'    => $value,
                    );
                    $rowCount = $db->createCommand()
                        ->insert('group_additional_fields', $addFieldsColumns);
				}
		}
    }

    public function getAdminID($groupId)
    {
        $db = Yii::app()->db;

        $adminId = $db->createCommand()
            ->select('user_id')
            ->from('group_admin')
            ->where('group_id = ' . (int)$groupId)
            ->queryScalar();

        return $adminId;
    }

    public function getGroupAdmin($groupId)
    {
        $db = Yii::app()->db;

        $admin = $db->createCommand()
            ->select('group_admin.user_id, user.name, user.avatar, user.email')
            ->from('user')
            ->join('group_admin', 'group_admin.user_id = user.id')
            ->where('group_admin.group_id = ' . (int)$groupId)
            ->queryRow();

        return $admin;
    }

    /*
     * @deprecated
     * */
    public function getModeratorsList($groupId)
    {
        $db = Yii::app()->db;

        $moderators = $db->createCommand()
            ->select('member.user_id, user.name, user.avatar, user.email')
            ->from('member')
            ->join('user', 'user.id = member.user_id')
            ->where('member.group_id = ' . (int)$groupId . ' AND member.member_type = "moder"')
            ->queryAll();

        return $moderators;
    }

    public function getGroupMembers($groupId)
    {
        $db = Yii::app()->db;

        $members = $db->createCommand()
            ->select('user.id, user.name, user.avatar, user.email')
            ->from($this->tableName())
            ->join('user', 'user.id = member.user_id')
            ->where('`member`.`group_id` = ' . (int)$groupId . ' AND `member`.`confirm` = 1')
            ->queryAll();

        return $members;
    }

    public function getGroupMembersVK($groupId)
    {
        $db = Yii::app()->db;

        $members = $db->createCommand()
            ->select('user.id, user.serv_id')
            ->from($this->tableName())
            ->join('user', 'user.id = member.user_id')
            ->where('`member`.`group_id` = ' . (int)$groupId . ' AND `member`.`confirm` = 1 AND `user`.`service` = "vkontakte"')
            ->queryAll();

        return $members;
    }

    public function getGroupMembersFacebook($groupId)
    {
        $db = Yii::app()->db;

        $members = $db->createCommand()
            ->select('user.serv_id as id, user.name, user.avatar')
            ->from($this->tableName())
            ->join('user', 'user.id = member.user_id')
            ->where('`member`.`group_id` = ' . (int)$groupId . ' AND `member`.`confirm` = 1 AND `user`.`service` = "facebook"')
            ->queryAll();

        return $members;
    }

    /*
     * @deprecated
     * */
    public function appointModerator($userId, $groupId)
    {
        $db = Yii::app()->db;

        $whereClause = 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId;
        $rowCount = $db->createCommand()
            ->update($this->tableName(), array('member_type' => 'moder'), $whereClause);

        return $rowCount;
    }

    /*
     * @deprecated
     * */
    public function disrankModerator($userId, $groupId)
    {
        $db = Yii::app()->db;

        $whereClause = 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId;
        $rowCount = $db->createCommand()
            ->update($this->tableName(), array('member_type' => ''), $whereClause);

        return $rowCount;
    }

    /*
     * @deprecated
     * */
    public function isModerator($userId, $groupId)
    {
        $db = Yii::app()->db;

        $isModerator = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where('`group_id` = ' . (int)$groupId . ' AND `user_id` = ' . (int)$userId
                . ' AND `confirm` = 1 AND `member_type` = "moder"')
            ->queryScalar();

        return $isModerator;
    }

    public function kickMember($userId, $groupId)
    {
        $db = Yii::app()->db;

        $whereClause = 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId;
        $rowCount = $db->createCommand()
            ->update($this->tableName(), array('confirm'=>2), $whereClause);

        return $rowCount;
    }

    public function getRequestsCount($meetId)
    {
        $db = Yii::app()->db;

        $count = $db->createCommand()
            ->select('COUNT(*)')
            ->from($this->tableName())
            ->where('`group_id` = ' . (int)$meetId . ' AND `confirm` = 1')
            ->queryScalar();

        return $count;
    }

    public function isMeetMember($meetId, $userId)
    {
        $db = Yii::app()->db;

        $isMember = $db->createCommand()
            ->select('confirm')
            ->from($this->tableName())
            ->where('group_id = ' . (int)$meetId . ' AND user_id = ' . (int)$userId)
            ->queryScalar();

        return $isMember;
    }

    public function getRequest($requestId)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from($this->tableName())
            ->where('id = ' . (int)$requestId)
            ->queryRow();

        return $request;
    }

    public function acceptRequest($requestId)
    {
        $db = Yii::app()->db;

        $value = $db->createCommand()
            ->update($this->tableName(), array('confirm' => 1), 'id = ' . (int)$requestId );

        return $value;
    }

    public function refuseRequest($requestId)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->delete($this->tableName(), '`id` = ' . (int)$requestId);

        return $result;
    }

    public function addToBlackList($requestId)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->update($this->tableName(), array('confirm'=>2), '`id` = ' . (int)$requestId);

        return $result;
    }

    public function removeFromBlacklist($requestId, $userId, $groupId)
    {
        $db = Yii::app()->db;

        $rowCount = $db->createCommand()
            ->delete('group_additional_fields', 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId);

        $rowCount += $db->createCommand()
            ->delete($this->tableName(), 'id = ' . (int)$requestId);

        return $rowCount;
    }
}
