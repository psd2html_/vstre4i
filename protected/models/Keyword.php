<?php

/**
 * This is the model class for table "keyword".
 *
 * The followings are the available columns in table 'keyword':
 * @property integer $id
 * @property integer $cat_id
 * @property string $name
 * @property integer $parent
 */
class Keyword extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Keyword the static model class
	 */


    public $ua = false;
    public $kz = false;
    public $be = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'keyword';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_id, name', 'required'),
			array('cat_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cat_id, name, category, ua, kz, be', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'translate' => array(self::HAS_MANY, 'Translate', 'row_id','condition'=>'translate.table_name="keyword"'),
            'category' => array(self::BELONGS_TO, 'Category', 'cat_id')

        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => '���������',
			'cat_id' => '���������',
			'name' => '�����',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	//public $category;
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.cat_id',$this->cat_id);
		$criteria->compare('t.name',$this->name,true);
        $criteria->with = array('category');
		$criteria->compare('category.id',$_GET['Keyword']['category'],true);
		//$criteria->compare('translate.value',$_GET['Keyword']['ua'],true);
		//$criteria->compare('translate.value',$_GET['Keyword']['kz'],true);
		//$criteria->compare('translate.value',$_GET['Keyword']['be'],true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function getKeywordName($keyword)
    {
        $db = Yii::app()->db;

        $name = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where('name = :keyword', array(':keyword'=>$keyword))
            ->queryScalar();

        return $name;
    }

    public function getUserInterests($userId)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->select('keyword.id, keyword.name')
            ->from('interest')
            ->join('keyword', 'interest.keyword_id = keyword.id')
            ->where('interest.user_id = ' . (int)$userId)
            ->queryAll();

        return $result;
    }

    public function insertUserInterest($userId, $keywordId)
    {
        $db = Yii::app()->db;

		$existId = $db->createCommand()
            ->select('id')
            ->from('interest')
            ->where('user_id = ' . (int)$userId . ' AND keyword_id = ' . (int)$keywordId)
            ->queryScalar();

        if (!$existId) {
            $columns = array(
                'user_id' => (int)$userId,
                'keyword_id' => (int)$keywordId
            );
            $rowCount = $db->createCommand()
                ->insert('interest', $columns);

        	return $rowCount;
        }

        return 0;
    }

    public function removeUserInterest($userId, $kid)
    {
        $db = Yii::app()->db;

        $rowCount = $db->createCommand()
            ->delete('interest', 'user_id = ' . (int)$userId . ' AND keyword_id = ' . (int)$kid);

        return $rowCount;
    }

    public function getInterestsCategory()
    {
        $result = '';
        $db = Yii::app()->db;

        $dataReader = $db->createCommand()
            ->select('id, name')
            ->from('category')
            ->order('name')
            ->queryAll();

        foreach ($dataReader as $val) {
            $result .= '<li class="keywords-list-cat" title="'.$val['name'].'">'.$val['name'].'</li>';
        }
        return $result;
    }

    public function getCategoryInterestsList($category)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->select('keyword.id, keyword.name')
            ->from('category')
            ->join('keyword', 'keyword.cat_id = category.id')
            ->where('category.name = :category', array(':category'=>$category))
            ->order('name')
            ->queryAll();

        return $result;
    }

    /* public function getGroupKeyWords($group_id)
    {
        $db = Yii::app()->db;

        $result = $db->createCommand()
            ->select('keyword.name')
            ->from('group_keyword')
            ->join('keyword', 'keyword.id = group_keyword.field_id')
            ->where('category.name = :category', array(':category'=>$category))
            ->order('name')
            ->queryAll();

        return $result;
    } */

    public function getCategoryKeywords($catId)
    {
        $db = Yii::app()->db;

        $keywords = $db->createCommand()
            ->select()
            ->from('keyword')
            ->where('cat_id = ' . (int)$catId)
            ->queryAll();

        return $keywords;
    }

    public function getCategoryKeywordIds($categoryId)
    {
        $keywords = $this->getCategoryKeywords($categoryId);

        $ids = array();
        foreach ($keywords as $keyword) {
            $ids[] = $keyword['id'];
        }

        return $ids;
    }

    public function getKeywordIds($keywordNames)
    {
        $db = Yii::app()->db;

        $keywords = $db->createCommand()
            ->select()
            ->from('keyword')
            ->where(array('in', 'name', $keywordNames))
            ->queryAll();

        $ids = array();
        foreach ($keywords as $keyword) {
            $ids[] = $keyword['id'];
        }

        return $ids;
    }
}
