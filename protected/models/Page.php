<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property string $name
 * @property string $text
 */
class Page extends CActiveRecord
{
	public $useCommonKeywords = false;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, text', 'required'),
			array('name, alias', 'length', 'max'=>100),
			array('keywords, description', 'length', 'max'=>255),
			array('alias, keywords, description, name', 'filter', 'filter'=>function($value){return htmlspecialchars(trim($value));}),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, text, alias, keywords, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Заголовок',
			'text' => 'Текст',
            'alias' => 'Алиас',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('text',$this->text,true);
        $criteria->compare('alias',$this->alias,true);
        $criteria->compare('keywords',$this->alias,true);
        $criteria->compare('description',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	} 
	protected function afterValidate()
	{
		Yii::import('application.extensions.translit.Translite');
		if(!$this->alias)
			$this->alias = Translite::rusencode($this->name);
		if(!$this->description)
			$this->description = self::formatTtext($this->text, 255);
		return parent::afterValidate();
	}

    public function getPageContent($pageId)
    {
        $db = Yii::app()->db;

        $content = $db->createCommand()
            ->select('text')
            ->from($this->tableName())
            ->where('id = ' . (int)$pageId)
            ->queryScalar();

        return $content;
    }

    public function getPageTitle($pageId)
    {
        $db = Yii::app()->db;

        $title = $db->createCommand()
            ->select('name')
            ->from($this->tableName())
            ->where('id = ' . (int)$pageId)
            ->queryScalar();

        return $title;
    }

    public function getPageAlias($pageId)
    {
        $db = Yii::app()->db;

        $alias = $db->createCommand()
            ->select('alias')
            ->from($this->tableName())
            ->where('id = ' . (int)$pageId)
            ->queryScalar();

        return $alias;
    }
	
	//Added 19/11/2014
	public function findByUrl($url) {
		
		return $this->find('alias=:url',array(':url'=>$url));
	}
	public function getPages() {
		$pages = CHtml::listData(self::model()->findAll(), 'id', 'name');
		return  $pages;
    }
	public static function formatTtext($value, $numOfWords) {
        $value = $value;
 
        $lenBefore = strlen($value);
 
        if($numOfWords){
            if(preg_match("/\s*(\S+\s*){0,$numOfWords}/", $value, $match)){
                $value = trim($match[0]);
            }
            if(strlen($value) != $lenBefore){
                $value .= ' ...';
            }
        }
 
        return $value;
    }
}
