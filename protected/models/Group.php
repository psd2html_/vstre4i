<?php

/**
 * This is the model class for table "group".
 *
 * The followings are the available columns in table 'group':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property string $date_start
 * @property string $date_end
 * @property integer $city_id
 * @property string $address
 * @property integer $seats
 * @property string $date_created
 * @property string $picture
 * @property integer $parent
 * @property string $appeal
 * @property string $background
 * @property integer $hided
 * @property integer $approved
 * @property integer $comment_notice
 * @property integer $photo_notice
 * @property integer $member_notise
 */
class Group extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Group the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, description, type, city_id, date_created', 'required'),
			array('city_id, seats, parent, hided, comment_notice, photo_notice, member_notise', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>150),
			array('description', 'length', 'max'=>1000),
			array('type', 'length', 'max'=>7),
			array('appeal', 'length', 'max'=>50),
			array('background', 'length', 'max'=>200),
			array('date_start, date_end, address, picture', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, type, date_start, date_end, city_id, address, seats, date_created, picture, parent, appeal, background, hided, comment_notice, photo_notice, member_notise', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'group_keyword'=>array(self::HAS_MANY, 'GroupKeyword', 'group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'type' => 'Type',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
			'city_id' => 'City',
			'address' => 'Address',
			'seats' => 'Seats',
			'date_created' => 'Date Created',
			'picture' => 'Picture',
			'parent' => 'Parent',
			'appeal' => 'Appeal',
			'background' => 'Background',
            'hided' => 'Hided',
            'approved' => 'Approved',
            'comment_notice' => 'Comment Notice',
            'photo_notice' => 'Photo Notice',
            'member_notise' => 'Member Notice',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($meet)
	{

		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        if($meet){
            $criteria->addCondition('t.type = "meetup"','AND');
        } else {
            $criteria->addCondition('t.type = "group"','AND');
        }
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('seats',$this->seats);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('parent',$this->parent);
		$criteria->compare('appeal',$this->appeal,true);
        $criteria->compare('background',$this->background,true);
        $criteria->compare('hided',$this->hided,true);
        $criteria->compare('approved',$this->approved,true);
        $criteria->compare('comment_notice',$this->comment_notice,true);
        $criteria->compare('photo_notice',$this->photo_notice,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Returns group by id.
     *
     * @param   int $groupId    Group id
     *
     * @return  mixed
     */
    public function getGroup($groupId)
    {
        $db = Yii::app()->db;

        $group = $db->createCommand()
            ->select()
            ->from('group')
            ->where('id = ' . (int)$groupId)
            ->queryRow();

        return $group;
    }

    public function getGroupName($groupId)
    {
        $db = Yii::app()->db;

        $name = $db->createCommand()
            ->select('name')
            ->from('group')
            ->where('id = ' . (int)$groupId)
            ->queryScalar();

        return $name;
    }

    public function createGroup($city, $name, $description, $userId, $keywords)
    {
        $city = City::model()->getCityID($city);
        $db = Yii::app()->db;

        $isApproved = 1;
        $premoderation = $this->getPremoderationSettings();
        if ($premoderation['mode']) {
            $nonApprovedCount = $db->createCommand()
                ->select('COUNT(*)')
                ->from('group')
                ->where('`approved` = 0')
                ->queryScalar();
            if ($nonApprovedCount < $premoderation['limit']) {
                $isApproved = 0;
            }
        }

        $group = new Group();
        $group->name = $name;
        $group->description = $description;
        $group->city_id = $city;
        $group->type = 'group';
        $group->date_created = Date('Y-m-d H:i:s');
        $group->parent = 0;
        $group->approved = $isApproved;

        if (!$group->save()) {
            $groupColumns = array(
                'name' => $name,
                'description' => $description,
                'type' => 'group',
                'city_id' => $city,
                'date_created' => Date('Y-m-d H:i:s'),
                'approved' => $isApproved
            );
            $db->createCommand()
                ->insert('group', $groupColumns);
	        $groupId = $db->lastInsertID;

            $memberColumns = array(
                'user_id' => $userId,
                'group_id' => $groupId,
                'confirm' => 1,
                'date' => date('Y-m-d'),
            );
            $db->createCommand()
                ->insert('member', $memberColumns);
            $db->createCommand()
                ->insert('group_admin', array( 'user_id' => $userId, 'group_id' => $groupId, 'admin' => 1 ) );
	        $keywords = explode(',',$keywords);
	        foreach ($keywords as $value) {
                $db->createCommand()
                    ->insert('group_keyword', array('group_id' => $groupId, 'field_id' => $value));
	        }
        } else {
        	$groupId = $group->id;
        	$member = new Member();
        	$member->user_id = $userId;
        	$member->group_id = $groupId;
        	$member->date = date('Y-m-d');
        	$member->confirm = 1;
        	$member->save();

        	// нет модели group_admin
            $db->createCommand()
                ->insert('group_admin', array('user_id' => $userId, 'group_id' => $groupId, 'admin' => 1) );

	        $keywords = explode(',', $keywords);
	        foreach ($keywords as $value) {
	        	$gk = new GroupKeyword();
	        	$gk->group_id = $groupId;
	        	$gk->field_id = $value;
	        	if (!$gk->save()) {
                    $db->createCommand()
                        ->insert('group_keyword', array('group_id' => $groupId, 'field_id' => $value) );
	            }
	        }
        }
        return $groupId;
    }

    public function isGroupAdmin($groupId, $userId){
        $db = Yii::app()->db;
        $isAdmin = $db->createCommand()
            ->select('admin')
            ->from('group_admin')
            ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
            ->queryScalar();

        if (!$isAdmin) {
            $isAdmin = $db->createCommand()
                ->select('id')
                ->from('group_moderators')
                ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
                ->queryScalar();
            $isAdmin = (bool)$isAdmin;
        }

        return $isAdmin;
    }
    public function getAdminId($itemId, $type = 'group'){
        $db = Yii::app()->db;
        $command = $db->createCommand();
		$command->select('user_id');
		$command->from('group_admin');
		if($type == 'group')
			$command->where('group_id = ' . (int)$itemId);
		elseif($type == 'meet')
			$command->where('group_id = ' . $this->getMeetGroupID($itemId));
		else
			return false;
		$adminId = $command->queryScalar();

        return $adminId;
    }
    public static function isGroupAdminS($groupId, $userId){
        $db = Yii::app()->db;
        $isAdmin = $db->createCommand()
            ->select('admin')
            ->from('group_admin')
            ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
            ->queryScalar();

        if (!$isAdmin) {
            $isAdmin = $db->createCommand()
                ->select('id')
                ->from('group_moderators')
                ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
                ->queryScalar();
            $isAdmin = (bool)$isAdmin;
        }

        return $isAdmin;
    }

    public function isGroupModerator($groupId, $userId)
    {
        $db = Yii::app()->db;

        $isAdmin = $db->createCommand()
            ->select('id')
            ->from('group_moderators')
            ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
            ->queryScalar();
        $isAdmin = (bool)$isAdmin;

        return $isAdmin;
    }

    public function isGroupMainAdmin($groupId, $userId)
    {
        $db = Yii::app()->db;

        $isAdmin = $db->createCommand()
            ->select('admin')
            ->from('group_admin')
            ->where('group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId)
            ->queryScalar();

        return $isAdmin;
    }

    public function joinGroup($userId, $groupId)
    {
        $db = Yii::app()->db;

        $isJoined = $db->createCommand()
            ->select('id')
            ->from('member')
            ->where('`user_id` = ' . $userId . ' AND `group_id` = ' . (int)$groupId)
            ->queryRow();

		if (!$isJoined) {
            $columns = array(
                'user_id' => $userId,
                'group_id' => $groupId,
                'member_type' => '',
                'confirm' => 0,
            );
			$rowCount = $db->createCommand()
                ->insert('member', $columns);

            return $rowCount;
		}
		return false;
    }

    public function leaveGroup($userId, $groupId)
    {
        $db = Yii::app()->db;

        $rowCount = $db->createCommand()
            ->delete('member', 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId);

        return $rowCount;
    }

    public function delGroup($userId, $groupId)
    {
        $db = Yii::app()->db;

        $isAdmin = $this->isGroupMainAdmin($groupId, $userId);

		if ($isAdmin) {
	       	$members = Member::model()->findAll('group_id=:group_id', array(':group_id'=>$groupId));
	       	foreach ($members as $k=>$member) {
		       	$member->delete();
	       	}

	       	$group = Group::model()->find('id=:id', array(':id'=>$groupId));
	       	$group->delete();

            $rowCount = $db->createCommand()
                ->delete('group_admin', 'group_id = ' . (int)$groupId);

	        Yii::app()->user->setFlash('success',"Ваша группа была успешно удалена!");
       	} else {
       		Yii::app()->user->setFlash('error',"Вы не имеете прав на удаление группы!");
       	}
    }

    public function deleteGroup($groupId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete('member', 'group_id = ' . (int)$groupId);
        $success |= $db->createCommand()
            ->delete('group', 'id = ' . (int)$groupId);
        $success |= $db->createCommand()
            ->delete('group_admin', 'group_id = ' . (int)$groupId);

        return $success;
    }

    public function joinMeet($userId, $groupId)
    {
        $db = Yii::app()->db;

        $columns = array(
            'user_id' => (int)$userId,
            'group_id' => (int)$groupId,
            'member_type' => '',
            'confirm' => 1,
        );

        $rowCount = $db->createCommand()
            ->insert('member', $columns);

        return $rowCount;
    }

    public function leaveMeet($userId, $groupId)
    {
        $db = Yii::app()->db;

        $rowCount = $db->createCommand()
            ->delete('member', 'user_id = ' . (int)$userId . ' AND group_id = ' . (int)$groupId);

        return $rowCount;
    }

    public function getPopularGroups()
    {
        $db = Yii::app()->db;

        $popularGroups = $db->createCommand()
            ->select('group.id, group.name, group.picture, group.city_id, COUNT(member.id) AS MEMBERSCOUNT')
            ->from($this->tableName())
            ->join('member', '`group`.`id` = `member`.`group_id` AND `member`.`confirm` = 1')
            ->where('`group`.`type` = "group" AND `group`.`approved` = 1')
            ->group('group.id')
            ->order('MEMBERSCOUNT DESC')
            ->limit(20)
            ->query();

        return $popularGroups;
    }

    public function getFilteredGroups($countryId, $city, $keyword, $keywords, $order = '')
    {
        $db = Yii::app()->db;

        $aliases = array();
        if ($city) {
            $cityClause = 'city.name = :city';
            $aliases[':city'] = $city;
        } elseif ($countryId) {
            $citiesInCountry = City::model()->getCountryCitiesIds($countryId);
            $cityClause = array('in', 'city.id', $citiesInCountry);
        } else {
            $cityClause = '';
        }

        if ($keyword != '') {
            $keyword = array('like', 'group.name', '%' . $keyword . '%');
        }
        if (count($keywords) > 0) {
            $keywordsClause = array('in', 'keyword.name', $keywords);
        } else {
            $keywordsClause = '';
        }

        switch ($order) {
            case 'date_up'  : $order = 'group.date_created ASC'; break;
            case 'date_down': $order = 'group.date_created DESC'; break;
            case 'members'  : $order = 'members DESC'; break;
            case 'meets'    : $order = 'meets DESC'; break;
            default: $order = '';
        }

        $select1 = '(SELECT count(*) FROM member WHERE member.group_id = group.id GROUP BY member.group_id) as members';
        $select2 = '(SELECT count(*) FROM `group` as g WHERE g.parent = group.id AND g.approved = 1 GROUP BY g.parent) as meets';
        $filteredGroups = $db->createCommand()
            ->selectDistinct('group.id, group.name, group.picture, ' . $select1 . ', ' . $select2)
            ->from($this->tableName())
            ->leftJoin('group_keyword', 'group.id = group_keyword.group_id')
            ->leftJoin('city', 'group.city_id = city.id')
            ->leftJoin('keyword', 'group_keyword.field_id = keyword.id')
            ->where(
                array(
                    'and',
                    'group.type = "group"',
                    'group.approved = 1',
                    $keyword,
                    $keywordsClause,
                    $cityClause,
                ),
                $aliases
            )->order($order)
            ->queryAll();

        return $filteredGroups;
    }

    public function getRandomGroup($groupId)
    {
        $db = Yii::app()->db;

        $keywords = array();
        $keys = GroupKeyword::model()->getGroupKeywords($groupId);
        foreach($keys as $row) {
            $keywords[] = $row['id'];
        }
        if (count($keywords)>0) {
            $groupKeywordClause = array('in', 'group_keyword.field_id', $keywords);
        } else {
            $groupKeywordClause = '';
        }

        $randomGroup = $db->createCommand()
            ->selectDistinct('group.id, group.name, group.description, group.picture')
            ->from($this->tableName())
            ->join('group_keyword', 'group_keyword.group_id = group.id')
            ->join('keyword', 'group_keyword.field_id = keyword.id')
            ->where(
                array(
                    'and',
                    'group.id <> ' . (int)$groupId,
                    'group.type = "group"',
                    'group.approved = 1',
                    $groupKeywordClause,
                )
            )->order('RAND()')
            ->limit(1)
            ->query();

        return $randomGroup;
    }

    public function getRandomPaidGroup($groupId)
    {
        $db = Yii::app()->db;

        $keywords = array();
        $keys = GroupKeyword::model()->getGroupKeywords((int)$groupId);
        foreach($keys as $row) {
            $keywords[] = $row['id'];
        }
        if (count($keywords)>0) {
            $groupKeywordClause = array('in', 'group_keyword.field_id', $keywords);
        } else {
            $groupKeywordClause = '';
        }

        $randomPaidGroup = $db->createCommand()
            ->selectDistinct('group.id, group.name, group.description, group.picture')
            ->from($this->tableName())
            ->join('group_keyword', 'group_keyword.group_id = group.id')
            ->join('keyword', 'group_keyword.field_id = keyword.id')
            ->join('paid_group', 'paid_group.group_id = group.id')
            ->where(
                array(
                    'and',
                    'group.id <> ' . (int)$groupId,
                    'group.type = "group"',
                    'group.approved = 1',
                    'paid_group.date_to > "' . date('Y-m-d H:i:s') . '"',
                    $groupKeywordClause,
                )
            )->order('RAND()')
            ->limit(2)
            ->query();

        return $randomPaidGroup;
    }

    public function createMeet($meet, $parent)
    {
        if ( ! isset($meet['date_end']) ) {
            $meet['date_end'] = $meet['date_start'];
        }
        if ( isset($meet['any-time']) ) {
            $meet['any_time'] = $meet['any-time'];
        }
        if ( isset($meet['any_time']) && $meet['any_time'] ) {
            $meet['date_end'] = '';
        }
        if ( ! isset($meet['seats']) ) {
            $meet['seats'] = 1;
        }

        $db = Yii::app()->db;
        $columns = array(
            'name'        => $meet['name'],
            'description' => $meet['description'],
            'type'        => 'meetup',
            'date_start'  => $meet['date_start'],
            'date_end'    => $meet['date_end'],
            'city_id'     => $meet['city'],
            'address'     => $meet['address'],
            'seats'       => $meet['seats'],
            'date_created'=> date('Y-m-d H:i:s'),
            'parent'      => $parent,
        );
        $rowCount = $db->createCommand()
            ->insert('group', $columns);

        $albumName = $meet['name'];		
		$lastId = Yii::app()->db->getLastInsertId();
		$date = date('Y-m-d H:i:s');
		Photoalbum::model()->createAlbum($albumName, $parent, $lastId, $date);
		
        return $lastId;
    }

    public function getGroupComments($groupId)
    {
        return $this->getMeetComments($groupId);
    }

    public function getMeetComments($meetId)
    {
        $db = Yii::app()->db;

        $comments = $db->createCommand()
            ->select('comment.*, user.id AS sender_id, user.name AS sender_name')
            ->from('comment')
            ->join('user', 'comment.user_id = user.id')
            ->where('comment.meet_id = ' . (int)$meetId)
            ->order('date ASC')
            ->queryAll();

        return $comments;
    }

    public function getFormatedComments($groupId)
    {
        $records = $this->getMeetComments($groupId);

        if (!$records) {
            return $records;
        }

        $levels = array();
        foreach ($records as $record) {
            $depth = $record['depth'];
            if (!isset($levels[$depth])) {
                $levels[$depth] = array();
            }
            $levels[$depth][$record['id']] = $record;
        }

        for ($i = count($levels) - 1; $i > 0; $i--) {
            foreach ($levels[$i] as $comment) {
                $parentId = $comment['parent_id'];
                if (!isset($levels[$i-1][$parentId]['children'])) {
                    $levels[$i-1][$parentId]['children'] = array();
                }
                $levels[$i-1][$parentId]['children'][$comment['id']] = $comment;
            }
        }

        $result = $levels[0];

        return $result;
    }

    public function sentComment($targetId, $text, $userId, $parentId = 0)
    {
        $db = Yii::app()->db;

        $depth = 0;
        if ($parentId) {
            $depth = $db->createCommand()
                ->select('depth')
                ->from('comment')
                ->where('id = ' . (int)$parentId)
                ->queryScalar();
            $depth++;
        }

        $columns = array(
            'user_id'   => (int)$userId,
            'meet_id'   => (int)$targetId,
            'text'      => trim($text),
            'parent_id' => (int)$parentId,
            'depth'     => (int)$depth,
        );

        $rowCount = $db->createCommand()
            ->insert('comment', $columns);

        return $rowCount;
    }

    public function getUserGroupList($userId)
    {
        $db = Yii::app()->db;

        $groups = $db->createCommand()
            ->select('group.id, group.picture, group.name')
            ->from($this->tableName())
            ->join('member', 'member.group_id = group.id')
            ->where('member.user_id = ' . (int)$userId)
            ->queryAll();


        return $groups;
    }
	
    /**
     * Returns group administrator
     *
     * @param   int $groupId    Group id
     *
     * @return  mixed
     */
    public function getGroupAdmin($groupId)
    {
        $db = Yii::app()->db;

        $groupAdmin = $db->createCommand()
            ->select('user.*, user.id as user_id')
            ->from('user')
            ->join('group_admin', 'group_admin.user_id = user.id')
            ->where('group_admin.group_id = ' . (int)$groupId)
            ->queryRow();

        return $groupAdmin;
    }

    public function getUserAdminGroupList($userId)
    {
        $db = Yii::app()->db;

        $groups = $db->createCommand()
            ->select('group.id, group.picture, group.name')
            ->from($this->tableName())
            ->join('member', 'member.group_id = group.id')
            ->join('group_admin', 'group_admin.group_id = group.id')
            ->where('member.user_id = ' . (int)$userId . ' AND group_admin.user_id = ' . (int)$userId)
            ->queryAll();

        return $groups;
    }

    public function deleteMeetProposal($proposalId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->delete('meet_proposal', 'id=' . (int)$proposalId);

        return $success;
    }

    public function meetProposal($name, $place, $seats, $date, $anyTime, $comment, $userId, $groupId)
    {
        $db = Yii::app()->db;

        $columns = array(
            'name'  => $name,
            'place' => $place,
            'seats' => $seats,
            'date'  => $date,
            'any_time' => $anyTime,
            'comment'  => $comment,
            'user_id'  => (int)$userId,
            'group_id' => (int)$groupId,
            'prop_date' => Date('Y-m-d'),
        );

        $rowCount = $db->createCommand()
            ->insert('meet_proposal', $columns);

        return $rowCount;
    }

    public function getMeetProposals($groupId)
    {
        $db = Yii::app()->db;

        $proposals = $db->createCommand()
            ->select('meet.*, user.id AS user_id, user.name AS user_name')
            ->from('meet_proposal AS meet')
            ->join('user', 'user.id = meet.user_id')
            ->where('group_id = ' . (int)$groupId)
            ->order('date')
            ->queryAll();

        return $proposals;
    }

    public function getUserMeetsJson($userId)
    {
        $db = Yii::app()->db;

        $meets = $db->createCommand()
            ->select('group.id, group.name, group.description, group.date_start as date, group.date_end, group.parent')
            ->from($this->tableName())
            ->join('member', 'group.id = member.group_id')
            ->where('member.user_id = ' . (int)$userId . ' AND group.type = "meetup"')
            ->queryAll();

        return $meets;
    }

    public function getMeetGroupID($meetId)
    {
        $db = Yii::app()->db;

        $groupId = $db->createCommand()
            ->select('parent')
            ->from('group')
            ->where('id = ' . (int)$meetId)
            ->queryScalar();

        return $groupId;
    }

    public function getNextMeet($groupId)
    {
        $db = Yii::app()->db;

        $date = $db->createCommand()
            ->select('date_start')
            ->from('group')
            ->where('`parent` = ' . (int)$groupId . ' AND `date_start` > "' . date('Y-m-d H:i:s') . '"')
            ->order('date_start')
            ->queryScalar();

        return $date;
    }

    public function getGroupMeetsID($groupId)
    {
        $db = Yii::app()->db;

        $ids = $db->createCommand()
            ->select('id, date_start, name')
            ->from('group')
            ->where('`parent` = ' . (int)$groupId)
            ->queryAll();

        return $ids;
    }

    public function getFutureMeetsCount($groupId)
    {
        $db = Yii::app()->db;

        $count = $db->createCommand()
            ->select('COUNT(*)')
            ->from($this->tableName())
            ->where('`parent` = ' . (int)$groupId . ' AND `date_created` > "' . date('Y-m-d H:i:s') . '"')
            ->queryScalar();

        return $count;
    }

    public function getPastMeetsCount($groupId)
    {
        $db = Yii::app()->db;

        $count = $db->createCommand()
            ->select('COUNT(*)')
            ->from($this->tableName())
            ->where('`parent` = ' . (int)$groupId . ' AND `date_created` < "' . date('Y-m-d H:i:s') . '"')
            ->queryScalar();

        return $count;
    }

    public function contactAdmin()
    {

    }

    public function getGroupsWithKeywords($keywords)
    {
        foreach ($keywords as $i => $id) {
            $keywords[$i] = (int)$id;
        }

        $db = Yii::app()->db;

        $groups = $db->createCommand()
            ->select()
            ->from('group_keyword')
            ->where(array('in', 'field_id', $keywords))
            ->queryAll();

        $ids = array();
        foreach ($groups as $group) {
            $ids[] = $group['group_id'];
        }

        return $ids;
    }

    public function getModerators($groupId)
    {
        $db = Yii::app()->db;

        $moderators = $db->createCommand()
            ->select('group_moderators.*, user.name, user.avatar, user.email, user.id as moderator_id')
            ->from('group_moderators')
            ->leftJoin('user', 'user.id = group_moderators.user_id')
            ->where('group_id = ' . (int)$groupId)
            ->queryAll();

        return $moderators;
    }

    public function getModeratorsRequest($targetId, $token)
    {
        $db = Yii::app()->db;

        $typeId = $db->createCommand()
            ->select('id')
            ->from('group_moderators_requests_types')
            ->where('type = "add"')
            ->queryScalar();

        $request = $db->createCommand()
            ->select()
            ->from('group_moderators_requests')
            ->where(
                'target_id = ' . (int)$targetId . ' AND token = :token AND type_id = ' . (int)$typeId,
                array(':token'=>$token)
            )->queryRow();

        return $request;
    }

    public function getModeratorsRequests($groupId)
    {
        $db = Yii::app()->db;

        $select = 'requests.*, '
            . ' user.id AS target_id, user.name AS target_name, user.avatar AS target_avatar, '
            . ' types.type';
        $moderators = $db->createCommand()
            ->select($select)
            ->from('group_moderators_requests AS requests')
            ->leftJoin('user', 'user.id = requests.target_id')
            ->leftJoin('group_moderators_requests_types AS types', 'types.id = requests.type_id')
            ->where('group_id = ' . (int)$groupId . ' AND target_agreed = 1')
            ->queryAll();

        return $moderators;
    }

    public function deleteModeratorRequests($groupId, $targetId)
    {
        $db = Yii::app()->db;

        $condition = 'group_id = ' . (int)$groupId . ' AND target_id = ' . (int)$targetId;
        $success = $db->createCommand()
            ->delete('group_moderators_requests', $condition);

        return $success;
    }

    public function deleteModRequestsByToken($targetId, $token)
    {
        $db = Yii::app()->db;

        $groupId = (int)$db->createCommand()
            ->select('group_id')
            ->from('group_moderators_requests')
            ->where('token = :token', array(':token'=>$token))
            ->queryScalar();

        $success = 0;
        if ($groupId) {
            $success = $this->deleteModeratorRequests($groupId, $targetId);
        }

        return $success;
    }

    public function satisfyModRequestByToken($targetId, $token)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from('group_moderators_requests')
            ->where('target_id = ' . (int)$targetId . ' AND token = :token', array(':token'=>$token))
            ->queryRow();

        $type = $db->createCommand()
            ->select('type')
            ->from('group_moderators_requests_types')
            ->where('id = ' . (int)$request['type_id'])
            ->queryScalar();

        $this->satisfyModeratorRequest($type, (int)$request['group_id'], $targetId);
    }

    public function satisfyModeratorRequest($type, $groupId, $targetId)
    {
        $db = Yii::app()->db;

        $typeId = $db->createCommand()
            ->select('id')
            ->from('group_moderators_requests_types')
            ->where('type = :type', array(':type' => $type))
            ->queryScalar();

        $this->deleteOldModeratorsRequests($typeId);

        $request = $db->createCommand()
            ->select()
            ->from('group_moderators_requests')
            ->where(
                'type_id = ' . (int)$typeId
                . ' AND group_id = ' . (int)$groupId
                . ' AND target_id = ' . (int)$targetId
            )->queryRow();

        switch ($type) {
            case 'add':
                $success = 0;
                if ($request['target_agreed'] and $request['admin_approved']) {
                    $success = $this->addModerator($groupId, $targetId);
                }
                break;
            case 'fire':
                $success = $this->fireModerator($groupId, $targetId);
                break;
            default:
                return 0;
        }

        if (!$typeId || !$success) {
            return 0;
        }

        $this->deleteModeratorRequests($groupId, $targetId);

        return $success;
    }

    public function makeModeratorRequest($type, $groupId, $targetId, $senderId)
    {
        $db = Yii::app()->db;

        $typeId = $db->createCommand()
            ->select('id')
            ->from('group_moderators_requests_types')
            ->where('type = :type', array(':type'=>$type))
            ->queryScalar();

        $targetAgreed = 0;
        if ($type == 'fire') {
            $targetAgreed = 1;
        }

        $columns = array(
            'group_id' => (int)$groupId,
            'target_id' => (int)$targetId,
            'sender_id' => (int)$senderId,
            'type_id' => (int)$typeId,
            'target_agreed' => (int)$targetAgreed,
        );

        $success = $db->createCommand()
            ->insert('group_moderators_requests', $columns);

        $id = $db->getLastInsertId();
        if ($id) {
            $token = md5($id . $groupId . $targetId . $senderId . time());
            $db->createCommand()
                ->update(
                    'group_moderators_requests',
                    array('token'=>$token),
                    'id = ' . (int)$id
                );
        }

        return $id;
    }

    public function getModeratorRequestToken($id)
    {
        $db = Yii::app()->db;

        $token = $db->createCommand()
            ->select('token')
            ->from('group_moderators_requests')
            ->where('id = ' . (int)$id)
            ->queryScalar();

        return $token;
    }

    public function addModerator($groupId, $userId)
    {
        $groupId = (int)$groupId;
        $userId  = (int)$userId;

        $db = Yii::app()->db;

        $columns = array(
            'group_id' => $groupId,
            'user_id'  => $userId,
        );

        $success = $db->createCommand()
            ->insert('group_moderators', $columns);

        if ($success) {
            $targetEmail = User::model()->getUserEmail($userId);
            $group = Group::model()->getGroup($groupId);

            //Notice::addNotice($userId, 'notice', $noticeMessage);
			$link = '<br><a href="'. Yii::app()->createUrl('group/view', array('id'=>$groupId, '#'=>'newmoderator')) . '">'.$group['name'].'</a>';
			$noticeMessage =  'Вы назначены модератором группы %s';
			$noticeMessageGroup = 'Вы назначены модератором группы %2$s';
			Notice::addNoticeCheckSimilar($parentUser->id, 'notice', $noticeMessage, $noticeMessageGroup, $link);

            $noticeMessage = 'Вы назначены модератором группы ' . $link;
            $subject = 'Вы назначены модератором группы ' . $group['name'];

            $href = Yii::app()->createAbsoluteUrl('group/view', array('id'=>$groupId));
            $emailMessage = $noticeMessage . '<a href="' . $href . '" target="_blank">'
                . 'Перейти на сайт.</a>';

            //mail($targetEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($targetEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }

        return $success;
    }

    public function fireModerator($groupId, $userId)
    {
        $db = Yii::app()->db;

        $condition = 'group_id = ' . (int)$groupId . ' AND user_id = ' . (int)$userId;
        $success = $db->createCommand()
            ->delete('group_moderators', $condition);

        $this->deleteModeratorRequests($groupId, $userId);

        return $success;
    }

    public function findMembers($username, $groupId)
    {
        $db = Yii::app()->db;

        $members = $db->createCommand()
            ->select('user.id, user.name')
            ->from('member')
            ->leftJoin('user', 'user.id = member.user_id')
            ->leftJoin('group_moderators AS moderator', 'moderator.user_id = user.id AND moderator.group_id = member.group_id')
            ->where(
                array(
                    'and',
                    'member.group_id = ' . (int)$groupId,
                    'member.confirm = 1',
                    'moderator.id is NULL',
                    array('like', 'user.name', '%' . $username . '%')
                )
            )->queryAll();

        return $members;
    }

    public function adminApproveModerator($type, $groupId, $targetId)
    {
        $db = Yii::app()->db;

        $typeId = $db->createCommand()
            ->select('id')
            ->from('group_moderators_requests_types')
            ->where('type = :type', array(':type'=>$type))
            ->queryScalar();

        $db->createCommand()
            ->update(
                'group_moderators_requests',
                array('admin_approved'=>1),
                'type_id = ' . (int)$typeId . ' AND group_id = ' . (int)$groupId
                . ' AND target_id = ' . (int)$targetId
            );

        $this->satisfyModeratorRequest($type, $groupId, $targetId);
    }

    public function moderatorAgree($targetId, $token)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->update(
                'group_moderators_requests',
                array('target_agreed'=>1),
                'token = :token AND target_id = ' . (int)$targetId,
                array(':token'=>$token)
            );

        $this->satisfyModRequestByToken($targetId, $token);

        return $success;
    }

    public function isModeratorRequestActual($token)
    {
        $db = Yii::app()->db;

        $this->deleteOldModeratorsRequests();

        $result = $db->createCommand()
            ->select()
            ->from('group_moderators_requests')
            ->where('token = :token', array(':token'=>$token));

        return (bool)$result;
    }

    public function deleteOldModeratorsRequests($typeId = 0)
    {
        $db = Yii::app()->db;

        if (!$typeId) {
            $typeId = $db->createCommand()
                ->select('id')
                ->from('group_moderators_requests_types')
                ->where('type = "add"')
                ->queryScalar();
        }

        $db->createCommand()
            ->delete(
                'group_moderators_requests',
                '(date < (NOW() - INTERVAL 24 HOUR)) AND type_id = ' . (int)$typeId
            );
    }

    public function delegateGroup($groupId, $newUserId, $oldUserId)
    {
        $db = Yii::app()->db;
        $success = $db->createCommand()
            ->update(
                'group_admin',
                array('user_id'=>(int)$newUserId),
                'group_id = ' . (int)$groupId . ' AND user_id=' . (int)$oldUserId
            );

        return $success;
    }

    public function makeDelegateRequest($groupId, $targetId, $senderId)
    {
        $db = Yii::app()->db;

        $columns = array(
            'group_id' => (int)$groupId,
            'target_id' => (int)$targetId,
            'sender_id' => (int)$senderId,
        );

        $success = $db->createCommand()
            ->insert('group_delegate_requests', $columns);

        $id = $db->getLastInsertId();
        if ($id) {
            $token = md5($id . $groupId . $targetId . $senderId . time());
            $db->createCommand()
                ->update(
                    'group_delegate_requests',
                    array('token'=>$token),
                    'id = ' . (int)$id
                );
            return $id;
        } else {
            return 0;
        }
    }

    public function getDelegateRequest($groupId)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from('group_delegate_requests')
            ->where('group_id = ' . (int)$groupId)
            ->queryRow();

        return $request;
    }

    public function getDelegateRequestByToken($targetId, $token)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from('group_delegate_requests')
            ->where(
                'target_id = ' . (int)$targetId . ' AND token = :token',
                array(':token'=>$token)
            )->queryRow();

        return $request;
    }

    public function getDelegateRequestToken($requestId)
    {
        $db = Yii::app()->db;

        $token = $db->createCommand()
            ->select('token')
            ->from('group_delegate_requests')
            ->where('id = ' . (int)$requestId)
            ->queryScalar();

        return $token;
    }

    public function satisfyDelegateRequest($targetId, $token)
    {
        $db = Yii::app()->db;

        $request = $db->createCommand()
            ->select()
            ->from('group_delegate_requests')
            ->where(
                'target_id = ' . (int)$targetId . ' AND token = :token',
                array(':token' => $token)
            )->queryRow();

        if ($request) {
            $groupId = (int)$request['group_id'];
            $oldUserId = (int)$request['sender_id'];
            $success = $this->delegateGroup($groupId, $targetId, $oldUserId);
        }

        if (empty($success)) {
            return 0;
        }

        $this->deleteDelegateRequests($groupId);

        return $success;
    }

    public function deleteDelegateRequests($groupId)
    {
        $db = Yii::app()->db;

        $success = 0;
        if ($groupId) {
            $condition = 'group_id = ' . (int)$groupId;// . ' AND target_id = ' . (int)$targetId;
            $success = $db->createCommand()
                ->delete('group_delegate_requests', $condition);
        }

        return $success;
    }

    public function deleteDelegateRequestsByToken($targetId, $token)
    {
        $db = Yii::app()->db;

        $groupId = (int)$db->createCommand()
            ->select('group_id')
            ->from('group_delegate_requests')
            ->where('token = :token AND target_id = ' . (int)$targetId, array(':token'=>$token))
            ->queryScalar();

        $success = 0;
        if ($groupId) {
            $success = $this->deleteDelegateRequests($groupId);
        }

        return $success;
    }

    public function deleteOldDelegateRequests()
    {
        $db = Yii::app()->db;

        $db->createCommand()
            ->delete('group_delegate_requests', 'date < NOW() - INTERVAL 24 HOUR');
    }

    public function isGroupDelegated($groupId)
    {
        $db = Yii::app()->db;

        $this->deleteOldDelegateRequests();

        $request = $db->createCommand()
            ->select()
            ->from('group_delegate_requests')
            ->where('group_id = ' . (int)$groupId)
            ->queryRow();

        return (bool)$request;
    }

    public function savePremoderation($mode, $limit)
    {
        $db = Yii::app()->db;

        $columns = array('value' => (int)$mode);
        $successFirst = $db->createCommand()
            ->update('admin', $columns, '`key` = "group_premoderation_mode"');

        $columns = array('value' => (int)$limit);
        $successSecond = $db->createCommand()
            ->update('admin', $columns, '`key` = "group_premoderation_limit"');

        return (bool)$successFirst && (bool)$successSecond;
    }

    public function getPremoderationSettings()
    {
        $db = Yii::app()->db;

        $mode = $db->createCommand()
            ->select('value')
            ->from('admin')
            ->where('`key` = "group_premoderation_mode"')
            ->queryScalar();
        $limit = $db->createCommand()
            ->select('value')
            ->from('admin')
            ->where('`key` = "group_premoderation_limit"')
            ->queryScalar();

        $result = array(
            'mode'  => $mode,
            'limit' => $limit,
        );

        return $result;
    }

    public function getPremoderationQueue()
    {
        $db = Yii::app()->db;
		/* $limit = $db->createCommand()
            ->select('value')
            ->from('admin')
            ->where('`key` = "group_premoderation_limit"')
            ->queryScalar(); */
        $groups = $db->createCommand()
            ->select('group.*, user.id AS admin_id, user.name AS admin_name')
            ->from('group')
            ->join('group_admin', 'group.id = group_admin.group_id')
            ->join('user', 'group_admin.user_id = user.id')
            ->where('group.approved = 0')
			//->limit($limit)
            ->queryAll();

        return $groups;
    }

    public function approveGroup($groupId)
    {
        $db = Yii::app()->db;

        $success = $db->createCommand()
            ->update('group', array('approved' => 1), 'id = ' . (int)$groupId);

        return $success;
    }

    public static function getHoursMinutes()
    {
        $hours = array();
        for ( $i = 0; $i < 24; $i++ ) {
        $number =  $i < 10 ? '0' . $i : $i;
        $hours[] = $number;
        }
        $minutes = array();
        for ( $i = 0; $i < 60; $i++ ) {
            $number =  $i < 10 ? '0' . $i : $i;
            $minutes[] = $number;
        }
        return array('hours' => $hours, 'minutes' => $minutes);
    }
	
	/* 
	public function getMeetGroupID($meetId)
    {
        $db = Yii::app()->db;

        $groupId = $db->createCommand()
            ->select('parent')
            ->from('group')
            ->where('id = ' . (int)$meetId)
            ->queryScalar();

        return $groupId;
    }

    public function getNextMeet($groupId)
    {
        $db = Yii::app()->db;

        $date = $db->createCommand()
            ->select('date_start')
            ->from('group')
            ->where('`parent` = ' . (int)$groupId . ' AND `date_start` > "' . date('Y-m-d H:i:s') . '"')
            ->order('date_start')
            ->queryScalar();

        return $date;
    }

    public function getGroupMeetsID($groupId)
    {
        $db = Yii::app()->db;

        $ids = $db->createCommand()
            ->select('id, date_start, name')
            ->from('group')
            ->where('`parent` = ' . (int)$groupId)
            ->queryAll();

        return $ids;
    }
	*/
	
    public function getAllUsersGroupsIds($userId)
    {
        $db = Yii::app()->db;

        $idsAdmin = $db->createCommand()
            ->select('group.id')
            ->from($this->tableName())
            ->join('group_admin', 'group_admin.group_id = group.id')
            ->where('group_admin.user_id = ' . (int)$userId)
            ->queryColumn();
        $idsModerator = $db->createCommand()
            ->select('group.id')
            ->from($this->tableName())
            ->join('group_moderators', 'group_moderators.group_id = group.id')
            ->where('group_moderators.user_id = ' . (int)$userId)
            ->queryColumn();
        $ids = array_merge($idsAdmin, $idsModerator);
		return $ids;
    }

    public function getMeetsIdsWhereUserIsAdmin($ids)
    {
        $db = Yii::app()->db;
		$ids = implode(',',array_values($ids));
        $summury = array(0);
		if((int)$ids >= 1){
		$ids = $db->createCommand()
            ->select('group.id')
            ->from($this->tableName())
            ->where('parent IN (' . $ids .')')
            ->queryColumn();
			$summury = $ids;
		}
		return $summury;
		
    }
	protected function message($message){
		//$message = 'На Вас жалоба. <a href="#">Просмотреть</a>';
		return Yii::app()->controller->renderPartial('//layouts/mail_template',array('message'=>$message),true);
	}

}
