<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $serv_id
 * @property string $service
 * @property string $name
 * @property string $reg_date
 * @property string $email
 * @property string $pass
 * @property integer $answer_comment_notice
 */
class User extends CActiveRecord
{
    public $newPssword = '';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, pass, email, reg_date', 'required'),
			array('serv_id, sex, city_id, country_id, region_id', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'min'=>3),
            array('pass', 'length', 'min'=>6),
            array('service, city, birthday', 'length', 'max'=>50),
			array('name, email', 'length', 'max'=>100),
			array('email','email'),
			array('pass', 'length', 'max'=>32),
            array('about', 'length', 'max'=>500),
            array('avatar', 'length', 'max'=>200, 'on'=>'insert,update'),
			array('name', 'filter', 'filter'=>function($value){return htmlspecialchars(trim($value));}),
            array('avatar', 'file','types'=>'jpg, gif, png, jpeg', 'allowEmpty'=>true, 'on'=>'update'),
            array('name','unique','message' => 'Пользователь с таким именем уже существует'),
            array('email','unique','message' => 'Пользователь с таким e-mail уже существует'),
          /*  array('name', 'match', 'pattern' => '/^[A-Za-z][A-Za-z0-9_]+$/u',
                'message' => 'Некоректные символы. Используйте, пожалуйста, латинский алфавит и цифры (A-z0-9)'),*/
            array('pass', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u',
                'message' => 'Некоректные символы. Используйте, пожалуйста, латинский алфавит и цифры (A-z0-9)'),
            array('birthday', 'match', 'pattern' => '/^(\d\d\d\d-\d\d-\d\d)?$/u',
                'message' => 'Формат даты: дд-мм-гггг.'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, serv_id, service, name, reg_date, email, pass, city, birthday, sex, avatar, about, city_id, country_id, region_id', 'safe', 'on'=>'search'),
		);
	}

    function translit($text) {

        $converter = array(

            'а' => 'a',   'б' => 'b',   'в' => 'v',

            'г' => 'g',   'д' => 'd',   'е' => 'e',

            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',

            'и' => 'i',   'й' => 'y',   'к' => 'k',

            'л' => 'l',   'м' => 'm',   'н' => 'n',

            'о' => 'o',   'п' => 'p',   'р' => 'r',

            'с' => 's',   'т' => 't',   'у' => 'u',

            'ф' => 'f',   'х' => 'h',   'ц' => 'c',

            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',

            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',

            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',

            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',

            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',

            'И' => 'I',   'Й' => 'Y',   'К' => 'K',

            'Л' => 'L',   'М' => 'M',   'Н' => 'N',

            'О' => 'O',   'П' => 'P',   'Р' => 'R',

            'С' => 'S',   'Т' => 'T',   'У' => 'U',

            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',

            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',

            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',

            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

        );



           return strtr($text,$converter);
    }


    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'serv_id' => 'ID сервиса',
			'service' => 'Сервис',
			'name' => 'Имя',
			'reg_date' => 'Дата регистрации',
			'email' => 'Email',
			'pass' => 'Пароль',
			'newPssword' => 'Введите новый пароль',
            'city' => 'Город',
            'birthday' => 'День рождения',
            'sex' => 'Пол',
            'avatar' => 'Аватар',
            'status' => 'Статус',
            'about' => 'Обо мне',
            'answer_comment_notice' => 'Получать уведомления об ответах на комментарии',
			'city_id' => 'Город', 
			'region_id' => 'Регион',
			'country_id' => 'Страна',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($banned)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('serv_id',$this->serv_id);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('reg_date',$this->reg_date,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pass',$this->pass,true);
        $criteria->compare('city',$this->city,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('region_id',$this->region_id);
		$criteria->compare('country_id',$this->country_id);
        $criteria->compare('birthday',$this->birthday,true);
        $criteria->compare('sex',$this->sex,true);
        $criteria->compare('avatar',$this->avatar,true);
        $criteria->compare('about',$this->about,true);
        $criteria->compare('answer_comment_notice',$this->answer_comment_notice,true);
        if($banned){
            $criteria->addCondition('status="0"');
        }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>array(
                'defaultOrder'=>'id DESC',
            )

        ));
	}

    public function save($runValidation=true,$attributes=null)
    {	$userId = $this->getUserId();
		$isAdmin = Admin::model()->isSiteAdmin($userId);		
		$hostInfo = Yii::app()->request->hostInfo;
		$hostInfoA = explode('//',$hostInfo);
		$hostName = $hostInfoA[1];
        if ($this->isNewRecord) {
			$this->pass = $_POST['User']['pass'];
		}else{
			$appUserId = Yii::app()->user->id;
			if(!empty($_POST['User']['pass']) && !empty($_POST['User']['newPssword'])){
				$user = self::model()->findByAttributes(array('pass'=>md5($_POST['User']['pass'])));
				if($user===null){ 
					Notice::addNotice($appUserId, 'error', 'Пароль не изменен, возможно вы указали неправильное значение в поле "старый пароль"');
					return false;
				}
				if($user->pass == md5($_POST['User']['pass'])){
					$message = "Ваш пароль на сайте $hostName изменен. Ваш логин: <b>".$_POST['User']['name'].'</b>, пароль: <b>'.$_POST['User']['pass'].'</b>';
					Notice::addNotice($appUserId, 'success', 'Пароль изменен');
					$this->pass = md5($_POST['User']['newPssword']);
				}
				
			}elseif($isAdmin && !empty($_POST['User']['newPssword'])){
				//fb($this->pass,'pass1');
				//fb($_POST);//exit;
				$this->pass = md5($_POST['User']['newPssword']);
				Notice::addNotice($appUserId, 'success', 'Пароль успешно изменен');
				
			}
		}
				//fb($this->pass,'pass2');
				//exit;
        if(!$runValidation || $this->validate($attributes)) {	
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($this->email, $this->name);
            
			//fb($this->email,'model email');
			//fb($_POST,'fack');exit;
			if ($this->getIsNewRecord()) {
                $this->pass = md5($this->pass);
                $result = $this->insert($attributes);
				//отправляем письмо о регистрации
				$mail->Subject = 'Регистрация на сайте '.$hostName;
				$content = "Вы успешно зарегистрировались на сайте $hostName. Ваш логин: <b>".$_POST['User']['name'].'</b>, пароль: <b>'.$_POST['User']['pass'].'</b>';
				$mail->MsgHTML($this->message($content));$mail->send();
            } else {
                $result = $this->update($attributes);
				//отправляем письмо о регистрации
				$mail->Subject = 'Изменение в профиле на '.$hostName;
				$content = isset($message) ? $message : "Ваши данные на сайте $hostName изменены.";
				$mail->MsgHTML($this->message($content));$mail->send();
            }
            return $result;
        } else {
            return false;
        }
    }
	/* protected function afterValidate()
	{
        if ($this->getIsNewRecord()) {
			$this->pass = md5($_POST['User']['pass']);
		}else{
			
		}
		return parent::afterValidate();
	} */

    public function registration($name, $email, $password)
    {
        $db = Yii::app()->db;

        $columns = array(
            'name'     => $name,
            'reg_date' => Date('Y-m-d H:i:s'),
            'email'    => $email,
            'pass'     => md5($password),
            'status'   => 1,
        );
        $db->createCommand()
            ->insert($this->tableName(), $columns);

        $insertedId = $db->getLastInsertId();

        return $insertedId;
    }

    public function authorization($name, $password)
    {
		$value = User::model()->findAllByAttributes(array("name" => $name, "pass" => md5($password)));

        return $value;
    }

    public function serviceRegistrationUser($servId, $serviceName, $name,$email = null, $file = null, $gender = 0,$city='',$city_id=0,$region_id=0,$country_id=0,$birthday)
    {
        $db = Yii::app()->db;


        $columns = array(
            'serv_id' => $servId,
            'service' => $serviceName,
            'name' => $name,
            'email' =>$email,
            'sex' =>$gender,
            'city' =>$city,
            'city_id' =>$city_id,
            'region_id' =>$region_id,
            'country_id' =>$country_id,
            'birthday' =>$birthday,
            'reg_date' => Date('Y-m-d H:i:s'),
        );
        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        if($file != null) {

            $users = User::model()->findByAttributes(array('serv_id' =>$servId,'service' => $serviceName));
            $users->avatar = $this->uploadAvatar($users->id, $file, true);

            $users->save('true', array('avatar'));

        }



        return $rowCount;
    }

    public function registrationUser($servId, $serviceName, $name)
    {
        $db = Yii::app()->db;

        $columns = array(
            'serv_id' => $servId,
            'service' => $serviceName,
            'name' => $name,
            'reg_date' => Date('Y-m-d H:i:s'),
        );
        $rowCount = $db->createCommand()
            ->insert($this->tableName(), $columns);

        return $rowCount;
    }

    public function getPicture($userId)
    {
        $db = Yii::app()->db;

        $picture = $db->createCommand()
            ->select('avatar')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        return $picture;
    }

    public function getId($servId, $service)
    {
        $db = Yii::app()->db;

        $id = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where(
                '`serv_id` = :servId AND `service` = :service',
                array(':servId'=>$servId,':service'=>$service)
            )->queryScalar();

        if (!$id) {
            $id = 0;
        }

        return $id;
    }

    public function getName($userId)
    {
        $db = Yii::app()->db;

        $name = $db->createCommand()
            ->select('name')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        return $name;
    }

    public function getAbout($userId)
    {
        $db = Yii::app()->db;

        $about = $db->createCommand()
            ->select('about')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        return $about;
    }

    public function getUserName($userId)
    {
        $db = Yii::app()->db;

        $username = $db->createCommand()
            ->select('name')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        return $username;
    }

    public function uploadAvatar($id, $path, $soc = null) {

        $uploaddir = 'images/user/';
        $filename = $id;
        $fileext = pathinfo($path, PATHINFO_EXTENSION);

        if(empty($fileext) OR $soc != null) {

            $fileext = (!empty($fileext))?$fileext :'png';
            $rezName = UploadImg::model()->upload_from_soc($uploaddir,$path, 'User', 'avatar', 205, 205,  $filename, false);

        } else {

            $rezName = UploadImg::model()->upload_image($uploaddir, 'User', 'avatar', 205, 205,  $filename, false);

        }




        return $filename.'.'.$fileext;

}

    public function getUserId()
    {
        if (isset(Yii::app()->user->service)) {
            $userId = User::model()->getId(Yii::app()->user->id, Yii::app()->user->service);
        } else {
            $userId = Yii::app()->user->id;
        }
        if (!Yii::app()->user->id) {
            $userId = 0;
        }

        if($userId) {

            $email = $this->getUserEmail($userId);
            if(strlen($email) == 0) {
				if(!$this->email){
					$message = Yii::t('var', 'Для завершения регистрации укажите адрес вашей электронной почты  в настройках  аккаунта');
					Yii::app()->user->setFlash('error', $message);
				}
                //$url = Yii::app()->createUrl('/user/update/'.$userId);

                if(Yii::app()->controller->id != 'user' OR Yii::app()->controller->action->id !='update') {

                    Yii::app()->controller->redirect( '/'.Yii::app()->language.'/user/update/'.$userId);
                }



            //    CController::redirect( Yii::app()->createUrl('/user/update/'.$userId));
            }
        }
        return $userId;
    }

    public function getUserServiceID($userId)
    {
        $db = Yii::app()->db;

        $serviceId = $db->createCommand()
            ->select('serv_id')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        if (!$serviceId) {
            $serviceId = 0;
        }

        return $serviceId;
    }

    public function getUserService($userId)
    {
        $db = Yii::app()->db;

        $service = $db->createCommand()
            ->select('service')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        if (!$service) {
            $service = 0;
        }

        return $service;
    }

    public function getFacebookUserID($userId)
    {
        $db = Yii::app()->db;

        $facebookId = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where(
                array(
                    'and',
                    'service = "facebook"',
                    array('in', 'serv_id', $userId)
                )
            )->queryScalar();

        return $facebookId;
    }

    public function getFacebookUsers()
    {
        $db = Yii::app()->db;

        $users = $db->createCommand()
            ->select('id, name, serv_id')
            ->from($this->tableName())
            ->where('`service` = "facebook"')
            ->queryAll();

        return $users;
    }

    public function getVKUserID($userId)
    {
        $db = Yii::app()->db;

        $vkUserId = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where(
                array(
                    'and',
                    'service = "vkontakte"',
                    array('in', 'serv_id', $userId)
                )
            )->queryScalar();

        return $vkUserId;
    }

    public function getUserEmail($userId)
    {
        $db = Yii::app()->db;

        $email = $db->createCommand()
            ->select('email')
            ->from($this->tableName())
            ->where('id = ' . (int)$userId)
            ->queryScalar();

        return $email;
    }

    public function getStringAllUsersWithEmail()
    {
        $db = Yii::app()->db;

        $idsArray = $db->createCommand()
            ->select('id')
            ->from($this->tableName())
            ->where('email <> ""')
            ->queryAll();

        $array = array();
        foreach($idsArray as $id){
            $array[] = $id['id'];
        }
        return implode(',',$array);
    }

    public function findUsers($usernamePiece, $limit = 15)
    {
        $db = Yii::app()->db;

        $users = $db->createCommand()
            ->select('user.id, user.name, user.email')
            ->from('user')
            ->where( array('like', 'user.name', '%' . $usernamePiece . '%') )
            ->order('user.name ASC')
            ->limit((int)$limit)
            ->queryAll();

        return $users;
    }
	protected function message($message){
		//$message = 'На Вас жалоба. <a href="#">Просмотреть</a>';
		return Yii::app()->controller->renderPartial('//layouts/mail_template',array('message'=>$message),true);
	}
}
