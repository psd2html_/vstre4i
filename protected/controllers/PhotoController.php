<?php

class PhotoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','upload'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
	
	
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Photo']))
		{
		
			//create album if not exist
			
			$model=new Photo;		
			$model->attributes=$_POST['Photo'];

            $uploaddir = 'images/album/'.$model->album_id.'/';
            $uploadfile = $uploaddir . basename($_FILES['Photo']['name']['link']);
            $file_from = $_FILES['Photo']['tmp_name']["link"];
            if (move_uploaded_file($file_from,$uploadfile)) {
                Photo::model()->photoUpload($_POST['Photo']['album_id']['link'],$_POST['Photo']['user_id']['link'],$_FILES['Photo']['name']['link']);
            }

            /*foreach ($_FILES["pictures"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    $tmp_name = $_FILES["pictures"]["tmp_name"][$key];
                    $name = $_FILES["pictures"]["name"][$key];
                    move_uploaded_file($tmp_name, "$uploads_dir/$name");
                }
            }*/


			/*if($model->save())
				$this->redirect(array('view','id'=>$model->id));*/
            header('Location: '.Yii::app()->createUrl('site/index').'/group/photos/'.$_POST['Photo']['album_id']['link'] );
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Photo']))
		{
			$model->attributes=$_POST['Photo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        if($model->link){
            unlink(YiiBase::getPathOfAlias("webroot.images.album")."/".$model->album_id."/".$model->link);
        }
        $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		/* if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); */
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Photo');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $model=new Photo('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Photo']))
            $model->attributes=$_GET['Photo'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Photo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Photo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Photo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='photo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionUpload()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['album_id'])) {
            header("Content-Type: text/plain");
			$filename = UploadImg::model()->upload_image('images/album/'.$_POST['album_id'].'/');
            $photo = new Photo();
            $photo->album_id = $_POST['album_id'];
            $photo->user_id =  User::model()->getUserId();
			
            $photo->link = $filename;
            $photo->date = date('Y-m-d');
            if ($photo->save()){
				/* $group_id = Photoalbum::model()->getAlbumsGroupId($photo->album_id);
				$group = Group::model()->getGroup($group_id);
				if($group['photo_notice']){
					$admin = Group::model()->getGroupAdmin($group_id);
					$user = User::model()->getName($photo->user_id);
					$userLink = '<a href="'. Yii::app()->createAbsoluteUrl('user/view', array('id'=>$user->id)) . '">'.$user->name.'</a>';
					$groupLink = '<a href="'. Yii::app()->createAbsoluteUrl('user/view', array('id'=>$group_id)) . '">'.$group['name'].'</a>';
					$mail = new JPhpMailer;
					$mail->setFrom(Yii::app()->params['adminEmail'], '');
					$mail->addAddress($admin['email'], '');
					$mail->Subject = "Новое фото в группе ".$group['name'];
					$content = 'Пользователь '.$userLink.' загрузил новое фото в группу '.$groupLink;
					$mail->MsgHTML($this->message($content));
					$noticeMessage = 'Новое фото в группе %s';
					$noticeMessageGroup = '%d новых фото в группе %s';
					$link = '<a href="'. Yii::app()->createAbsoluteUrl('user/view', array('id'=>$group_id, '#'=>'newPhoto')) . '">'.$group['name'].'</a>';
					Notice::addNoticeCheckSimilar($admin['user_id'], 'error', $noticeMessage, $noticeMessageGroup, $link);
					$moderators = Group::model()->getModerators($group_id);
					foreach($moderators as $moderator){
						$mail->addAddress($moderator['email'], $moderator['name']);
						Notice::addNoticeCheckSimilar($moderator['moderator_id'], 'error', $noticeMessage, $noticeMessageGroup, $link);
					}			
					
					$mail->send();
				} */
                echo CJSON::encode(array('success' => true, 'filename' => $filename));
				
            }else
                echo CJSON::encode(array('success' => false));
        }
    }
}
