<?php

class RegionController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2admin';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }


    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('index','view','getcities','getregionlist','autocomplete'),
//                'users'=>array('*'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('create','update'),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete'),
//                'users'=>array('admin'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('row_id'=>$id,'table_name'=>'region'));

        foreach($translateModels as $translate){
            $model->{$translate->language} = $translate->value;
        }

        $this->render('view',array(
            'model'=>$model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Region();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Region']))
        {
            $model->attributes=$_POST['Region'];
            if($model->save()){
                foreach ($_POST['translate'] as $lang=>$translate){
                    $trModel = new Translate();
                    $trModel->row_id = $model->id;
                    $trModel->language = $lang;
                    $trModel->value = $translate;
                    $trModel->table_name = 'region';
                    $trModel->save(false);
                }
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }
        $categories = array();
        $categoriesModel = Category::model()->findAll();
        foreach($categoriesModel as $category){
            $categories[$category->id] = $category->name;
        }
        $this->render('create',array(
            'model'=>$model,
            'trModels' => $trModels,
            'categories' => $categories
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('table_name'=>'region','row_id'=>$id));
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        foreach($translateModels as  $translate){
            if(array_key_exists($translate->language,$languages)){
                unset($languages[$translate->language]);
                $trModels[$translate->language] = $translate;
            }
        }


        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Region']) && isset($_POST['translate']))
        {

            foreach ($_POST['translate'] as $lang=>$translate){
                $trModel = Translate::model()->findByAttributes(array('table_name'=>'region','row_id'=>$id,'language'=>$lang));
                if(!$trModel){
                    $trModel = new Translate();
                    $trModel->row_id = $id;
                }
                $trModel->language = $lang;
                $trModel->value = $translate;
                $trModel->table_name = 'region';
                $trModel->save();
            }
            $model->attributes=$_POST['Region'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'trModels' => $trModels,
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Region');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->layout='//layouts/column2';
        $model=new Region('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Region']))
            $model->attributes=$_GET['Region'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Region the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Region::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Region $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='region-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    public function actionGetRegions()
    {
        $country_id = (int)Yii::app()->request->getParam('country_id', 0);
        if (Yii::app()->language != 'ru') {
            $criteria = new CDbCriteria();
            $criteria->alias = 'region';
            $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
            $criteria->join = 'LEFT JOIN `translate` ON `region`.`id` = `translate`.`row_id`';
            $criteria->condition = "`translate`.`table_name`='region' AND `region`.`country_id`='".(int)$country_id."' AND `translate`.`language`='".Yii::app()->language."'";
            $regions = Region::model()->findAll($criteria);
        } else {
            $regions = Region::model()->findAllByAttributes(array('country_id' => $country_id));
        }
        if ($regions != null)
            for ($i=0;$i<count($regions);$i++) {
                if ($i == 0)
                    echo '<option value="">Выберите регион</option>';
                echo '<option value="'.CHtml::encode($regions[$i]->id).'">'.CHtml::encode($regions[$i]->name).'</option>';
            }
    }

    public function actionGetRegionList()
    {
        $result = '';
        $dataReader = Region::model()->getRegionList($_GET['country_id']);
        foreach ($dataReader as $val) {
            $result .= CHtml::encode($val['name']).',';
        }
        $result = substr($result,0,strlen($result)-1);
        echo $result;
    }

    public function actionAutocomplete()
    {
        //fb($_GET, 'ggggggggggggg');
		/* $criteria = new CDbCriteria();
		$criteria->select='name as value, id as data';
		if (!empty($_GET['country_id'])){
			$criteria->condition='country_id=:country_id';
			$criteria->params=array(':country_id'=>$_GET['country_id']);
		}
		$regions = Region::model()->findAll($criteria); */
		//$sql = 'select name as value, id as data from region where country_id=:country_id';
		$db = Yii::app()->db;
		$command = $db->createCommand();
		$command->select('region.name AS value, region.id AS data');
		$command->from('region');
		if(!empty($_GET['country_id']))
			$command->where('name LIKE :name AND country_id=:country_id', array(':country_id'=>$_GET['country_id'], 'name'=>$_GET['query'].'%'));
		else
			$command->where(array('like', 'name', $_GET['query'].'%'));
			
		$regions = $command->queryAll();
		
		header('Content-type: application/json');
		$suggestions = array();
		$suggestion['query'] = 'unit';
		$suggestion['suggestions'] = $regions;
		
		//fb(CJSON::encode($suggestion));
		
		echo CJSON::encode($suggestion);
		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false; // disable any weblogroutes
			}
		}
		Yii::app()->end();
    }

}
