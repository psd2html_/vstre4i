<?php

class CityController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getcities','getcitylist','autocomplete'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('row_id'=>$id,'table_name'=>'city'));

        foreach($translateModels as $translate){
            $model->{$translate->language} = $translate->value;
        }

        $this->render('view',array(
            'model'=>$model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate()
    {
        $model=new City();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['City']))
        {
            $model->attributes=$_POST['City'];
            if($model->save()){
                foreach ($_POST['translate'] as $lang=>$translate){
                    $trModel = new Translate();
                    $trModel->row_id = $model->id;
                    $trModel->language = $lang;
                    $trModel->value = $translate;
                    $trModel->table_name = 'city';
                    $trModel->save(false);
                }
                $this->redirect(array('admin'));
            }
        }
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }
        $categories = array();
        $categoriesModel = Category::model()->findAll();
        foreach($categoriesModel as $category){
            $categories[$category->id] = $category->name;
        }
        $this->render('create',array(
            'model'=>$model,
            'trModels' => $trModels,
            'categories' => $categories
        ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
		$translateModels = Translate::model()->findAllByAttributes(array('table_name'=>'city','row_id'=>$id));
		$languages = Yii::app()->params['languages'];
		unset($languages['ru']);
		$trModels = array();
		foreach($translateModels as  $translate){
			if(array_key_exists($translate->language,$languages)){
				unset($languages[$translate->language]);
				$trModels[$translate->language] = $translate;
			}
		}


		if($languages){
			foreach($languages as $lang=>$value){
				$langModel = new Translate();
				$langModel->language = $lang;
				$trModels[$lang] = $langModel;
			}
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['City']) && isset($_POST['translate']))
		{

			foreach ($_POST['translate'] as $lang=>$translate){
				$trModel = Translate::model()->findByAttributes(array('table_name'=>'city','row_id'=>$id,'language'=>$lang));
				if(!$trModel){
					$trModel = new Translate();
					$trModel->row_id = $id;
				}
				$trModel->language = $lang;
				$trModel->value = $translate;
				$trModel->table_name = 'city';
				$trModel->save();
			}
			
			if($_POST['City']['other_id'] == $id){
				$model->attributes=$_POST['City'];
				if($model->save())
					$this->redirect(array('admin'));
			}else{
				//$model=$this->loadModel($id);
				$otherModel = $this->loadModel($_POST['City']['other_id']);
				if($otherModel->name != $_POST['City']['name']){
					$otherModel->name = (string)$_POST['City']['name'];
					$otherModel->save();
				}
				$model->delete();
				$users = User::model()->findAll('city_id = :city_id', array('city_id'=>$id));
				foreach($users as $user){
					$user->city = $otherModel->city;
					$user->city_id = $otherModel->id;
					$user->region_id = $otherModel->region_id;
					$user->country_id = $otherModel->country_id;
					$user->save();
				}
				$this->redirect(array('admin'));
				
			}
		}
		

        $this->render('update',array(
            'trModels' => $trModels,
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('City');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->layout='//layouts/column2';
		$model=new City('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['City']))
            $model->attributes=$_GET['City'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return City the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=City::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param City $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='city-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionGetCities()
    {
        $region_id = (int)Yii::app()->request->getParam('region_id', false);
        if ($region_id){
            if (Yii::app()->language != 'ru') {
                $criteria = new CDbCriteria();
                $criteria->alias = 'city';
                $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
                $criteria->join = 'LEFT JOIN `translate` ON `city`.`id` = `translate`.`row_id`';
                $criteria->condition = "`translate`.`table_name`='city'
            AND `city`.`region_id`='".(int)$region_id."'
            AND `translate`.`language`='".Yii::app()->language."'
            AND approved =".City::STATUS_APPROVED;
				$criteria->order = 'name';
                $cities = City::model()->findAll($criteria);
            } else {
                $cities = City::model()->sorted()->findAllByAttributes(array('region_id' => $region_id, 'approved' => City::STATUS_APPROVED));
            }
            if ($cities != null)
                for ($i=0;$i<count($cities);$i++)
                    echo '<option value="'.CHtml::encode($cities[$i]->name).'">'.CHtml::encode($cities[$i]->name).'</option>';
        } else echo '';

    }
    public function actionGetCityList()
    {
        $result = '';
        $dataReader = City::model()->getCityList($_GET['country_id']);
        foreach ($dataReader as $val) {
            $result .= CHtml::encode($val['name']).',';
        }
        $result = substr($result,0,strlen($result)-1);
        echo $result;
    }
	public function actionAutocomplete()
    {
		$db = Yii::app()->db;
		$command = $db->createCommand();
		$command->select('city.name AS value, city.id AS data');
		$command->from('city');
		if(!empty($_GET['region_id']))
			$command->where('name LIKE :name AND region_id=:region_id', array(':region_id'=>$_GET['region_id'], 'name'=>$_GET['query'].'%'));
		else
			$command->where(array('like', 'name', $_GET['query'].'%'));
			
		$regions = $command->queryAll();
		
		header('Content-type: application/json');
		$suggestions = array();
		$suggestion['query'] = 'unit';
		if(!empty($regions)){
			$suggestion['suggestions'] = $regions;
		}else{
			$suggestion['suggestions'] = array();
		}
		echo CJSON::encode($suggestion);
		foreach (Yii::app()->log->routes as $route) {
			if($route instanceof CWebLogRoute) {
				$route->enabled = false; // disable any weblogroutes
			}
		}
		Yii::app()->end();
    }
}
