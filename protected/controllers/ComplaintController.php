<?php

class ComplaintController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','sentcomplaint','sendRecord'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(
                    'create','update',
                    'admin','delete' // this two actions for admin and moderators only
                ),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('resolve'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    private function _redirectNonAllowedUser()
    {
        $siteModeratorsModel = new SiteModerators();
        $userId = User::model()->getUserId();
        $isSiteModerator = $siteModeratorsModel->isSiteModerator($userId);
        $isAdmin = Admin::model()->isSiteAdmin($userId);
        if ( !$isAdmin && !$isSiteModerator ) {
            Yii::app()->request->redirect('/');
        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout='//layouts/column2admin';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Complaint;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Complaint']))
		{
			$model->attributes=$_POST['Complaint'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->layout='//layouts/column2admin';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Complaint']))
		{
			$model->attributes=$_POST['Complaint'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $this->_redirectNonAllowedUser();

		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Complaint');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->_redirectNonAllowedUser();

        $model=new Complaint('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Complaint']))
            $model->attributes=$_GET['Complaint'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
	}
	public function actionResolve($id)
	{
		$sql = 'UPDATE `complaint` SET `status`=1 WHERE `id`=:id OR `parent_id` = :id;';
		$db = Yii::app()->db;
        $command = $db->createCommand($sql);
		$command->bindParam(":id",$id,PDO::PARAM_INT);	
		
		$resalt = $command->execute();
		if($resalt){
			$complaint = Complaint::model()->findByPk($id);
			
			$sender_id = $complaint->user_id;
			$noticeMessage = 'Ваша жалоба отмечена как "решена" администратором сайта. %s';
			$noticeMessageGroup = 'Ваши жалобы (<b>%d</b>) отмечены как "решенные" администратором сайта. %s';
			$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$sender_id)) . '">'.'Просмотреть'.'</a>';
			Notice::addNoticeCheckSimilar($sender_id, 'notice', $noticeMessage, $noticeMessageGroup, $link);
			
			if($complaint->text_id == 'user')
				$resipient_id = $complaint->item_id;
			elseif($complaint->text_id == 'group')
				$resipient_id = Group::model()->getAdminId($complaint->item_id, 'group');
			else
				$resipient_id = Group::model()->getAdminId($complaint->item_id, 'meet');
			$noticeMessage = 'Жалоба к Вам отмечена как "решена" администратором сайта. %s';
			$noticeMessageGroup = 'Жалобы к Вам (<b>%d</b>) отмечены как "решенные" администратором сайта. %s';
			$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$resipient_id)) . '">'.'Просмотреть'.'</a>';
			Notice::addNoticeCheckSimilar($resipient_id, 'notice', $noticeMessage, $noticeMessageGroup, $link);
		}
		$this->redirect(array('admin'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Complaint the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Complaint::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Complaint $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='complaint-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionSentComplaint()
    {
        $request = Yii::app()->request;
	

        $mid    = (int)$request->getParam('mid', false);
        $text   = $request->getParam('text', false);
        $textId = $request->getParam('text_id', false);
        $userId = (int)$request->getParam('user_id', false);

        Complaint::model()->sentComplaint($mid, $text, $textId, $userId);

		$noticeMessage = 'Поступила новая жалоба. %s';
		$noticeMessageGroup = 'Поступили новые жалобы (<b>%d</b>). %s';

        switch ($textId) {
            case 'group':
                $group = Group::model()->getGroup($mid);
                $admin = Group::model()->getGroupAdmin($mid);
				$itemName = 'на группу';

                $recipientId = $admin['id'];
                $email = $admin['email'];
				$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$recipientId)) . '">'.'Просмотреть'.'</a>';
                $emailMessage  = 'Поступила жалоба ' . '<br /><strong>Текст жалобы:</strong><br />'
                    . CHtml::encode($text) . '<br /><br />'
                    . '<a href="http://vstre4i.com'. Yii::app()->createUrl('group/view/') . '/' . (int)$group['id']
                    . '" target="_blank">Перейти на сайт</a>';
                break;
            case 'meet':
                $meet = Group::model()->getGroup($mid);
                $admin = Group::model()->getGroupAdmin($meet['parent']);
				$itemName = 'на встречу';

                $recipientId = $admin['id'];
                $email = $admin['email'];
				$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$recipientId)) . '">'.'Просмотреть'.'</a>';
                $emailMessage  = 'Поступила жалоба ' . '<br /><strong>Текст жалобы:</strong><br />'
                    . CHtml::encode($text) . '<br /><br />'
                    . '<a href="'. Yii::app()->createAbsoluteUrl('group/meet', array('id'=>$meet['parent'], 'mid'=>$mid)).'" target="_blank">Перейти на сайт</a>';
                break;
            case 'user':
                $user = User::model()->findByPk($mid);
				$itemName = 'на пользователя';

                $recipientId = $mid;
                $email = $user->email;
				$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$recipientId)) . '">'.'Просмотреть'.'</a>';
                $emailMessage  = 'Поступила жалоба ' . '<br /><strong>Текст жалобы:</strong><br />'
                    . CHtml::encode($text) . '<br /><br />'
                    . '<a href="'. Yii::app()->createAbsoluteUrl('user/view', array('id'=>$recipientId)).'" target="_blank">Перейти на сайт</a>';
                break;
            default: return;
        }
        //Notice::addNotice($recipientId, 'error', $noticeMessage);
		Notice::addNoticeCheckSimilar($recipientId, 'error', $noticeMessage, $noticeMessageGroup, $link);
		//Yii::app()->user->setFlash('recipientId',$recipientId);
		//Yii::app()->user->setFlash('noticeMessage',$noticeMessage);
        //$subject = 'Жалоба на группу. Vstre4i.kz';
        //mail($email, $subject, $emailMessage, "Content-type: text/html\r\n");
		$hostInfo = Yii::app()->request->hostInfo;
		$hostInfoA = explode('//',$hostInfo);
		$hostName = $hostInfoA[1];
		$mail = new JPhpMailer;
		$mail->setFrom(Yii::app()->params['adminEmail'], '');
		$mail->addAddress($email, '');
		$mail->Subject = "Жалоба $itemName | $hostName";
		$content = $emailMessage;
		$mail->MsgHTML($this->message($content));
		$moderators = Group::model()->getModerators($mid);
		foreach($moderators as $moderator){
			$mail->addAddress($moderator['email'], $moderator['name']);
			Notice::addNoticeCheckSimilar($moderator['moderator_id'], 'error', $noticeMessage, $noticeMessageGroup, $link);
		}				
		$mail->send();
    }
    public function actionSendRecord()
    {
        $parentId = Yii::app()->request->getParam('parent_id', 0);
        $senderId = Yii::app()->request->getParam('sender_id', 0);
        $itemId   = Yii::app()->request->getParam('item_id', 0);
        //$userId   = (int)Yii::app()->request->getParam('user_id', 0);
		$userId = (int)$senderId;
        $commentText = Yii::app()->request->getParam('text', '');
        $text_id   = Yii::app()->request->getParam('text_id', 0);

        $commentText = trim($commentText);

        $success = 0;
        if ($commentText !== '') {
            $success = Complaint::model()->sentRecord($senderId, $commentText, $itemId, $text_id, $parentId);
        }

        if ($success && $parentId) {
            $parentComment = $this->loadModel($parentId);
            $parentUser = User::model()->findByPk($parentComment->user_id);
            $wallUserName = User::model()->getUserName($userId);
			$message =  $wallUserName.' ответил на вашу жалобу.';
			//Notice::addNotice($parentUser->id, 'notice', $message);
			$complaint = Complaint::model()->findByPk($parentId);
			$authorOfCompId = $complaint->user_id;
			
			$noticeMessage = $wallUserName.' ответил на Вашу жалобу. %s';
			$noticeMessageGroup = $wallUserName.' %d раза ответил на Вашу жалобу. %s';
			$link = '<br><a href="'. Yii::app()->createUrl('user/complaints', array('id'=>$parentId)) . '">'.'Просмотреть'.'</a>';
			Notice::addNoticeCheckSimilar($authorOfCompId, 'notice', $noticeMessage, $noticeMessageGroup, $link);
			
			
		
			$emailMessage = $message . '<br />'.'<a href="'. Yii::app()->createAbsoluteUrl('user/view') . '/' . (int)$userId . '" target="_blank">Перейти на сайт</a>';
			//$subject = $message . ' Vstre4i.kz';
			//mail($parentUser->email, $subject, $emailMessage, "Content-type: text/html\r\n");
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($parentUser->email, $parentUser->name);
			$mail->Subject = $message;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
        }

		$isAdmin = Admin::model()->isSiteAdmin($userId);
		if($isAdmin)
			$this->redirect(array('user/checkAnswer', 'complaint'=>$parentId));
        $this->redirect(array('user/complaints', 'id'=>$userId));
    }
}
