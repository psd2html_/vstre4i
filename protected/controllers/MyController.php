<?php

class MyController extends CController {

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('ajaxrequest'),
                'users'=>array('@'),
            ),
            // ...
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    public function actionAjaxRequest()
    {
        echo '<option>1</option>';

        Yii::app()->end();
    }

}