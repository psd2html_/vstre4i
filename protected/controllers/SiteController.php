<?php

class SiteController extends Controller
{
	public $useCommonKeywords = false;
	//public $layout='//layouts/column2';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
            'upload'=>array(
                'class'=>'xupload.actions.XUploadAction',
                'path' =>Yii::app() -> getBasePath() . "/../uploads",
                'publicPath' => Yii::app() -> getBaseUrl() . "/uploads",
            ),
            array('allow',
                'actions'=>array('moderatorapprove'),
                'users'=>array('@'),
            ),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndexOld()
	{
		$this->useCommonKeywords = true;
		$user_id = User::model()->getUserId();
		$user = User::model()->findByPk($user_id);
		if($user===null){
			$user = new stdClass();
			$user->country_id = 0;
			$user->region_id = 0;
			$user->city = '';
		}
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index',array('user'=>$user, 'user_id'=>$user_id));
	}
    public function actionIndex()
    {
		$this->useCommonKeywords = true;
		$user_id = User::model()->getUserId();
		$user = User::model()->findByPk($user_id);
		if($user===null){
			$user = new stdClass();
			$user->country_id = 0;
			$user->region_id = 0;
			$user->city = '';
		}
		if(isset($_GET['country'])){
			$search_resalt = $this->renderPartial('_index',NULL,true);
			//fb($search_resalt,'search_resalt');
			$this->render('index2', array('user'=>$user, 'user_id'=>$user_id, 'search_resalt'=>$search_resalt));   
		}else{
			$this->render('index2', array('user'=>$user, 'user_id'=>$user_id, 'search_resalt'=>'')); 
		}
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				/* $headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8"; */

				//mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				
				$mail = new JPhpMailer;
				$mail->setFrom($model->email, $name);
				$mail->addAddress(Yii::app()->params['adminEmail'], 'Администратор');
				$mail->addReplyTo($model->email, $name);
				$mail->Subject = $subject;
				$content = $model->body;
				$mail->MsgHTML($this->message($content));$mail->send();
				
				
				Yii::app()->user->setFlash('contact',Yii::t('var', 'Спасибо, что написали нам. Мы с Вами свяжемся в ближайшее время.'));
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
	        $redirect = Yii::app()->createUrl('site/index');
	        if(empty($_SERVER['HTTP_REFERER'])) $_SERVER['HTTP_REFERER'] = '';
			if (substr_count($_SERVER['HTTP_REFERER'], "/group/")>0) {
                Yii::app()->session['redirect'] = $_SERVER['HTTP_REFERER'];
            }
            if (substr_count(Yii::app()->session['redirect'], "/group/")>0) {
                $redirect = Yii::app()->session['redirect'];
            }

	        $service = Yii::app()->request->getQuery('service');
	        if (isset($service)) {
	            $authIdentity = Yii::app()->eauth->getIdentity($service);
	            $authIdentity->setRedirectUrl(Yii::app()->user->returnUrl);
	            $authIdentity->setCancelUrl($this->createAbsoluteUrl('site/login'));

	            if ($authIdentity->authenticate()) {
	                $identity = new ServiceUserIdentity($authIdentity);

	                // Успешный вход
	                if ($identity->authenticate()) {
                        $duration = !empty($_POST['LoginForm']['rememberMe']) ? 3600 * 24 * 14 : 0; // 14 days
	                    Yii::app()->user->login($identity, $duration);

	                    // Специальный редирект с закрытием popup окна
	                    $authIdentity->redirect($redirect);
	                }
	                else {
	                    // Закрываем popup окно и перенаправляем на cancelUrl
	                    $authIdentity->cancel();
	                }
	            }

	            // Что-то пошло не так, перенаправляем на страницу входа
	            //$this->redirect(array('site/login'));
	            $this->redirect(array('site/login'));
	        }

			$model=new LoginForm;

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm']))
			{

	            $identity=new UserIdentity($_POST['LoginForm']['username'],$_POST['LoginForm']['password']);
	            if($identity->authenticate()) {
                    $duration = !empty($_POST['LoginForm']['rememberMe']) ? 3600 * 24 * 14 : 0; // 14 days
	                Yii::app()->user->login($identity, $duration);
                } else {
					$model->addError('password','Incorrect username or password.');
					$model->addError('username','Incorrect username or password.');
					//die("!!!!");
				}

	            //$this->redirect($redirect);
			}
			// display the login form
			$this->render('login',array('model'=>$model));
		}
		else {
		//print_r(Yii::app()->homeUrl);
			$this->redirect(Yii::app()->homeUrl);
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
    public function actionRegistration()
    {
        if (Yii::app()->user->isGuest) {
	        $redirect = Yii::app()->createUrl('site/index');
	        if (substr_count($_SERVER['HTTP_REFERER'], "/group/")>0)
	            Yii::app()->session['redirect'] = $_SERVER['HTTP_REFERER'];
	        if (substr_count(Yii::app()->session['redirect'], "/group/")>0)
	            $redirect = Yii::app()->session['redirect'];

	        $service = Yii::app()->request->getQuery('service');

	        if (isset($service)) {


	            $authIdentity = Yii::app()->eauth->getIdentity($service);
	            $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
	            $authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');



	            if ($authIdentity->authenticate()) {

                    $identity = new ServiceUserIdentity($authIdentity);

	                // Успешный вход
	                if ($identity->authenticate()) {
	                    Yii::app()->user->login($identity);

                        $message = Yii::t('var', 'Регистрация успешно завершена');
	                    Yii::app()->user->setFlash('success', $message);

	                    // Специальный редирект с закрытием popup окна
	                    $authIdentity->redirect($redirect);
	                }
	                else {
	                    // Закрываем popup окно и перенаправляем на cancelUrl
	                    $authIdentity->cancel();
	                }
	            }

	            // Что-то пошло не так, перенаправляем на страницу входа
	            $this->redirect(array('site/login'));
	        }

	        $model=new User;
	        // Uncomment the following line if AJAX validation is needed
	        //$this->performAjaxValidation($model);
	        if(isset($_POST['User']))
	        {

	            $model->name = $_POST['User']['name'];
                $model->pass = $_POST['User']['pass'];
	            $model->email = $_POST['User']['email'];
	            $model->reg_date = Date('Y-m-d');

	            if($model->save()){
	                $identity=new UserIdentity($model->name,$_POST['User']['pass']);
	                if($identity->authenticate()) {
	                    Yii::app()->user->login($identity);

                        $message = Yii::t('var', 'Регистрация успешно завершена');
	                    Yii::app()->user->setFlash('success', $message);

	                    $this->redirect($redirect);
	                }
	            } else {

	            }
	        }

	        $this->render('registration',array('model'=>$model));
		}
		else {
		//print_r(Yii::app()->homeUrl);
			$this->redirect(Yii::app()->homeUrl);
		}
    }

    public function actionAbout()
    {
        $model=new Page;
        $this->render('about',array('model'=>$model));
    }
    public function actionFaq()
    {
        $model=new Page;
        $this->render('faq',array('model'=>$model));
    }
    public function actionTerms()
    {
        $model=new Page;
        $this->render('terms',array('model'=>$model));
    }

    public function actionModeratorApprove()
    {
        $model = new SiteModerators();
        $this->render('moderatorapprove',array('model'=>$model));
    }

    public function actionModeratorAgree()
    {
        $targetId = User::model()->getUserId();
        $agree = Yii::app()->request->getParam('agree', 0);
        $model = new SiteModerators();

        $model->deleteOldRequests();
        $moderatorRequest = $model->getRequest($targetId);

        if (!$moderatorRequest) {
            $failMessage = 'Запросов на вас не поступало';
            Yii::app()->user->setFlash('notice', $failMessage);
            Yii::app()->request->redirect('/');
        }

        $success = 0;
        $refused = false;
        if ($agree) {
            $success = $model->satisfyRequest($targetId);
        } else {
            $success = $model->deleteRequest($targetId);
            if ($success) {
                $refused = true;
            }
        }

        if (!$success) {
            $failMessage = 'Ошибка запроса. Попробуйте ещё раз.';
            Yii::app()->user->setFlash('error', $failMessage);
            Yii::app()->request->redirect('/');
        }

        $adminId = $moderatorRequest['sender_id'];
        $adminEmail = User::model()->getUserEmail($adminId);

        $targetEmail = User::model()->getUserEmail($targetId);
        $targetName = User::model()->getUserName($targetId);
        $targetLink = 'http://vstre4i.com'. Yii::app()->createUrl('user/view') . '/' . $targetId;

        $adminPanelLink = 'http://vstre4i.com/admin';

        if ($refused) {
            $refuseMessage = 'Вы отказались от должности модератора сайта.';
            Yii::app()->user->setFlash('success', $refuseMessage);
			$hostInfo = Yii::app()->request->hostInfo;
			$hostInfoA = explode('//',$hostInfo);
			$hostName = $hostInfoA[1];
            $noticeMessage = 'Пользователь ' . $targetName . ' отказался стать модератором сайта.';
            Notice::addNotice($adminId, 'notice', $noticeMessage);
            $subject = 'Пользователь отклонил заявку. '.$hostName;
            $emailMessage = 'Пользователь <a href="' . $targetLink . '">' . $targetName . '</a>'
                . ' отказался принять ваше предложение получить права модератора сайта ' . Yii::app()->name
                . '<br /><a href="' . $adminPanelLink . '" target="_blank">Перейти в административную панель.</a>';
            //mail($adminEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
			
			$mail = new JPhpMailer;
			$mail->setFrom(Yii::app()->params['adminEmail'], '');
			$mail->addAddress($adminEmail, '');
			$mail->Subject = $subject;
			$content = $emailMessage;
			$mail->MsgHTML($this->message($content));$mail->send();
			
			
            Yii::app()->request->redirect('/');
        }

        //to new moderator
        $noticeMessage = 'Вы получили права модератора сайта ' . Yii::app()->name;
        Notice::addNotice($targetId, 'notice', $noticeMessage);
		
		$hostInfo = Yii::app()->request->hostInfo;
		$hostInfoA = explode('//',$hostInfo);
		$hostName = $hostInfoA[1];
        
		$subject = "Вы получили права модератора сайта. $hostName";
        $emailMessage = $noticeMessage . '<br /><a href="' . $adminPanelLink . '" target="_blank">'
            . 'Перейти в административную панель.</a>';
        //mail($targetEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
		
		$mail = new JPhpMailer;
		$mail->setFrom(Yii::app()->params['adminEmail'], '');
		$mail->addAddress($targetEmail, $targetName);
		$mail->Subject = $subject;
		$content = $emailMessage;
		$mail->MsgHTML($this->message($content));$mail->send();
		
		

        //to site administrator
        $noticeMessage = 'Пользователь ' . $targetName . ' согласился стать модератором сайта.';
        Notice::addNotice($adminId, 'notice', $noticeMessage);

        $subject = "Новый модератор. $hostName";
        $emailMessage = 'Пользователь <a href="' . $targetLink . '">' . $targetName . '</a>'
            . ' согласился на вашу заявку получить права модератора сайта ' . Yii::app()->name
            . '<br /><a href="' . $adminPanelLink . '" target="_blank">Перейти в административную панель.</a>';
        //mail($adminEmail, $subject, $emailMessage, "Content-type: text/html\r\n");
		
		$mail = new JPhpMailer;
		$mail->setFrom(Yii::app()->params['adminEmail'], '');
		$mail->addAddress(Yii::app()->params['adminEmail'], '');
		$mail->Subject = $subject;
		$content = $emailMessage;
		$mail->MsgHTML($this->message($content));$mail->send();

        Yii::app()->request->redirect('/');
    }

}
