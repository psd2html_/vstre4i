<?php
class ItemController extends CController
{

    public function actionCreate()
    {
        $model=new Item;  // this is my model related to table
        if(isset($_POST['User']))
        {
            $rnd = rand(0,9999);  // generate random number between 0-9999
            $model->attributes=$_POST['User'];

            $uploadedFile=CUploadedFile::getInstance($model,'avatar');
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->image = $fileName;

            if($model->save())
            {
                $uploadedFile->saveAs('/images/user/'.$fileName);  // image will uplode to rootDirectory/banner/
                $this->redirect(array('admin'));
            }
        }
        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['User']))
        {
            $_POST['User']['avatar'] = $model->image;
            $model->attributes=$_POST['User'];

            $uploadedFile=CUploadedFile::getInstance($model,'avatar');

            if($model->save())
            {
                if(!empty($uploadedFile))  // check if uploaded file is set or not
                {
                    $uploadedFile->saveAs('/images/user/'.$model->image);
                }
                $this->redirect(array('admin'));
            }

        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }


}