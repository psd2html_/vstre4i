<?php

class PaymentController extends Controller
{   ///Pay by SMS
    public function actionSdpaysRequestHandler(){
        $modelAdmin = Admin::model()->findByAttributes(array('key'=>'smsdostup_md5'));
        $project_md5 = $modelAdmin->value;
        if (!isset($_POST['_md5_hash']) || !isset($_POST['_session_code']) || !isset($_POST['_sms_id']) || !isset($_POST['_sms_number']) || !isset($_POST['_sms_operator']) || !isset($_POST['_sms_phone']) || !isset($_POST['_sms_message']) || !isset($_POST['_sms_price'])) $this->return_result("err void", true);
        if (!$_POST['_md5_hash'] || !$_POST['_session_code'] || !$_POST['_sms_id'] || !$_POST['_sms_number'] || !$_POST['_sms_operator'] || !$_POST['_sms_phone']) $this->return_result("err false", true);
        $_md5hash = md5($project_md5.$_POST['_session_code'].$_POST['_sms_id'].$_POST['_sms_number'].$_POST['_sms_operator'].$_POST['_sms_phone'].stripslashes($_POST['_sms_message']).$_POST['_sms_price']);
        if ($_md5hash != $_POST['_md5_hash']) return_result("err hash", true);
        $code = rand(1000,100000);
        $paymentModel = new Payment();
        $paymentModel->confirmation_code = $code;
        $paymentModel->type = 'sms';
        $paymentModel->payer_id =  $_POST['_sms_phone'];
        $paymentModel->transaction_id = $_POST['_sms_id'];
        $paymentModel->submit_date = time();
        $paymentModel->save(false);
        $this->return_result(
            $code
        );
    }



    private function return_result($message, $is_error = false) {
        if ($is_error) exit("<SMSDERR>".stripslashes($message)."</SMSDERR>");
        exit("<SMSDOSTUP>".stripslashes($message)."</SMSDOSTUP>");
    }


    ////  Paypall

    public function actionPaypalPay($group_id){
        $modelAdmin = Admin::model()->findByAttributes(array('key' => 'group_price_usa'));
        $paymentInfo['Order']['theTotal'] = $modelAdmin->value;
        $paymentInfo['Order']['description'] = Yii::t('var','Some payment description here');
        $paymentInfo['Order']['quantity'] = '1';

        // call paypal
        $result = Yii::app()->Paypal->SetExpressCheckout($paymentInfo);
        //Detect Errors
        if(!Yii::app()->Paypal->isCallSucceeded($result)){
            if(Yii::app()->Paypal->apiLive === true){
                //Live mode basic error message
                $error = Yii::t('var','We were unable to process your request. Please try again later');
            }else{
                //Sandbox output the actual error message to dive in.
                $error = $result['L_LONGMESSAGE0'];
            }
            echo CHtml::encode($error);
            Yii::app()->end();

        }else {
            // send user to paypal
            $token = urldecode($result["TOKEN"]);

            $payPalURL = Yii::app()->Paypal->paypalUrl.$token;

            $paymentModel = new Payment();
            $paymentModel->type = 'paypal';
            $paymentModel->group_id = $group_id;
            $paymentModel->transaction_id = $token;
            $paymentModel->submit_date = time();
            $paymentModel->save(false);

            $this->redirect($payPalURL);
        }
    }

    public function actionPaypalConfirm()
    {
        $token = trim($_GET['token']);
        $payerId = trim($_GET['PayerID']);

        $result = Yii::app()->Paypal->GetExpressCheckoutDetails($token);

        $result['PAYERID'] = $payerId;
        $result['TOKEN'] = $token;
        $modelAdmin = Admin::model()->findByAttributes(array('key' => 'group_price_usa'));
        $result['ORDERTOTAL'] = $modelAdmin->value;
        //Detect errors
        if(!Yii::app()->Paypal->isCallSucceeded($result)){
            if(Yii::app()->Paypal->apiLive === true){
                //Live mode basic error message
                $error = Yii::t('vat','We were unable to process your request. Please try again later');
            }else{
                //Sandbox output the actual error message to dive in.
                $error = $result['L_LONGMESSAGE0'];
            }
            echo CHtml::encode($error);
            Yii::app()->end();
        }else{

            $paymentResult = Yii::app()->Paypal->DoExpressCheckoutPayment($result);
            //Detect errors
            if(!Yii::app()->Paypal->isCallSucceeded($paymentResult)){
                if(Yii::app()->Paypal->apiLive === true){
                    //Live mode basic error message
                    $error = Yii::t('var','We were unable to process your request. Please try again later');
                }else{
                    //Sandbox output the actual error message to dive in.
                    $error = $paymentResult['L_LONGMESSAGE0'];
                }
                echo CHtml::encode($error);
                Yii::app()->end();
            }else{
                $paymentModel = Payment::model()->findByAttributes(array('transaction_id'=>$token));
                $paymentModel->payer_id = $payerId;
                $paymentModel->status = "1";
                $paymentModel->submit_date  = time();
                $paymentModel->save();
                Yii::app()->groupExtender->extendGroup($paymentModel->group_id);

                $this->render('application.views.group.payment.paypal_success');
            }
        }
    }

    public function actionPaypalCancel()
    {
        //The token of the cancelled payment typically used to cancel the payment within your application
        $token = $_GET['token'];
        $paymentModel = Payment::model()->findByAttributes(array('transaction_id'=>$token));
        if($paymentModel){
            $paymentModel->delete();
        }

        $this->render('application.views.group.payment.paypal_cancel');
    }


    public function actionWebmoneySuccess(){
        echo 'success';
    }

    public function actionWebmoneyFail(){
        echo 'fail';
    }



    public function actionWebmoneyResult(){

//        Array
//        (
//            [LMI_MODE] => 1
//    [LMI_PAYMENT_AMOUNT] => 0.05
//    [LMI_PAYEE_PURSE] => Z303342120238
//    [LMI_PAYMENT_NO] => 3
//    [LMI_PAYER_WM] => 257476131005
//    [LMI_PAYER_PURSE] => Z303342120238
//    [LMI_SYS_INVS_NO] => 687
//    [LMI_SYS_TRANS_NO] => 189
//    [LMI_SYS_TRANS_DATE] => 20130703 16:55:06
//    [LMI_HASH] => 26D07937F840966025CF630E353FC254
//    [LMI_PAYMENT_DESC] => Group 2 Top. vstre4i.kz
//    [LMI_LANG] => ru-RU
//    [LMI_DBLCHK] => SMS
//    [YII_CSRF_TOKEN] => b9ce2d982d7ad5b434cf8fbc48d9be2112f55a04
//    [yt0] => Оплатить
//)
        if($_REQUEST){
            $secretKey = Admin::model()->findByAttributes(array('key'=>'webmoney_secret_key'));
            $price = Admin::model()->findByAttributes(array('key'=>'group_price_usa'));
            $webmoney = Admin::model()->findByAttributes(array('key'=>'webmoney'));
            $common_string = $_POST['LMI_PAYEE_PURSE'].$_POST['LMI_PAYMENT_AMOUNT'].$_POST['LMI_PAYMENT_NO'].
                $_POST['LMI_MODE'].$_POST['LMI_SYS_INVS_NO'].$_POST['LMI_SYS_TRANS_NO'].
                $_POST['LMI_SYS_TRANS_DATE'].$secretKey->value.$_POST['LMI_PAYER_PURSE'].$_POST['LMI_PAYER_WM'];
            $hash = strtoupper(md5($common_string));
            if($hash === $_POST['LMI_HASH'] && $_POST['LMI_PAYMENT_AMOUNT']==$price->value && $_POST['LMI_PAYEE_PURSE']==$webmoney->value){
                $model = new Payment();
                $model->group_id = $_POST['FIELD_GROUP_ID'];
                $model->type='webmoney';
                $model->payer_id = $_POST['LMI_PAYER_PURSE'];
                $model->transaction_id = $_POST['LMI_SYS_TRANS_NO'];
                $model->status = '1';
                $model->submit_date = time();
                if($model->save(false)){
                    Yii::app()->groupExtender->extendGroup($model->group_id,$_POST['FIELD_USER_ID']);
                }
                echo 'YES';
            }
        }

    }



    public function actionPayByQiwi(){
        echo Yii::app()->ishop->createBill( 380664604401, 3, 'Vstre4i', '2222', $create = true );  
    }
}