<?php

class KeywordController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getkeywords','insertuserinterest','removeuserinterest','getcategoryinterestslist','getcategorylist'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $this->layout='//layouts/column2admin';
        $model = $this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('row_id'=>$id,'table_name'=>'keyword'));

        foreach($translateModels as $translate){
            $model->{$translate->language} = $translate->value;
        }

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $this->layout='//layouts/column2admin';
        $model=new Keyword();

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Keyword']))
        {
            $model->attributes=$_POST['Keyword'];
            if($model->save()){
                foreach ($_POST['translate'] as $lang=>$translate){
                    $trModel = new Translate();
                    $trModel->row_id = $model->id;
                    $trModel->language = $lang;
                    $trModel->value = $translate;
                    $trModel->table_name = 'keyword';
                    $trModel->save(false);
                }
                $this->redirect(array('view','id'=>$model->id));
            }
        }
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }
        $categories = array();
        $categoriesModel = Category::model()->findAll();
        foreach($categoriesModel as $category){
            $categories[$category->id] = $category->name;
        }
        $this->render('create',array(
            'model'=>$model,
            'trModels' => $trModels,
            'categories' => $categories
        ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $this->layout='//layouts/column2admin';
		$model=$this->loadModel($id);
        $translateModels = Translate::model()->findAllByAttributes(array('table_name'=>'keyword','row_id'=>$id));
        $languages = Yii::app()->params['languages'];
        unset($languages['ru']);
        $trModels = array();
        foreach($translateModels as  $translate){
            if(array_key_exists($translate->language,$languages)){
                unset($languages[$translate->language]);
                $trModels[$translate->language] = $translate;
            }
        }


        if($languages){
            foreach($languages as $lang=>$value){
                $langModel = new Translate();
                $langModel->row_id = $id;
                $langModel->table_name = 'keyword';
                $langModel->language = $lang;
                $trModels[$lang] = $langModel;
            }
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Keyword']) && isset($_POST['translate']))
        {

            foreach ($_POST['translate'] as $lang=>$translate){
                $trModel = Translate::model()->findByAttributes(array('table_name'=>'keyword','row_id'=>$id,'language'=>$lang));
                if(!$trModel){
                    $trModel = new Translate();
                    $trModel->row_id = $id;
                }
                $trModel->language = $lang;
                $trModel->value = $translate;
                $trModel->table_name = 'keyword';
                $trModel->save();
            }
            $model->attributes=$_POST['Keyword'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }
        $categories = array();
        $categoriesModel = Category::model()->findAll();
        foreach($categoriesModel as $category){
            $categories[$category->id] = $category->name;
        }

        $this->render('update',array(
            'trModels' => $trModels,
            'model'=>$model,
            'categories' => $categories
        ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->layout='//layouts/column2admin';
		$dataProvider=new CActiveDataProvider('Keyword');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $model=new Keyword();
        $model->search(true);
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Keyword']))
            $model->attributes=$_GET['Keyword'];
        $content = $this->renderPartial('admin',array(
            'model'=>$model,
        ),true);
        $this->render('application.views.admin.index',array('content'=>$content));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Keyword the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Keyword::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Keyword $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='keyword-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionGetKeywords()
    {
        $cat_id = (int)Yii::app()->request->getParam('cat_id', 0);

        if (Yii::app()->language != 'ru') {
            $criteria = new CDbCriteria();
            $criteria->alias = 'keyword';
            $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
            $criteria->join = 'LEFT JOIN `translate` ON `keyword`.`id` = `translate`.`row_id`';
            $criteria->condition = "`translate`.`table_name`='keyword' AND `keyword`.`cat_id`='".(int)$cat_id."' AND `translate`.`language`='".Yii::app()->language."'";
            $keywords = Keyword::model()->findAll($criteria);
        } else {
            $keywords = Keyword::model()->findAllByAttributes(array('cat_id' => $cat_id));
        }
        $text = '<ul>';
        if ($keywords != null)
            for ($i=0;$i<count($keywords);$i++)
                $text .= '<li class="keyword-item" kid="'.CHtml::encode($keywords[$i]->id).'">'.CHtml::encode($keywords[$i]->name).'</li>';
        echo $text.'</ul>';
    }
    public function actionInsertUserInterest()
    {
        if (isset($_GET['user_id']) && isset($_GET['keyword'])) {
            $keyword_id = Keyword::model()->getKeywordName($_GET['keyword']);
            Keyword::model()->insertUserInterest($_GET['user_id'],$keyword_id);
        }
    }
    public function actionRemoveUserInterest()
    {
        if (isset($_GET['user_id']) && isset($_GET['kid'])) {
            Keyword::model()->removeUserInterest($_GET['user_id'],$_GET['kid']);
        }
    }
    public function actionGetCategoryInterestsList()
    {
        $result = '';
        $dataReader = Keyword::model()->getCategoryInterestsList($_GET['cat']);
        foreach ($dataReader as $val) {
            $result .= '<li class="keywords-list-keyword">'.CHtml::encode($val['name']).'</li>';
        }
        echo $result;
    }
    public function actionGetCategoryList()
    {
        echo Keyword::model()->getInterestsCategory();
    }
}
