-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица vstre4i.menus_elements
DROP TABLE IF EXISTS `menus_elements`;
CREATE TABLE IF NOT EXISTS `menus_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `params` varchar(512) NOT NULL,
  `page_id` int(11) NOT NULL,
  `order_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vstre4i.menus_elements: 10 rows
/*!40000 ALTER TABLE `menus_elements` DISABLE KEYS */;
INSERT INTO `menus_elements` (`id`, `menu_id`, `type`, `title`, `params`, `page_id`, `order_number`) VALUES
	(1, 1, 'user_profile', 'Главная', '[]', 0, 0),
	(2, 1, 'user_admin_groups', 'Я создал', '[]', 0, 1),
	(3, 1, 'user_groups', 'Я участвую', '[]', 0, 2),
	(4, 1, 'user_profile', 'Моя страница', '[]', 0, 3),
	(5, 1, 'user_update', 'Настройки', '[]', 0, 4),
	(6, 1, 'link', 'Поиск', '{"link":"/"}', 0, 5),
	(7, 1, 'link', 'Админка', '{"link":"/admin","admin_only":1}', 0, 6),
	(8, 2, '', 'О нас', '[]', 1, 1),
	(9, 2, '', 'Контакты', '[]', 3, 2),
	(10, 2, '', 'Помощь', '[]', 2, 3);
/*!40000 ALTER TABLE `menus_elements` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
