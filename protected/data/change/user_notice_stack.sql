-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица vstre4i.user_notice_stack
DROP TABLE IF EXISTS `user_notice_stack`;
CREATE TABLE IF NOT EXISTS `user_notice_stack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `count` int(3) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vstre4i.user_notice_stack: 23 rows
/*!40000 ALTER TABLE `user_notice_stack` DISABLE KEYS */;
INSERT INTO `user_notice_stack` (`id`, `type_id`, `recipient_id`, `message`, `link`, `count`, `timestamp`) VALUES
	(164, 2, 151, '4 предложил вас на роль модератора группы Группа1', '', 1, '0000-00-00 00:00:00'),
	(119, 2, 140, '4 предложил вас на роль модератора группы Группа1', '', 1, '0000-00-00 00:00:00'),
	(196, 2, 107, 'Ваша группа "test" была одобрена.', '', 1, '0000-00-00 00:00:00'),
	(197, 2, 111, 'К сожалению, заявка на создание группы "Инспектор Жубер" была отклонена.', '', 1, '0000-00-00 00:00:00'),
	(198, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(199, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(200, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(201, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(202, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(231, 2, 111, 'admin подал заявку на вступление в группу <br><a href="/ru/group/9">Инспектор Жубер</a>', '', 1, '2015-01-20 17:51:18'),
	(206, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(207, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(208, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(209, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(210, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(211, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(212, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(213, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(214, 2, 111, 'Поступила жалоба на группу Инспектор Жубер.', '', 1, '0000-00-00 00:00:00'),
	(215, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00'),
	(216, 2, 0, 'hello', '', 1, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user_notice_stack` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
