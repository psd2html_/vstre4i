<?php

class MenusWidget extends CWidget
{
    public $id = 0;

    public function run()
    {
        $userId = User::model()->getUserId();
        $db = Yii::app()->db;
		$alias = '';
		
        $menuElements = $db->createCommand()
            ->select()
            ->from('menus_elements')
            ->where('menu_id = ' . (int)$this->id)
            ->order('order_number ASC')
            ->queryAll();

        $params = array('userId' => $userId);
        if ($menuElements) {
            foreach($menuElements as $key => $element) {

                $menuElements[$key]['params'] = json_decode($element['params'], true);

                switch( $element['type'] ) {
                    case 'user_groups':
                        $params['userGroups'] = $this->getUserGroups($userId);
                        break;
                    case 'user_admin_groups':
                        $params['userAdminGroups'] = $this->getUserAdminGroups($userId);
                        break;
                }
				
				if($element['page_id']){
					$sql='SELECT alias FROM `page` WHERE id = :id';
					$command=$db->createCommand($sql);
					$command->bindParam(":id",$element['page_id'],PDO::PARAM_INT);	
					$alias = $command->queryScalar();
					$menuElements[$key]['alias'] = $alias;
				}
            }
        }
        $params['menuElements'] = $menuElements;
        $this->render('menus_'.$this->id, $params);
    }

    private function getUserGroups($userId)
    {
        $db = Yii::app()->db;

        $rows = $db->createCommand()
            ->select('member.*, group.name AS group_name')
            ->from('member')
            ->leftJoin('group', 'member.group_id = group.id')
            ->where('member.user_id = ' . (int)$userId
                . ' AND member.confirm > 0 AND type = "group"')
            ->queryAll();

        return $rows;
    }

    private function getUserAdminGroups($userId)
    {
        $db = Yii::app()->db;

        $rows = $db->createCommand()
            ->select('group_admin.*, group.name AS group_name')
            ->from('group_admin')
            ->leftJoin('group', 'group_admin.group_id = group.id')
            ->where('group_admin.user_id = ' . (int)$userId . ' AND group_admin.admin = 1')
            ->queryAll();
        return $rows;
    }
}
