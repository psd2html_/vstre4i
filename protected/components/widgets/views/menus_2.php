<?php
if(!empty($menuElements)):
    foreach($menuElements as $element):
        $href = $element['alias'] ? Yii::app()->createAbsoluteUrl('//page/view', array('alias'=>$element['alias'])) : Yii::app()->createAbsoluteUrl('//site/page', array('view'=>$element['page_id']));
        //$href = Yii::app()->createUrl('page/view', array('id'=>$element['page_id']));
		if ( isset($element['params']['admin_only'])
            && $element['params']['admin_only']
            && !Admin::model()->isSiteAdminOrModerator($userId)
        ) {
            continue;
        }
		echo '<li><a class="white" href="'.$href.'">'.$element['title'].'</a></li>';
	endforeach;
endif;
