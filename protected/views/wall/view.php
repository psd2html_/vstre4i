<?php
/* @var $this WallController */
/* @var $model Wall */
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'user_id',
            'header' => Yii::t('var','user_id')
        ),
        array(
            'name' => 'text',
            'header' => Yii::t('var','text')
        ),
        array(
            'name' => 'date',
            'header' => Yii::t('var','date')
        ),
        array(
            'name' => 'sender_id',
            'header' => Yii::t('var','sender_id')
        ),
	),
)); ?>
