<?php $groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($data->id); ?>
<div class="event border-radius-5 even align-center f-left"
     onclick="window.location.href='<?php echo CHtml::encode($groupLink); ?>'">
    <?php
    if ($data->picture != NULL && $data->picture != '') {
        $picSrc = '/images/group/mini_' . CHtml::encode($data->picture);
    } else {
        $picSrc = '/images/group/mini_no-pic.png';
    }
    ?>
    <div class="event-img f-left">
        <img style="height: 167px;width: 167px;" src="<?php echo CHtml::encode($picSrc); ?>" alt="event">
    </div>
    <div class="event-name f-left">
        <a href="<?php echo CHtml::encode($groupLink); ?>"><?php echo CHtml::encode($data->name); ?></a>
    </div>
    <?php
    $city = City::model()->getCity($data->city_id);
    $country = Country::model()->getCountryByCityID($data->city_id);
    $region = Region::model()->getRegionByCityID($data->city_id);
    ?>
    <div class="event-place f-left">
        <?php echo CHtml::encode($country) . ' -> ' . CHtml::encode($region) . ' -> ' . CHtml::encode($city); ?>
    </div>
    <?php $model = new Member(); ?>
    <div class="event-qty f-left">
        Участников:&nbsp;<?php echo CHtml::encode($model->getGroupMembersCount($data->id)); ?>
    </div>
    <div class="event-date f-left">
        <?php
        $meet = Group::model()->getNextMeet($data->id);
        if ($meet) {
            echo 'ближайшая встреча ' . CHtml::encode(date('d.m.Y',strtotime($meet)));
        } else {
            echo 'ближайших встреч нет';
        }
        ?>
    </div>
</div>
