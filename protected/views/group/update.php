<script>var group_id = '<?php echo $group_id; ?>';</script>
<?php
/* @var $this GroupController */
/* @var $model Group */

$this->setPageTitle('Редактирование группы - ' . $model->name . ' - ' . Yii::app()->name);
$this->breadcrumbs = array(
    $model->name => Yii::app()->createUrl('group/view') . '/' . urlencode($model->id),
	'Настройки',
);

$user_id = User::model()->getUserId();

$isgroupadmin = Group::model()->isGroupAdmin($model->id,$user_id);
$isGroupMainAdmin = Group::model()->isGroupMainAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);

if (Yii::app()->user->name!='admin') {
    if (!Yii::app()->user->id || !$isgroupadmin ) {
        Yii::app()->request->redirect(Yii::app()->createUrl('group/view').'/'.urlencode($model->id));
    }
}

$groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($model->id);
if (!$model->approved) {
    Yii::app()->request->redirect($groupLink);
}

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/date-inputs-extending.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/colorpicker.js');

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";

$this->breadcrumbs=array(
    CHtml::encode($model->name)=>array('view','id'=>(int)$model->id),
    'Управление группой',
);

$mainAdmin = $model->getGroupAdmin($model->id);
?>

<div class="center">
<aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
    <div class="sidebar-left">
        <div class="group-img">
            <?
            if ($model->picture != '' || $model->picture != NULL) {
                $picture = "mini_".$model->picture;
            } else {
                $picture = 'mini_no-pic.png';
            }
            ?>
            <img src="/images/group/<?php echo CHtml::encode($picture); ?>" style="">
        </div>
        <? if ($isgroupadmin == 1) { ?>
            <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.$model->id ?>'"
               title="<?php echo Yii::t('var', 'Управление группой');?>" class="group-control align-center">
                <?php echo Yii::t('var', 'Управление группой');?>
            </a>
        <? }else { ?>
            <? if ($user_id!=0) { ?>
                <a mid="<? echo $model->id ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на группу" class="complain align-center"><? echo Yii::t('var', 'пожаловаться на группу');?></a>
            <? } ?>
        <? } ?>

    </div>
    <div class="sidebar-left">
        <div class="group-info">
            <ul>
                <li><?php echo CHtml::encode($groupCity) . ', ' . CHtml::encode($groupCountry);?></li>
                <li><?php echo Yii::t('var', 'Группа создана');?>:
                    <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                <li><?php echo Yii::t('var', 'Участники');?>:
                    <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                <li><?php echo Yii::t('var', 'Будущие события');?>:
                    <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                <li><?php echo Yii::t('var', 'Прошедшие события');?>:
                    <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                <li><span class="calend-span"><?php
                        echo Yii::t('var', 'Календарь группы');
                        ?>: </span><span class="calend-link"></span></li>
                <script>
                    $(document).ready(function(){
                        var name = 'y';
                        var dec_url = decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
                        if (dec_url != null) {
                            $('.calend').show();
                        }
                        $('.calend-show, .calend-close').on('click',function(){
                            $('.calend').slideToggle();
                        });
                        $('.calend-link').click(function(){
                            window.open('<?php echo $groupLink; ?>?calend=1', '_blank');
                        });
                    });
                </script>
            </ul>
            <div class="key-words">
                <?php echo Yii::t('var', 'Ключевые слова'); ?>:
                <ul>
                    <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                    $i = 0;
                    foreach($dataReader as $row) {
                        $keywordLink = Yii::app()->createUrl('site/index')
                            . '/?key=' . urlencode($row['name'])
                            . '&cat_id=' . urlencode($row['cat_id']);
                        echo '<li><a href="' . $keywordLink . '">'
                            . CHtml::encode($row['name']) . '</a></li>';
                        $i++;
                    } ?>
                </ul>
            </div>
            <div class="organizers">
                <?php echo Yii::t('var', 'Администратор') . ':';?>
                <ul>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($mainAdmin['user_id']); ?>"
                           class="admin-link-<?php echo CHtml::encode($mainAdmin['user_id']); ?>"><?php
                            echo CHtml::encode($mainAdmin['name']); ?></a>
                    </li>
                </ul>
                <?php
                $cModerators = $model->getModerators($model->id);
                if (!empty($cModerators)):
                    echo Yii::t('var', 'Модераторы') . ':';
                    ?>
                    <ul>
                        <?php foreach($cModerators as $cModerator): ?>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($cModerator['user_id']); ?>"
                                   class="admin-link-<?php echo CHtml::encode($cModerator['user_id']); ?>"><?php
                                    echo CHtml::encode($cModerator['name']); ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</aside>
<section id="group-content" class="f-left">
    <div class="content-block-wrap">
        <h2 class="sharp align-center"><?php echo CHtml::encode($model->name);?></h2>
        <ul class="tab-container">
            <li id="tab1-cell" onclick="showTab(1)">
                <a href="#tab1" class="sharp-white" title="">Основные</a>
            </li>
            <li id="tab2-cell" onclick="showTab(2)" class="two-words">
                <a href="#tab2" class="sharp-white" title="">Дополнительные<br />поля</a>
            </li>
            <li id="tab3-cell" onclick="showTab(3)">
                <a href="#tab3" class="sharp-white" title="">Заявки</a>
            </li>
            <li id="tab4-cell" onclick="showTab(4)">
                <a href="#tab4" class="sharp-white" title="">Черный список</a>
            </li>
            <li id="tab5-cell" onclick="showTab(5)">
                <a href="#tab5" class="sharp-white" title="">Отклонены</a>
            </li>
            <li id="tab6-cell" onclick="showTab(6)" class="two-words">
                <a href="#tab6" class="sharp-white" title="">Предложения<br />событий</a>
            </li>
            <li id="tab7-cell" onclick="showTab(7)">
                <a href="#tab7" class="sharp-white" title="">Модераторы</a>
            </li>
            <li class="gag"></li>
            <!--<li><a href="#" class="sharp-white" onclick="showTab(4)" title="">Оплата</a></li>-->
        </ul>
        <script>
            function showTab(tab) {
                $(".tabs").addClass('no-display');
                $(".tabs[tab=tab"+tab+"]").removeClass('no-display');
            }

            $(document).ready(function(){
                var hash = window.location.hash ? window.location.hash : '#tab1';
                $( hash + '-cell' ).addClass('active');
                showTab(parseInt(hash.substr(-1)));
                $('.tab-container li').on('click',function(){
                    if ( ! $(this).hasClass('active') && ! $(this).hasClass('gag') ) {
                        $('.tab-container li').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            });
        </script>
        <div class="content-block tabs" tab="tab1">
            <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
        </div>
        <div class="content-block no-display tabs b-add-fields" tab="tab2">
            <?
            $criteria = new CDbCriteria(array('condition' => "group_id=".(int)$model->id));
            $dataProvider2 = new CActiveDataProvider('AdditionalFields', array('criteria' => $criteria));

            echo '<table><tr><th style="width: 169px;">Название</th><th style="width: 137px;">Описание</th><th colspan="2">Обязательное</th></tr>';
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider2,
                'itemView'=>'_form_add_fields',
                'template'=>'{items}'
            ));
            echo "</table>";
            ?>
            <script>
                function deleteAdditionalField(id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('additionalFields/deleteAdditionalField');?>',
                        data: {
                            id: id,
                            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
                        },
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            </script>
            <form id="addAdditFieldForm" onsubmit="return false;">
                <table>
                    <tr>
                        <td>
                            <input type="text" name="name" class="search f-left input-border" style="width:150px;">
                        </td>
                        <td>
                            <input type="text" name="description" class="search f-left input-border" style="width:150px;">
                        </td>
                        <td>
                            <input type="checkbox" name="required" style="vertical-align: bottom;">
                        </td>
                        <td style="text-align: right;">
                            <button onclick="addAdditionalField();">Добавить</button>
                        </td>
                    </tr>
                </table>
                <input type="hidden" value="<?php echo CHtml::encode($model->id); ?>" name="group_id">
            </form>
            <script>
                function addAdditionalField() {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('additionalFields/addAdditionalField');?>',
                        data: $('#addAdditFieldForm').serialize() + "&YII_CSRF_TOKEN=" + '<?php echo Yii::app()->request->csrfToken;?>',
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            </script>
        </div>
        <div class="content-block no-display tabs" tab="tab3">
            <div>
                <?
                $criteria = new CDbCriteria;
                $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm=0';

                $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_requests',
                    'template'=>'{items}'
                ));
                ?>
            </div>
            <script>
                function acceptRequest(request_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('Member/acceptRequest');?>',
                        data: {
                            request_id: request_id,
                            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
                        },
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
                function refuseRequest(request_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('Member/refuseRequest');?>',
                        data: {
                            request_id: request_id,
                            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
                        },
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
                function addToBlackList(request_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('Member/addToBlackList');?>',
                        data: {
                            request_id: request_id,
                            YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>'
                        },
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
                function removeFromBlacklist(request_id, user_id, group_id) {
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo Yii::app()->createUrl('Member/removeFromBlacklist');?>',
                        data: {
                            request_id: request_id,
                            user_id: user_id,
                            group_id: group_id,
                            YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
                        },
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            </script>
        </div>
        <!--<div class="content-block no-display tabs" tab="tab4">
            <form class="payment-form">
                <select class="payment-methods" name="payment-methods">
                    <option value="sms">sms</option>
                    <option value="paypal">paypal</option>
                    <option value="webmoney">webmoney</option>
                    <option value="qiwi">qiwi</option>
                </select>
                <div class="payment-page-container">
                </div>
            </form>
        </div>-->
        <div class="content-block no-display tabs" tab="tab4">
            <div>
                <?
                $criteria = new CDbCriteria;
                $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm = 2';

                $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_blacklist',
                    'template'=>'{items}'
                ));
                ?>
            </div>
        </div>
        <div class="content-block no-display tabs" tab="tab5">
            <div>
                <?
                $criteria = new CDbCriteria;
                $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm = 3';

                $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_blacklist',
                    'template'=>'{items}'
                ));
                ?>
            </div>
        </div>
        <div class="content-block no-display tabs" tab="tab6">
            <div class="b-admin-meet-proposals">
                <?php $proposals = $model->getMeetProposals($model->id); ?>
                <?php if (empty($proposals)): ?>
                    <div class="b-admin-meet-proposals__empty">Никто не предлагал события</div>
                <?php else: ?>
                <ul class="b-admin-meet-proposals__list">
                    <?php foreach($proposals as $proposal): ?>
                        <li class="b-admin-proposal" id="mp-<?php echo CHtml::encode($proposal['id']); ?>">
                            <div class="b-admin-proposal__right">
                                <div class="non-approved">
                                    <form action="<?php echo Yii::app()->createUrl('group/creatingmeet'); ?>" method="get"
                                          class="b-admin-proposal__form" id="mp-approve-<?php echo CHtml::encode($proposal['id']); ?>">
                                        <input type="hidden" name="meet[id]" value="<?php echo CHtml::encode($proposal['id']); ?>" />
                                        <input type="hidden" name="meet[name]"
                                               value="<?php echo CHtml::encode($proposal['name']); ?>" class="f-name"/>
                                        <input type="hidden" name="meet[seats]"
                                               value="<?php echo CHtml::encode($proposal['seats']); ?>" class="f-seats"/>
                                        <input type="hidden" name="meet[description]"
                                               value="<?php echo CHtml::encode($proposal['comment']); ?>" class="f-comment"/>
                                        <input type="hidden" name="meet[date_start]"
                                               value="<?php echo CHtml::encode($proposal['date']); ?>" class="field-date"/>
                                        <input type="hidden" name="meet[date_end]"
                                               value="<?php echo CHtml::encode($proposal['date']); ?>" class="field-date"/>
                                        <input type="hidden" name="meet[city]" value="<?php echo CHtml::encode($model->city_id); ?>"/>
                                        <input type="hidden" name="meet[address]"
                                               value="<?php echo CHtml::encode($proposal['place']); ?>" class="f-place"/>
                                        <input type="hidden" name="parent" value="<?php echo CHtml::encode($model->id); ?>" />
                                        <input type="hidden" name="meet[any_time]" value="<?php echo CHtml::encode($proposal['any_time']);?>" class="field-any-time"/>

                                        <button class="b-admin-proposal__approve non-edit-mode">Подтвердить</button>
                                    </form>
                                    <a href="#" class="b-admin-proposal__edit"
                                        id="mp-edit-<?php echo CHtml::encode($proposal['id']); ?>">
                                        <span class="non-edit-mode">Редактировать</span>
                                        <span class="edit-mode">Закончить редактирование</span>
                                    </a>
                                    <br />
                                    <a href="<?php echo Yii::app()->createUrl('group/deletemeetproposal') . '?proposal_id=' . urlencode($proposal['id']); ?>"
                                       class="b-admin-proposal__delete non-edit-mode"
                                       id="mp-delete-<?php echo CHtml::encode($proposal['id']); ?>">Удалить</a>
                                </div><!-- .non-approved-->

                            </div><!-- .b-admin-meet-proposals__right-->
                            <div class="b-admin-proposal__left">
                                <table>
                                    <tr>
                                        <td class="b-admin-proposal__caption"><strong>Отправитель:</strong></td>
                                        <td class="b-admin-proposal__caption">
                                            <a href="/ru/user/view/<?php echo urlencode($proposal['user_id']); ?>" target="_blank">
                                                <?php echo CHtml::encode($proposal['user_name']); ?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Название:</strong>
                                        </td>
                                        <td>
                                            <input name="name" class="b-admin-proposal__edit-field edit-mode"
                                                   value="<?php echo CHtml::encode($proposal['name']); ?>"/>
                                            <span class="b-admin-proposal__field f-name
                                                non-edit-mode"><?php echo CHtml::encode($proposal['name']); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Место:</strong>
                                        </td>
                                        <td>
                                            <input name="place" class="b-admin-proposal__edit-field edit-mode"
                                                   value="<?php echo CHtml::encode($proposal['place']); ?>"/>
                                            <span class="b-admin-proposal__field f-place
                                                non-edit-mode"><?php echo CHtml::encode($proposal['place']); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Количество мест:</strong>
                                        </td>
                                        <td>
                                            <input name="seats" class="b-admin-proposal__edit-field edit-mode"
                                                   value="<?php echo CHtml::encode($proposal['seats']); ?>"/>
                                            <span class="b-admin-proposal__field f-seats
                                                non-edit-mode"><?php echo CHtml::encode($proposal['seats']); ?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Время:</strong>
                                        </td>
                                        <td>
                                            <?php $dateField = strtotime($proposal['date']); ?>
                                            <div class=" edit-mode">
                                                <input name="date" class="b-admin-proposal__edit-field b-jui-datepicker"
                                                       value="<?php echo CHtml::encode(date('d.m.Y',$dateField)); ?>"/>
                                                <br /><br />
                                                <span>Час:</span>
                                                <select name="time_hour" class="b-admin-proposal__edit-field b-meet-time-hour">
                                                    <option value="0">0</option>
                                                </select>
                                                <span>Минуты:</span>
                                                <select name="time_minute" class="b-admin-proposal__edit-field b-meet-time-minute">
                                                    <option value="0">0</option>
                                                </select>
                                                <br />
                                                <span>Любое время:</span>
                                                <?php
                                                $anyTimeChecked = '';
                                                if ( $proposal['any_time'] ) {
                                                    $anyTimeChecked = 'checked';
                                                }
                                                ?>
                                                <input name="any-time" class="b-any-time-input" id="any-time-<?php echo CHtml::encode($proposal['id']); ?>"
                                                       type="checkbox" value="1" <?php echo CHtml::encode($anyTimeChecked); ?> />
                                            </div>
                                            <div class="b-date-field non-edit-mode">
                                                <span class="b-date-field__date f-date"><?php echo CHtml::encode(date('d.m.Y',$dateField)); ?></span>
                                                <span class="b-date-field__time">
                                                    <span class="b-date-field__hour f-time_hour"><?php echo CHtml::encode(date('H',$dateField)); ?></span>:<span
                                                        class="b-date-field__minute f-time_minute"><?php echo CHtml::encode(date('i',$dateField)); ?></span>
                                                </span><!-- .b-date-field__time-->
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Комментарий: </strong>
                                        </td>
                                        <td>
                                            <textarea name="comment" class="b-admin-proposal__edit-field edit-mode"
                                                ><?php echo CHtml::encode($proposal['comment']); ?></textarea>
                                            <span class="b-admin-proposal__field f-comment
                                                non-edit-mode"><?php echo CHtml::encode($proposal['comment']); ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </div><!-- .b-admin-proposal__left-->
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div><!-- .b-admin-meet-proposals-->
        </div><!-- .tabs-->
        <div class="content-block no-display tabs" tab="tab7">
            <?php
            if ($isGroupMainAdmin) {
                $findMessage = 'Найти участника по имени и <br />предложить ему стать модератором:';
                $addCaption  = 'Предложить должность';
                $addLinkText = 'Предложить<br />должность';
                $fireCaption  = 'Снять с должности';
                $fireLinkText = 'Снять с<br />должности';
            } else {
                $findMessage = 'Найти участника по имени и <br />предложить его на должность модератора:';
                $addCaption  = 'Предложить на должность';
                $addLinkText = 'Предложить на<br />должность';
                $fireCaption  = 'Предложить снять с должности';
                $fireLinkText = 'Предложить снять <br />с должности';
            }
            ?>
            <div class="b-admin-moderators">
                <h4><strong>Модераторы:</strong></h4>
                <br />
                <?php
                $moderators = $model->getModerators($model->id);
                if (empty($moderators)) {
                    echo 'Ни один пользователь ещё не был назначен модератором.<br />';
                }
                foreach ($moderators as $moderator):
                    if ( ! $moderator['avatar'] ) {
                        $moderatorPic = '/images/user/mini_no-pic.png';
                    } else {
                        $moderatorPic = '/images/user/mini_' . CHtml::encode($moderator['avatar']);
                    }
                ?>
                    <div class="b-moderator">
                        <?php
                        $moderatorUrl = Yii::app()->createUrl('site/index') . '/user/' . urlencode($moderator['user_id']);
                        $fireLinkId = 'makeModReq-fire-' . CHtml::encode($moderator['user_id']);
                        ?>
                        <div class="b-moderator__info">
                            <a href="<?php echo $moderatorUrl; ?>" target="_blank">
                                <img src="<?php echo CHtml::encode($moderatorPic); ?>" class="b-moderator__pic" />
                                <br />
                                <?php echo CHtml::encode($moderator['name']); ?>
                            </a>
                        </div><!-- .b-moderator__info-->
                        <div class="b-moderator__button">
                            <span class="span-link b-moderator__make-request-link"
                                  id="<?php echo CHtml::encode($fireLinkId); ?>"><?php echo $fireLinkText; ?></span>
                        </div><!-- .b-moderator__button-->
                        <div style="clear: both;"></div>
                    </div><!-- .b-moderator-->
                <?php endforeach; ?>
                <div class="b-moderators-search">
                    <div><strong><?php echo $findMessage; ?></strong></div>
                    <div class="b-moderators-search__button">Найти</div>
                    <div class="b-moderators-search__names">
                        <input type="text" class="b-moderators-search__input" size="40"
                               placeholder="Введите имя участника группы" value="" />
                        <div class="b-moderators-search__answer"></div>
                    </div><!-- .b-moderators-search__names-->
                </div>
                <?php
                $moderatorsRequests = Group::model()->getModeratorsRequests($model->id);
                $moderatorsAdd = $moderatorsFire = array();
                foreach ($moderatorsRequests as $request) {
                    switch ($request['type']) {
                        case 'add':
                            $moderatorsAdd[] = $request;
                            break;
                        case 'fire':
                            $moderatorsFire[] = $request;
                            break;
                        default: ;
                    }
                }
                ?>
                <?php
                if (!empty($moderatorsRequests) && $isGroupMainAdmin):
                    $requestMessages = array(
                        'add'  => array(
                            'caption' => $addCaption . ':',
                            'link' => $addLinkText,
                        ),
                        'fire' => array(
                            'caption' => $fireCaption . ':',
                            'link' => $fireLinkText,
                        ),
                    );
                ?>
                    <div class="b-moderators-requests">
                        <h4 class="b-moderators-requests__caption"><strong>Предложения</strong></h4>
                        <?php
                        foreach (array($moderatorsAdd, $moderatorsFire) as $requestList):
                            if (empty($requestList)) {
                                continue;
                            }
                            $requestType = $requestList[0]['type'];
                        ?>
                            <div class="b-moderators-requests__<?php echo $requestType; ?>">
                                <h4 class="b-moderators-requests__caption">
                                    <strong><?php echo $requestMessages[$requestType]['caption']; ?></strong>
                                </h4>
                                <?php
                                foreach($requestList as $request):
                                    if ( ! $request['target_avatar'] ) {
                                        $userPic = '/images/user/mini_no-pic.png';
                                    } else {
                                        $userPic = '/images/user/mini_' . CHtml::encode($request['target_avatar']);
                                    }
                                    $satisfyRequestLinkId = 'satisfyModReq-' . CHtml::encode($requestType . '-' . $request['target_id']);
                                ?>
                                    <div class="b-moderator">
                                        <?php $userUrl = Yii::app()->createUrl('site/index') . '/user/' . urlencode($request['target_id']); ?>
                                        <div class="b-moderator__info">
                                            <a href="<?php echo $userUrl; ?>"
                                               target="_blank"><img src="<?php echo CHtml::encode($userPic); ?>" class="b-moderator__pic"
                                                    /><br /><?php echo CHtml::encode($request['target_name']); ?></a>
                                        </div><!-- .b-moderator__info-->
                                        <div class="b-moderator__button">
                                            <span class="span-link b-moderator__satisfy-request-link"
                                                  id="<?php echo CHtml::encode($satisfyRequestLinkId); ?>"><?php
                                                echo $requestMessages[$requestType]['link']; ?></span>
                                        </div><!-- .b-moderator__button-->
                                        <div style="clear: both;"></div>
                                    </div><!-- .b-moderator-->
                                <?php endforeach; ?>
                            </div><!-- .b-moderators-requests__<?php echo $requestType; ?>-->
                        <?php
                        endforeach;
                        ?>
                    </div><!-- .b-moderators-requests-->
                <?php endif; ?>
            </div>
        </div><!-- .tabs-->

    </div>
</section>
<script>
    function makeModeratorRequest(linkTag)
    {
        var id = $(linkTag).attr('id').split('-');
        var requestType = id[1];
        var userId = id[2];
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('group/makeModeratorRequest'); ?>',
            data: {
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
                type: requestType,
                group_id: <?php echo (int)$model->id; ?>,
                user_id: userId
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    $().ready(function() {
        $('.b-moderator__satisfy-request-link').click(function(){
            var id = $(this).attr('id').split('-');
            var requestType = id[1];
            var userId = id[2];
            $.ajax({
                type: 'GET',
                url: '<?php echo Yii::app()->createUrl('group/satisfyModeratorRequest'); ?>',
                data: {
                    YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
                    type: requestType,
                    group_id: <?php echo (int)$model->id; ?>,
                    user_id: userId
                },
                success: function(data){
                    window.location.reload();
                }
            });
        });
        $('.b-moderator__make-request-link').click(function(){
            makeModeratorRequest(this);
        });
        $('.b-moderators-search__button').click(function(){
            $('.b-moderators-search__input').keyup();
        });
        var searchAnswerTag = $('.b-moderators-search__answer');
        $('.b-moderators-search').focusout(function(){
            $('.b-moderators-search__answer').delay(100).slideUp('fast');
        });
        $('.b-moderators-search__input').focusin(function(){
            if (searchAnswerTag.text()) {
                searchAnswerTag.slideDown('fast');
            }
        }).keyup(function(){
            $.ajax({
                type: 'GET',
                url : '<?php echo Yii::app()->createUrl('ajax/findMembers'); ?>',
                data: {
                    YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>',
                    username: $(this).val(),
                    group_id: <?php echo (int)$model->id;?>
                },
                success: function(data){
                    var answer = '';
                    if (data != 0) {
                        $(JSON.parse(data)).each(function(index,member){
                            if ($(member.name).html()) {
                                member.name = $(member.name).html()
                            }
                            answer += '<div class="b-moderators-search__answer-unit">'
                                + '<span class="span-link b-moderator__add-link b-moderators-search__add" id="makeModReq-add-' + member.id + '">'
                                + '<?php echo $addCaption; ?></span>'
                                + '<div class="b-moderators-search__answer-unit-name">'
                                + '<a href="<?php echo Yii::app()->createUrl('user/view'); ?>/' + parseInt(member.id) + '" target="_blank">'
                                + member.name + '</a></div>'
                                + '</div>';
                        });
                    }
                    if (answer === '') {
                        answer = '<div class="b-moderators-search__answer-unit">Нет результатов</div>';
                    }
                    $('.b-moderators-search__answer').html(answer).slideDown('fast');
                    $('.b-moderators-search__add').click(function(){
                        makeModeratorRequest(this);
                    });
                }
            });
        });
        $(".b-admin-proposal__form").each(function(index,element){
            $(element).validate({
                rules: {
                    id: "required",
                    name: "required",
                    description: "required",
                    date_start: "required",
                    date_end: "required",
                    city: "required",
                    address: "required",
                    seats: "required",
                    parent: "required"
                },
                messages: {
                    text: "Обязательное поле"
                },
                submitHandler: function(form) {
                    form = element;
                    var propId = $(form).attr('id').split('-')[2];
                    var idSelector = '#mp-' + propId + ' ';

                    $.ajax({
                        type: 'GET',
                        url: '<? echo Yii::app()->createUrl('group/approvemeetproposal'); ?>',
                        data: $(form).serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                        success: function(data){
                            $(idSelector + '.b-admin-proposal__right').html('<div class="approved">Подтверждено</div>');
                            $(idSelector).delay(700).hide('drop',{},1000).fadeOut();
                        }
                    });
                }
            });
        });
        $('.b-any-time-input').each(function(){
            var propId = $(this).attr('id').split('-')[2];
            var idSelector = '#mp-' + propId + ' ';

            var timeFields = $(idSelector + '.b-meet-time-hour, ' + idSelector + '.b-meet-time-minute');

            $(this).change(function(){
                $(idSelector + '.b-date-field__time').toggle();
                if ( $(this).prop('checked') ) {
                    timeFields.attr('disabled', 'disabled');
                    $(idSelector + 'input.field-any-time').val(1);
                } else {
                    timeFields.removeAttr('disabled');
                    $(idSelector + 'input.field-any-time').val(0);
                }
            });

            if ( $(this).prop('checked') ) {
                timeFields.attr('disabled', 'disabled');
                $(idSelector + '.b-date-field__time').hide();
            } else {
                timeFields.removeAttr('disabled');
                $(idSelector + '.b-date-field__time').show();
            }
        });
        $('.b-admin-proposal__delete').click(function() {
            var propId = $(this).attr('id').split('-')[2];
            var idSelector = '#mp-' + propId + ' ';

            $.ajax({
                type: 'GET',
                url: $( this ).attr('href'),
                data: "YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                success: function(data){
                    $(idSelector + '.b-admin-proposal__right').html('<div class="deleted">Удалено</div>');
                    $(idSelector).delay(700).hide('drop',{},1000).fadeOut();
                }
            });
            return false;
        });
        $('.b-admin-proposal__edit').click(function(){
            var propId = $(this).attr('id').split('-')[2];
            var idSelector = '#mp-' + propId + ' ';

            if ( ! $(this).hasClass('editing') ) {
                $(idSelector + '.edit-mode').show();
                $(idSelector + '.non-edit-mode').hide();

                var hour = $(idSelector + '.b-date-field__hour').text();
                $(idSelector + '.b-meet-time-hour option[value=' + hour + ']').attr('selected', 'selected');
                var minute = $(idSelector + '.b-date-field__minute').text();
                $(idSelector + '.b-meet-time-minute option[value=' + minute + ']').attr('selected', 'selected');
            } else {
                $(idSelector + '.edit-mode').hide();
                $(idSelector + '.non-edit-mode').show();

                $(idSelector + '.b-admin-proposal__edit-field').each(function(index, element){
                    var el = $(element);
                    var fieldName = el.attr('name');
                    $(idSelector + 'span.f-' + fieldName).html( el.val() );
                    $(idSelector + 'input.f-' + fieldName).val( el.val() );
                });
                var dateFieldValue = $(idSelector + '.b-date-field.f-date').text() + ' '
                    + $(idSelector + '.b-date-field.f-time_hour').text() + ':'
                    + $(idSelector + '.b-date-field.f-time_minute').text();
                $(idSelector + '.b-admin-proposal__edit-field.field-date').val(dateFieldValue);
            }
            $(this).toggleClass('editing');

            return false;
        });
    });
</script>

<aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
<!--    <div class="sidebar-right" style="background: none;">
        <?/* if (!Yii::app()->user->isGuest /*&& !$isgroupadmin) { ?>
            <?/* if ($ismember == Null) { */?>
                <button onclick="$('#exampleModal').arcticmodal();" type="button" name="enter-group" class="align-center white enter-group"><span>Вступить в группу</span></button>
            <?/* } else if ($ismember == 1) { */?>
                <button onclick="leaveGroup(<?/* echo User::model()->getUserId().', '.$model->id;*/?>)" type="button" name="enter-group" class="align-center white enter-group"><span>Покинуть группу</span></button>
            <?/* } else { */?>
                <span>Вы подали заявку. Она рассматривается администратором группы</span>
            <?/* } */?>
        <?/* } */?>
        <button type="button" name="invite" class="align-center white enter-group invite" onclick="$('#mailto').arcticmodal();"><span>Пригласить друга</span></button>
    </div>-->
    <div class="sidebar-right">
        <h2 class="sidebar-right-title align-center sharp">Оплаченные группы</h2>
        <?
        $dataReader = Group::model()->getRandomPaidGroup($model->id);
        if (count($dataReader)>0)
            foreach($dataReader as $row) {
                echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']).'"><img src="'.(($row['picture']!=''&&$row['picture']!=NULL)?('/images/group/no-pic.pg'):('/images/group/no-pic.png')).'">'.CHtml::encode($row['name']).'</a>';
            }
        else echo 'Проплаченные группы не найдены';
        ?>
        <div class="sidebar-right-content"></div>
    </div>
    <div class="sidebar-right">
        <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;">Участники</h2>
        <div class="sidebar-right-content sidebar-right-content-participants">
            <div class="group-participants">
                <?
                $criteria = new CDbCriteria;
                $criteria->condition = 'group_id = ' . (int)$model->id . ' AND confirm = 1';

                $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'member',
                    'template'=>'{items}'
                ));
                ?>
            </div>
            <a href="<?php echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть всех</a>
        </div>
    </div>
    <div class="sidebar-right">
        <h2 class="sidebar-right-title align-center sharp">Фотографии группы</h2>
		<?php 
		$dataReader = Photoalbum::model()->getRandomPhoto($model->id);
		if(!empty($dataReader)):?>
        <div class="sidebar-right-content sidebar-right-content-photo-group">
            <?php
            echo '<img src="/images/album/'.CHtml::encode($dataReader['album_id'].'/'.$dataReader['link']).'" style="width:150px;height:150px;" alt="'.$dataReader['link'].'">';
            ?>
        </div>
        <a href="<? echo Yii::app()->createUrl('site/index').'/group/photos/'.urlencode($model->id); ?>" title="просмотреть все фотографии группы" class="show-all-participants align-center" style="width: 160px; height: auto;">просмотреть все фотографии группы</a>
	<?php else: ?>
        <a href="<? echo Yii::app()->createUrl('site/index').'/group/photos/'.urlencode($model->id); ?>" title="Добавить фотографии" class="show-all-participants align-center" style="width: 160px; height: auto;">Добавить фотографии</a>
	<?php endif; ?>
    </div>
</aside>
</div>


<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
                <?
                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                foreach($dataReader as $row) {
                    echo '<tr><td>'.CHtml::encode($row['name']).'</td>'
                        . '<td><input type="text" value="" name="fields['.CHtml::encode($row['id']).']"></td></tr>';
                }
                ?>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#addit-fields-form").validate({
                    rules: {
                        <?
                            $i = 0;
                            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                            foreach($dataReader as $row) {
                                if ($row['required']==1) {
                                    echo (($i>0)?',':'').CHtml::encode($row['id']).': "required"';
                                    $i++;
                                }
                             }
                        ?>
                    },
                    messages: {
                        <?
                                $i = 0;
                                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                                foreach($dataReader as $row) {
                                    if ($row['required']==1) {
                                        echo (($i>0)?',':'').CHtml::encode($row['id']).': "Обязательное поле"';
                                        $i++;
                                    }
                                 }
                            ?>
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('member/sendRequest'); ?>',
                            data: $('#addit-fields-form').serialize()+'&group_id=<?php echo urlencode($model->id); ?>'
                                + '&user_id=<?php echo urlencode(User::model()->getUserId()); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form"">
        <table>
            <tr><td>Введите текст жалобы</td><td><textarea name="text"></textarea></td></tr>
            <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
        </table>
        <input type="hidden" name="mid" value="">
        </form>

        <script>
            $().ready(function() {

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&text_id=&user_id=<?php echo urlencode(User::model()->getUserId()); ?>'+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;">Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="meet-proposal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="meet-proposal-form">
            <table>
                <tr><td>Введите место111</td><td><input type="text" name="place" value=""></td></tr>
                <tr><td>Введите дату</td><td><input type="text" name="date" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="comment" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<? echo $model->id; ?>">
        </form>
        <script>
            $().ready(function() {

                $("#meet-proposal-form").validate({
                    rules: {
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        place: "Обязательное поле",
                        date: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('group/meetproposal'); ?>',
                            data: $('#meet-proposal-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
