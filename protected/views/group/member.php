<?
$picture = User::model()->getPicture($data->user_id);
$name = User::model()->getUserName($data->user_id);
if ($picture == '' || $picture == NULL) {
    $picture = '/images/user/mini_no-pic.png';
} else {
    $picture = "/images/user/mini_".$picture;
}
?>
<a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>" title="#">
    <img src="<?php echo CHtml::encode($picture); ?>" alt="<?=$name?>" style="width: 70px; height: 60px;"/>
    <br />
    <?php echo CHtml::encode($name); ?>
</a>
