<?php
/* @var $this PhotoalbumController */
/* @var $model Photoalbum */
/* @var $form CActiveForm */


$this->breadcrumbs=array(
    $group->name => Yii::app()->createUrl('group/view') . '/' . urlencode($group->id),
    'Создание альбома'
);
?>

<div class="center">
    <div class="form">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'photoalbum-form',
            'enableAjaxValidation'=>false,
        )); ?>

        <?php echo  $form->errorSummary($album); ?>

        <table style="width: 400px; margin: 0 auto">
            <tr>
                <td><?php echo $form->labelEx($album,Yii::t('var','name')); ?></td>
                <td>
                    <?php echo $form->textField($album,'name',array('size'=>60,'maxlength'=>100, 'class'=> 'select-input ')); ?>
                    <?php echo $form->error($album,'name'); ?>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle"><?php echo $form->labelEx($album,Yii::t('var','description')); ?></td>
                <td>
                    <?php echo $form->textArea($album,'description',array('style' => 'width:250px; height:95px; margin-top:10px', 'class'=> 'select-input')); ?>
                    <?php echo $form->error($album,'description'); ?>
                </td>
            </tr>
            <tr><td></td>
                <td>
                    <?php echo CHtml::submitButton($album->isNewRecord ? Yii::t('var','Создать') : Yii::t('var','Сохранить'), array('class' => 'registration input-border')); ?>
                </td>
            </tr>
        </table>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->
</div>
