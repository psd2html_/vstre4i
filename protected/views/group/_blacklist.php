<?php
/* @var $this GroupController */
/* @var $data Group */
?>

<div class="request b-blacklist__item">
    <?php
    $picture = User::model()->getPicture($data->id);
    if ($picture == '' || $picture == NULL) $picture = '/images/user/no-pic.png';
    ?>
    <a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>"><img src="<?php echo CHtml::encode($picture); ?>" style="width: 100px;"></a>
    <br>
    <a href="<?php echo Yii::app()->createUrl('site/index').'/user/'.urlencode($data->user_id); ?>">
        <?php echo User::model()->getName($data->user_id); ?>
    </a>
    <br />
    <?php $rmBlackListFuncArguments = (int)$data->id.','.(int)$data->user_id.','.(int)$data->group_id; ?>
    <button class="b-blacklist__button"
            onclick="removeFromBlacklist(<?php echo $rmBlackListFuncArguments; ?>)">Удалить из списка</button>
</div>