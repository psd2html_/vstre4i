<?php
/* @var $this GroupController */
/* @var $model Group */

function showCommentsRecursive($comments)
{
    static $user_id = -1;
    if ($user_id == -1) {
        $user_id = User::model()->getUserId();
    }

    foreach($comments as $comment):
        if (empty($comment['text'])) {
            continue;
        }
        $date = explode(' ', CHtml::encode($comment['date']));
        $date[0] = '<strong>' . CHtml::encode(date('d.m.Y', strtotime($date[0]))) . '</strong> ';
        $date = implode($date);

        $answerModificator = empty($comment['children'])
            ? ''
            : 'b-comment__answer_border';
        ?>
        <div class="b-comment" id="comment-<?php echo CHtml::encode($comment['id']); ?>">
            <div class="b-comment__content">
                <div class="b-comment__head">
                    <div class="b-comment__author">
                        <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($comment['sender_id']); ?>"
                           target="_blank"><?php echo CHtml::encode($comment['sender_name']); ?></a>
                    </div>
                    <div class="b-comment__date">[<?php echo $date; ?>]</div>
                </div><!-- .b-comment__head-->
                <div class="b-comment__body"><?php echo CHtml::encode($comment['text']); ?></div>
                <?php if ($user_id): ?>
                    <div class="b-comment__buttons" id="answer-buttons-<?php echo (int)$comment['id']; ?>">
                        <a href="#" class="b-comment__answer-close"
                           onclick="closeAnswerForm(<?php echo (int)$comment['id']; ?>);return false;">Закрыть</a>
                        <a href="#" class="b-comment__answer-open"
                           onclick="showAnswerForm(<?php echo (int)$comment['id']; ?>, this);return false;">Ответить</a>
                    </div><!-- .b-comment__buttons-->
                <?php endif; ?>
            </div><!-- .b-comment__content-->
            <div class="b-comment__answer <?php echo $answerModificator; ?>" id="answer-<?php echo (int)$comment['id']; ?>">
                <div class="b-comment__answer-form-block"></div>
                <?php if (!empty($comment['children'])): ?>
                    <div class="b-comment__answer-body">
                        <?php showCommentsRecursive($comment['children']); ?>
                    </div><!-- .b-comment__answer-body-->
                <?php endif; ?>
            </div><!-- .b-comment__answer-->
        </div><!-- .b-comment-->
    <?php
    endforeach;
}

$request = Yii::app()->request;

$meet = Group::model()->findByPk(
    (int)$request->getParam('mid', 0)
);
$parent = Group::model()->findByPk((int)$meet->parent);

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js');

$this->setPageTitle($meet->name . ' | ' . $parent->name . ' | ' . $this->pageTitle);
$description = $meet->description ? $meet->description : $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description');
}

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";

$this->breadcrumbs=array(
    $parent->name => Yii::app()->createUrl('group/view') . '/' . urlencode($parent->id),
    $meet->name,
);

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

$user_id = User::model()->getUserId();

$isgroupadmin = Group::model()->isGroupAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);

$hide = $model->hided && ! $ismember;
if ($hide) {
    $request->redirect(Yii::app()->createUrl('group/view') . '/' . urlencode($model->id));
}

$free_seats = $meet->seats - Member::model()->getRequestsCount($meet->id);
?>


<div class="center event-page">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) {
                    $picture = $model->picture;
                } else {
                    $picture = 'mini_no-pic.png';
                }
                ?>
                <img src="/images/group/mini_<?php echo CHtml::encode($picture); ?>" style="" alt="Группа <?=$model->name;?>">
            </div>
            <?php if ($isgroupadmin == 1): ?>
                <a onclick="window.location.href='<?php echo Yii::app()->createUrl('site/index').'/group/update/'.urlencode($model->id); ?>'" title="настройки" class="complain align-center"><?php echo Yii::t('var', 'Настройки');?></a>
            <?php endif; ?>

        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><?php echo CHtml::encode($groupCity).', '.CHtml::encode($groupCountry);?></li>
                    <li>Группа создана:
                        <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                    <li>Участники:
                        <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                    <li>Будущие события:
                        <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                    <li>Прошедшие события:
                        <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                    <li><span class="calend-span"><?php
                            echo Yii::t('var', 'Календарь группы');
                            ?>: </span><span class="calend-link"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-link').click(function(){
                                window.open('<?php echo $groupLink; ?>?calend=1', '_blank');
                            });
                            $('.calend-show, .calend-close').on('click',function(){
                                $('.calend').slideToggle();
                            })
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <?php echo Yii::t('var', 'Ключевые слова'); ?>:
                    <ul>
                        <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
						$keywords = '';
                        foreach($dataReader as $row) {
                            $keywordLink = Yii::app()->createUrl('site/index')
                                . '/?key=' . urlencode($row['name'])
                                . '&cat_id=' . urlencode($row['cat_id']);
                            echo '<li><a href="' . $keywordLink . '">'
                                . CHtml::encode($row['name']) . '</a></li>';
							if($row['name'])	
								$keywords .= $row['name'].', ';
							$i++;
                        } 
						if($keywords){
							$keywords = substr($keywords,0,-2);
							Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords).', '.Admin::getMainKeywords(), 'keywords');
						}
						?>
                    </ul>
                </div>
                <div class="organizers">Организаторы:
                    <ul>
                        <?
                        $dataReader = Member::model()->getModeratorsList($model->id);
                        foreach($dataReader as $row) {
                            echo '<li><a href="'.Yii::app()->createUrl('site/index').'/user/'.urlencode($row['user_id']).'">'
                                . CHtml::encode($row['name']).'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="sidebar-right" style="margin-top: 10px;">
            <h2 class="sidebar-right-title align-center sharp sharp-white">Случайные группы</h2>
            <?php
            $dataReader = Group::model()->getRandomGroup($model->id);
            if (count($dataReader)>0) {
                foreach($dataReader as $row) {
                    $randomGroupPic = $row['picture'] != '' && $row['picture'] != NULL
                        ? '/images/group/mini_' . $row['picture']
                        : '/images/group/mini_no-pic.png';
                    echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']).'">'
                        . '<img alt="Группа '.CHtml::encode($row['name']).'" style="width:187px;height:189px;" src="'.CHtml::encode($randomGroupPic).'">'.CHtml::encode($row['name'])
                        .'</a>';
                }
            } else {
                echo 'Похожей группы не найдено';
            }
            ?>
            <div class="sidebar-right-content"></div>
        </div>
    </aside>

    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center sharp-white"><?php echo CHtml::encode($meet->name); ?></h2>
            <div class="content-block">
                <div class="data"><?php echo CHtml::encode(date('d.m.Y',strtotime($meet->date_start))); ?></div >
                <div class="time"><?php
                    if ($meet->date_end == '0000-00-00 00:00:00') {
                        echo 'Любое время';
                    } else {
                        echo CHtml::encode(date('H:i',strtotime($meet->date_start)));
                    }
                    ?>
                </div >
                <div class="place"><?php
                    $city = City::model()->getCity($meet->city_id);
                    $address = 'г. ' . CHtml::encode($city) . ', ' . CHtml::encode($meet->address);
                    ?>
                    <a href="http://www.google.ru/maps/search/<?php echo urlencode($address); ?>"
                       target="_blank" class="place-link"><?php echo CHtml::encode($address); ?></a>
                </div>
                <?php if($parent): ?>
                    <div>
                        <?php echo Yii::t('var', 'Группа'); ?>:&nbsp;
                        <a href="<?php echo Yii::app()->createUrl('site/index') . '/group/' . urlencode($parent->id); ?>"
                           target="_blank"><?php echo CHtml::encode($parent->name); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="content-block-wrap">
            <div class="content-block">
                <?php echo CHtml::encode($meet->description); ?>
            </div>
        </div>

        <div class="content-block-wrap calend">
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Календарь');?></h2>
            <div class="calend-close">[<?php echo Yii::t('var', 'закрыть');?>]</div>
            <div class="content-block">
                <?php

                Yii::import('application.extensions.calendar.classes.calendar', true);

                $month = isset($_GET['m']) ? $_GET['m'] : NULL;
                $year  = isset($_GET['y']) ? $_GET['y'] : NULL;

                $calendar = Calendar::factory($month, $year);

                $events = array();

                $dataReader = Group::model()->getGroupMeetsID($model->id);
                foreach ($dataReader as $val) {
                    $events[] = $calendar->event()
                        ->condition('timestamp', strtotime($val['date_start']))
                        ->title('Hello All')
                        ->output('<a href="'.Yii::app()->createUrl('site/index').'/group/meet/'.urlencode($model->id).'/?mid='.urlencode($val['id']).'">'
                            .CHtml::encode(date('H:i',strtotime($val['date_start']))).'<br>'.CHtml::encode($val['name']).'</a>');
                }

                $calendar->standard('today')->standard('prev-next');

                foreach ($events as $val) {
                    $calendar->attach($val);
                }
                ?>
                <table class="calendar">
                    <thead>
                    <tr class="navigation">
                        <th class="prev-month"><a href="<?php echo $calendar->prev_month_url(); ?>"><?php echo $calendar->prev_month() ?></a></th>
                        <th colspan="5" class="current-month"><?php echo $calendar->month(); ?> <?php echo $calendar->year ?></th>
                        <th class="next-month"><a href="<?php echo $calendar->next_month_url(); ?>"><?php echo $calendar->next_month() ?></a></th>
                    </tr>
                    <tr class="weekdays">
                        <?php foreach ($calendar->days() as $day): ?>
                            <th><?php echo $day ?></th>
                        <?php endforeach ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($calendar->weeks() as $week): ?>
                        <tr>
                            <?php foreach ($week as $day): ?>
                                <?php
                                list($number, $current, $data) = $day;

                                $classes = array();
                                $output  = '';

                                if (is_array($data))
                                {
                                    $classes = $data['classes'];
                                    $title   = $data['title'];
                                    $output  = empty($data['output']) ? '' : '<ul class="output"><li>'.implode('</li><li>', $data['output']).'</li></ul>';
                                }
                                ?>
                                <td class="day <?php echo implode(' ', $classes) ?>">
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                    <div class="day-content">
                                        <?php echo $output ?>
                                    </div>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <style>
                    .calendar {width:100%; border-collapse:collapse;}
                    .calendar tr.navigation th {padding-bottom:20px;}
                    .calendar th.prev-month {text-align:left;}
                    .calendar th.current-month {text-align:center; font-size:1.5em;}
                    .calendar th.next-month {text-align:right;}
                    .calendar tr.weekdays th {text-align:left;}
                    .calendar td {width:14%; height:73px; vertical-align:top; border:1px solid #CCC;}
                    .calendar td.today {background:#FFD;}
                    .calendar td.prev-next {background:#EEE;}
                    .calendar td.prev-next span.date {color:#9C9C9C;}
                    .calendar td.holiday {background:#DDFFDE;}
                    .calendar span.date {display:block; padding:4px; line-height:12px; background:#EEE;}
                    .calendar div.day-content {}
                    .calendar ul.output {margin:0; padding:0 4px; list-style:none;}
                    .calendar ul.output li {margin:0; padding:5px 0; line-height:1em; border-bottom:1px solid #CCC;}
                    .calendar ul.output li:last-child {border:0;}

                        /* Small Calendar */
                    .calendar.small {width:auto; border-collapse:separate;}
                    .calendar.small tr.navigation th {padding-bottom:5px;}
                    .calendar.small tr.navigation th a {font-size:1.5em;}
                    .calendar.small th.current-month {font-size:1em;}
                    .calendar.small tr.weekdays th {text-align:center;}
                    .calendar.small td {width:auto; height:auto; padding:4px 8px; text-align:center; border:0; background:#EEE;}
                    .calendar.small span.date {display:inline; padding:0; background:none;}
                </style>
            </div>
        </div>
        <? if ($user_id!=0) { ?>
        <div class="content-block-wrap">
            <div class="content-block" id="comments">
                <?php
                $comments = Group::model()->getFormatedComments($meet->id);
                if (count($comments)):
                    ?>
                    <label for="comment" class="f-left"><h3 class="sharp"><? echo Yii::t('var', 'Комментарии');?>:</h3></label>
                <?php else: ?>
                    <label for="comment" class="f-left">
                        <h3 class="sharp">
                            <?php echo Yii::t('var', 'Здесь пока никто ничего не писал.'); ?>
                            <br />
                            <?php echo Yii::t('var', 'Вы можете сделать это первым:'); ?>
                        </h3>
                    </label>
                <?php endif; ?>
                <div class="clear"></div>
                <?php

                showCommentsRecursive($comments);

                if ($user_id):
                    ?>
                    <form id="wall-form" action="<?php echo Yii::app()->request->baseUrl; ?>/group/sentComment"
                          name="sent-record" method="post">
                        <textarea class="b-comment__meet-textarea f-left" name="text"></textarea><br>
                        <input class="f-right" type="image" src="/images/send-comment.png" />
                        <input type="hidden" name="meet_id" value="<?php echo CHtml::encode($meet->id); ?>" />
                        <input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />
                        <input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />
                    </form>
                <?php endif; ?>
            </div>
        </div>
        <? } ?>
    </section>

    <aside id="#SideRight" class="f-right" style="width: 200px; margin: 0 2px 10px 7px;">

        <?php if ($user_id!=0 && $ismember): ?>
            <div class="sidebar-right">
                <h2 class="sidebar-right-title align-center sharp sharp-white" style="margin-bottom: 0;">Вы участвуете?</h2>
                <div class="sidebar-right-content sidebar-right-content-participants">
                    <p class="sharp">Всего мест: <?php echo CHtml::encode($meet->seats);?></p>
                    <p class="sharp">Осталось мест: <?php echo CHtml::encode($free_seats); ?></p>
                    <?php if ($free_seats>0 && Member::model()->isMeetMember($meet->id,$user_id)==NULL): ?>
                        <p><strong>Вы не идёте на встречу</strong></p>
                        <input type="button" name="yes" class="yes" value="Пойти на встречу" onclick="joinMeet(<?php echo (int)$user_id.', '.(int)$meet->id;?>)" />
                    <?php endif ?>
                    <?php if (Member::model()->isMeetMember($meet->id,$user_id)!=NULL): ?>
                        <p><strong>Вы идёте на встречу</strong></p>
                        <input type="button" name="no" class="no" value="Отказаться" onclick="leaveMeet(<?php echo (int)$user_id.', '.(int)$meet->id;?>)" />
                    <?php endif; ?>
                </div>
            </div>
        <?php else: ?>
            <div class="sidebar-right">
                <div class="sidebar-right-content sidebar-right-content-participants">
                    <p>Чтобы принять участие во встрече, вступите в <a href="<?php echo Yii::app()->createUrl('group/view') . '/' . urlencode($parent->id); ?>">группу</a></p>
                </div>
            </div>
        <?php endif; ?>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp sharp-white" style="margin-bottom: 0;">Участники</h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = ' . (int)$meet->id . ' AND confirm = 1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member_list',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
            </div>

            <a href="<? echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id).'/?mid='.urlencode($meet->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center">просмотреть всех</a>
        </div>
        <? if ($isgroupadmin != 1 && $user_id!=0): 
		?>
            <div class="sidebar-right">
                <a mid="<?php echo CHtml::encode($model->id); ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на dcnhtxe" class="complain align-center b-complain-button">
                    <?php echo 'Пожаловаться на встречу';?>
                </a>
            </div>
        <? endif; 
		?>
        <div class="sidebar-right" style="margin-top: 10px;">
            <h2 class="sidebar-right-title align-center sharp sharp-white">Поделиться встречей</h2>
            <script type="text/javascript" src="//yandex.st/share/share.js"
                    charset="utf-8"></script>
            <div class="yashare-auto-init" data-yashareL10n="ru"
                 data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir"></div>
        </div>
    </aside>
</div>





<script>
    function closeAnswerForm(parentId)
    {
        $('#answer-' + parentId + ' > .b-comment__answer-form-block').slideUp('fast').html('');
        $('#answer-buttons-' + parentId).removeClass('active');

        return false;
    }
    function closeParentForm(){
		$('.prependBlock').remove();
	}
    function showAnswerForm(parentId, that)
    {
        /* parentId = parseInt(parentId);
        $('.b-comment__answer-form-block').slideUp('fast').html('');
        $('.b-comment__buttons').removeClass('active');

        var formHtml = '<form id="wall-form-' + parentId + '" action="<?php echo Yii::app()->request->baseUrl; ?>/group/sentComment"'
            + ' name="sent-record-' + parentId + '" method="post" class="">'
            + '<textarea class="b-comment__answer-textarea f-left" name="text"></textarea><br>'
            + '<input class="b-comment__answer-submit f-right" type="image" src="/images/send-comment.png" />'
            + '<input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>" />'
            + '<input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />'
            + '<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />'
            + '<input type="hidden" name="parent_id" value="' + parentId + '" /><div class="clear"></div></form>';

        $('#answer-' + parentId + ' > .b-comment__answer-form-block').html(formHtml).slideDown('fast');
        $('#answer-buttons-' + parentId).toggleClass('active');

        return false; */
		$('.prependBlock').remove();
		f = $(that).closest('.b-comment__content').find('.b-comment__author a').text();
		console.log($(that).parent().parent(),'888');
		$('.b-comment__textarea.f-left').focus();
		$('#wall-form').prepend("<div class='prependBlock' style='color:grey'>Ответ для "+f+" <span class='closeParentForm' style='cursor: pointer;' title='Закрыть' onclick='closeParentForm()'>(<b style='color:#4A9DD9;'>х</b>)<span></div><input class='prependBlock' type='hidden' name='parent_id' value='"+parentId+"' />");
    }
</script>

<script>
    function joinMeet(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/joinMeet');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveMeet(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/leaveMeet');?>',
            data: {
                id: user_id,
                group_id: group_id,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function addMeetRequest(){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('member/addMeetRequest');?>',
            data: {
                id: id,
                group_id: <?php echo (int)$model->id; ?>,
                YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function removeMeetRequest(){
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('member/removeMeetRequest');?>',
            data: {
                id: id,
                group_id: <?php echo (int)$model->id; ?>,
                YII_CSRF_TOKEN: '<?php echo Yii::app()->request->csrfToken;?>'
            },
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>






<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
                <?
                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                foreach($dataReader as $row) {
                    echo '<tr><td>'.CHtml::encode($row['name']).'</td>'
                        . '<td><input type="text" value="" name="fields['.CHtml::encode($row['id']).']"></td></tr>';
                }
                ?>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#addit-fields-form").validate({
                    rules: {
                        <?
                            $i = 0;
                            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                            foreach($dataReader as $row) {
                                if ($row['required']==1) {
                                    echo (($i>0)?',':'').CHtml::encode($row['id']).': "required"';
                                    $i++;
                                }
                             }
                        ?>
                    },
                    messages: {
                        <?
                                $i = 0;
                                $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                                foreach($dataReader as $row) {
                                    if ($row['required']==1) {
                                        echo (($i>0)?',':'').CHtml::encode($row['id']).': "Обязательное поле"';
                                        $i++;
                                    }
                                 }
                            ?>
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('member/sendRequest'); ?>',
                            data: $('#addit-fields-form').serialize()+'&group_id=<?php echo urlencode($model->id); ?>'
                                +'&user_id=<?php echo urlencode(User::model()->getUserId()); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr>
                    <td><br /><?php echo Yii::t('var', 'Введите текст жалобы');?>:<br /><br /></td>
                </tr>
                <tr>
                    <td>
                        <textarea cols="72" rows="9" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>">
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mid" value="<?php echo (int)$meet->id; ?>">
            <input type="hidden" name="text_id" value="meet">
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "Обязательное поле"
                    },
                    submitHandler: function(form) {
						$.arcticmodal('close');
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&item_id=<?php echo urlencode($model->id); ?>'
                                + '&user_id=<?php echo urlencode(User::model()->getUserId()); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td>Введите имя друга</td><td><input type="text" name="name" value=""></td></tr>
                <tr><td>Введите email друга</td><td><input type="text" name="email" value=""></td></tr>
                <tr><td>Введите комментарий</td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<? echo $model->id; ?>">
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                var height = $('.group-img img').height();
                var width = $('.group-img img').width();
                var left,top;
                if (width > height) {
                    $('.group-img img').css({'height':'192px'});
                    width = $('.group-img img').width();
                    left = (width - 185.5)/2;
                    $('.group-img img').css({'left':'-'+left+'px'});
                }
                else {
                    $('.group-img img').css({'width':'185.5px'});
                    height = $('.group-img img').height();
                    top = (height - 192)/2;
                    $('.group-img img').css({'top':'-'+top+'px'});
                }

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: "required",
                        comment: "required"
                    },
                    messages: {
                        name: "Обязательное поле",
                        email: "Обязательное поле",
                        comment: "Обязательное поле"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
