<?php
/* @var $this GroupController */
/* @var $model Group */
Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

$this->setPageTitle($model->name . ' - '. Yii::app()->name);
$description = $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description');
}

if ($model->background && $model->background!='') {
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";
}

if ($model->background && $model->background!='')
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";

$this->breadcrumbs = array(
    $model->name,
);

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

$user_id = User::model()->getUserId();
$isGroupMainAdmin = Group::model()->isGroupMainAdmin($model->id,$user_id);
$ismember = Member::model()->isMember($user_id,$model->id);
$groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($model->id);
if (!$isGroupMainAdmin) {
    Yii::app()->request->redirect('/');
}

$mainAdmin = $model->getGroupAdmin($model->id);
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) {
                    $picture = $model->picture;
                } else {
                    $picture = 'mini_no-pic.png';
                }
                ?>
                <img src="/images/group/<?php echo CHtml::encode($picture); ?>" style="">
            </div>
            <?php if ($isGroupMainAdmin == 1 && $model->approved) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.urlencode($model->id); ?>'"
                   title="<?php echo Yii::t('var', 'Управление группой'); ?>" class="complain align-center b-settings-button">
                    <?php echo Yii::t('var', 'Управление группой');?>
                </a>
            <? } ?>

        </div>
        <div class="sidebar-left">
            <div class="group-info">
                <ul>
                    <li><?php echo CHtml::encode($groupCity).', '.CHtml::encode($groupCountry);?></li>
                    <li><?php echo Yii::t('var', 'Группа создана');?>:
                        <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                    <li><?php echo Yii::t('var', 'Участники');?>:
                        <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Будущие события');?>:
                        <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Прошедшие события');?>:
                        <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                    <li><span class="calend-span"><?php
                            echo Yii::t('var', 'Календарь группы');
                            ?>: </span><span class="calend-link"></span></li>
                    <script>
                        $(document).ready(function(){
                            $('.calend-link').click(function(){
                                window.open('<?php echo $groupLink; ?>?calend=1', '_blank');
                            });
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <?php echo Yii::t('var', 'Ключевые слова'); ?>:
					<ul>
                        <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
						$keywords = '';
                        foreach($dataReader as $row) {
                            $keywordLink = Yii::app()->createUrl('site/index')
                                . '/?key=' . urlencode($row['name'])
                                . '&cat_id=' . urlencode($row['cat_id']);
                            echo '<li><a href="' . $keywordLink . '">'
                                . CHtml::encode($row['name']) . '</a></li>';
							if($row['name'])	
								$keywords .= $row['name'].', ';
							$i++;
                        } 
						if($keywords){
							$keywords = substr($keywords,0,-2);
							Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords).', '.Admin::getMainKeywords(), 'keywords');
						}
						?>
                    </ul>
                </div>
                <div class="organizers">
                    <?php echo Yii::t('var', 'Администратор') . ':';?>
                    <ul>
                        <li>
                            <div class="organizers__contact-button"
                                 id="adm-con-<?php echo CHtml::encode($mainAdmin['user_id']); ?>"></div>
                            <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($mainAdmin['id']); ?>"
                               class="admin-link-<?php echo CHtml::encode($mainAdmin['user_id']); ?>" target="_blank"><?php
                                echo CHtml::encode($mainAdmin['name']); ?></a>
                        </li>
                    </ul>
                    <?php
                    $cModerators = $model->getModerators($model->id);
                    if (!empty($cModerators)):
                        echo Yii::t('var', 'Модераторы') . ':';
                        ?>
                        <ul>
                            <?php foreach($cModerators as $cModerator): ?>
                                <li>
                                    <div class="organizers__contact-button"
                                         id="mod-con-<?php echo CHtml::encode($cModerator['user_id']); ?>"></div>
                                    <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($cModerator['user_id']); ?>"
                                       class="admin-link-<?php echo CHtml::encode($cModerator['user_id']); ?>" target="_blank"><?php
                                        echo CHtml::encode($cModerator['name']); ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <h2 class="sharp align-center"><?php echo CHtml::encode($model->name); if ($isGroupMainAdmin == 1) echo ' '.Yii::t('var', '(вы - администратор)') ?></h2>
            <div class="content-block">
                <p class="group-description">
                    Ваша группа была успешно создана, но пока-что не доступна для просмотра другим пользователям. <br />Группа будет доступна для просмотра после проверки модератором (обычно это занимает до 48 часов).
                </p>
            </div>
        </div>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Участники');?></h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm = 1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
                <a href="<?php echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><?php echo Yii::t('var', 'просмотреть всех');?></a>
            </div>
        </div>
    </aside>
</div>







<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
            <?
            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
            foreach($dataReader as $row) {
                echo "<tr><td>".CHtml::encode($row['name'])."</td>'
                . '<td><input type='text' value='' name='fields[".CHtml::encode($row['id'])."]'></td></tr>";
            }
            ?>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите имя друга');?></td><td><input type="text" name="name" value=""></td></tr>
                <tr><td><? echo Yii::t('var', 'Введите email друга');?></td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;"><? echo Yii::t('var', 'Введите комментарий');?></td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        comment: "required"
                    },
                    messages: {
                        name: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        email: "<?php echo Yii::t('var', 'Введите правильный email');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="contact-admin">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="contact-admin-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите ваше сообщение');?></td><td><textarea type="comment" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
        </form>
        <script>
            $().ready(function() {

                $("#contact-admin-form").validate({
                    rules: {
                        place: "required",
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        place: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        date: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('group/contactadmin'); ?>',
                            data: $('#contact-admin-form').serialize()+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
<script>
    function sentComment(meet_id,user_id) {
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('group/sentComment');?>',
            data: $('#comments-form').serialize() + "&user_id=" + parseInt(user_id)
                + "&meet_id=" + parseInt(meet_id)
                + "&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
            success: function(data){
                window.location.reload();
            }
        });
    }
</script>
<script>
    $(document).ready(function(){
        $('#сontact-us').on('change',function(){
            $('#contact-admin').arcticmodal();
        });

        var height = $('.group-img img').height();
        var width = $('.group-img img').width();
        var left,top;
        if (width > height) {
            $('.group-img img').css({'height':'192px'});
            width = $('.group-img img').width();
            left = (width - 185.5)/2;
            $('.group-img img').css({'left':'-'+left+'px'});
        }
        else {
            $('.group-img img').css({'width':'185.5px'});
            height = $('.group-img img').height();
            top = (height - 192)/2;
            $('.group-img img').css({'top':'-'+top+'px'});
        }


    });
</script>
