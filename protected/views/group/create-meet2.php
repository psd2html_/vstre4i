<?php
/* @var $this GroupController */
/* @var $model Group */

$this->breadcrumbs=array(
    'Групы'=>array('index'),
    'Создать',
);
Yii::app()->clientScript
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/now-date-input.js')
    ->registerCssFile('http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.0/themes/base/jquery-ui.css');
?>

<div class="center">
    <div class="white-block">
        <h1 class="sharp align-center"><?php echo Yii::t('var', 'Создание встречи');?></h1>
        <form id="create-meet-form" class="b_input" method="post">
            <label for="name">Название</label><input type="text" name="meet[name]" value=""><br>
            <label for="description">Описание</label><input type="text" name="meet[description]" value=""><br>
            <label for="date_start">Дата начала</label><input class="b-jui-datepicker" type="text" name="meet[date_start]" value=""><br>
            <label for="date_end">Дата окончания</label><input class="b-jui-datepicker" type="text" name="meet[date_end]" value=""><br>
            <label for="city">Город</label><input type="text" name="meet[city]" value=""><br>
            <label for="address">Адресс</label><input type="text" name="meet[address]" value=""><br>
            <label for="seats">Место проведения</label><input type="text" name="meet[seats]" value=""><br>
			<br>
            <button onclick="createMeet()">Создать</button>
        </form>
        <script>
             /*function createMeet(){
                $.ajax({
                    type: 'POST',
                    url: '<?php echo Yii::app()->createUrl('group/creatingmeet');?>',
                    data: $('#create-meet-form').serialize()+'&parent=<?php echo (int)$model->id; ?>&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>',
                    success: function(data){
                        window.location.href="<?php echo Yii::app()->createUrl('site/index').'/group/meet/'.urlencode($model->id).'/?mid='; ?>"+data;
                    }
                });
            }*/
        </script> 
    </div>
</div>
