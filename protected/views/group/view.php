<?php
/* @var $this GroupController */
/* @var $model Group */

function showCommentsRecursive($comments)
{
	if($comment['depth'] > 1) return;
    static $user_id = -1;
    if ($user_id == -1) {
        $user_id = User::model()->getUserId();
    }

    foreach($comments as $comment):
        if (empty($comment['text'])) {
            continue;
        }
        $date = explode(' ', CHtml::encode($comment['date']));
        $date[0] = '<strong>' . CHtml::encode(date('d.m.Y', strtotime($date[0]))) . '</strong> ';
        $date = implode($date);


        $answerModificator = empty($comment['children'])
            ? ''
            : 'b-comment__answer_border';
        ?>
        <div class="b-comment" id="comment-<?php echo CHtml::encode($comment['id']); ?>">
            <div class="b-comment__content">
                <div class="b-comment__head">
                    <div class="b-comment__author">
                        <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($comment['sender_id']); ?>"
                           target="_blank"><?php echo CHtml::encode($comment['sender_name']); ?></a>
                    </div>
                    <div class="b-comment__date">[<?php echo $date; ?>]</div>
                </div><!-- .b-comment__head-->
                <div class="b-comment__body"><?php echo CHtml::encode($comment['text']); ?></div>
                <?php //if ($user_id && $comment['depth'] < 1): ?>
                <?php if ($user_id): ?>
                    <div class="b-comment__buttons" id="answer-buttons-<?php echo (int)$comment['id']; ?>">
                        <a href="#" class="b-comment__answer-close"
                           onclick="closeAnswerForm(<?php echo (int)$comment['id']; ?>);return false;">Закрыть</a>
                        <a href="#" class="b-comment__answer-open"
                           onclick="showAnswerForm(<?php echo (int)$comment['id']; ?>, this);return false;">Ответить</a>
                    </div><!-- .b-comment__buttons-->
				<?php else: ?>
					<div class="b-comment__buttons">
                        <a href="#" class="b-comment__answer-close"></a>
                        <a href="#" class="b-comment__answer-open"></a>
					</div><!-- .b-comment__buttons-->
                <?php endif; ?>
            </div><!-- .b-comment__content-->
			<!--<div style="height:15px;">
			</div>-->
            <div class="b-comment__answer <?php echo $answerModificator; ?>" id="answer-<?php echo (int)$comment['id']; ?>">
                <div class="b-comment__answer-form-block"></div>
                <?php if (!empty($comment['children'])): ?>
                    <div class="b-comment__answer-body">
                        <?php showCommentsRecursive($comment['children']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div><!-- .b-comment-->
    <?php
    endforeach;
}

Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.ui.datepicker-ru.js')
    ->registerScriptFile(Yii::app()->request->baseUrl . '/js/date-inputs-extending.js');

$this->setPageTitle($model->name . ' | '. $this->pageTitle);
$description = $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag(CHtml::encode($description), 'description');
}

if ($model->background && $model->background!='') {
    echo "<style>body { background: none repeat scroll 0 0 #".CHtml::encode($model->background)." !important; }</style>";
}

$this->breadcrumbs = array(
	$model->name,
);

$groupCity = City::model()->getCity($model->city_id);
$groupCountry = Country::model()->getCountryByCityID($model->city_id);

/*
$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
	array('label'=>'Update Group', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Group', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Group', 'url'=>array('admin')),
);
*/

$user_id = User::model()->getUserId();
$isgroupadmin = Group::model()->isGroupAdmin($model->id, $user_id);
$isModerator  = Group::model()->isGroupModerator($model->id, $user_id);
$confirmType = Member::model()->isMember($user_id, $model->id);
$ismember = $confirmType;

$hide = $model->hided && ! $ismember;
$mainAdmin = $model->getGroupAdmin($model->id);

$isSiteAdmin = Admin::model()->isSiteAdmin($user_id);
$isSiteModerator = Admin::model()->isSiteModerator($user_id);
$isSiteAdminOrModerator = $isSiteAdmin || $isSiteModerator;
if ($isSiteAdmin) {
    $premoderationWarning = 'Предпросмотр группы администратором.';
}
if ($isSiteModerator) {
    $premoderationWarning = 'Предпросмотр группы модератором.';
}

$groupLink = Yii::app()->createUrl('group/view') . '/' . (int)$model->id;

$showPastEvents = (int)Yii::app()->request->getParam('show_past_events', 0);
?>
<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 187px; margin: 0 7px 10px 0;">
        <?php if (!$model->approved
            && $isSiteAdminOrModerator
        ):?>
            <div class="b-premoderation-warning">
                <div class="b-premoderation-warning__text"><?php echo CHtml::encode($premoderationWarning); ?></div>
            </div><!-- .b-premoderation-warning-->
        <?php endif;?>
        <div class="sidebar-left">
            <div class="group-img">
                <?
                if ($model->picture != '' || $model->picture != NULL) {
                    $picture = $model->picture;
                } else {
                    $picture = 'no-pic.png';
                }
                ?>
                <img src="/images/group/mini_<?php echo CHtml::encode($picture); ?>" style="">
            </div>
            <? if ($isgroupadmin == 1) { ?>
                <a onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/group/update/'.urlencode($model->id); ?>'"
                   title="<?php echo Yii::t('var', 'Управление группой'); ?>" class="group-control align-center b-settings-button">
                    <?php echo Yii::t('var', 'Управление группой');?>
                </a>
            <? } ?>

        </div>
        <div class="sidebar-left">
            <?php if ( ! $hide ): ?>
            <div class="group-info">
                <ul>
                    <li><?php echo CHtml::encode($groupCity) . ', ' . CHtml::encode($groupCountry);?></li>
                    <li><?php echo Yii::t('var', 'Группа создана');?>:
                        <?php echo CHtml::encode(date('d.m.Y',strtotime($model->date_created)));?></li>
                    <li><?php echo Yii::t('var', 'Участники');?>:
                        <?php echo CHtml::encode(Member::model()->getGroupMembersCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Будущие события');?>:
                        <?php echo CHtml::encode(Group::model()->getFutureMeetsCount($model->id));?></li>
                    <li><?php echo Yii::t('var', 'Прошедшие события');?>:
                        <?php echo CHtml::encode(Group::model()->getPastMeetsCount($model->id));?></li>
                    <li><span class="calend-span"><?php echo Yii::t('var', 'Календарь группы');?>: </span><span class="calend-show"></span></li>
                    <script>
                        $(document).ready(function(){
                            var name = 'y';
                            var dec_url = decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
                            if (dec_url != null
                                || window.location.search.indexOf('calend=1') != -1
                            ) {
                                $('.calend').show();
                            }
                            $('.calend-show, .calend-close').on('click',function(){
                                $('.calend').slideToggle();
                            });
                        });
                    </script>
                </ul>
                <div class="key-words">
                    <?php echo Yii::t('var', 'Ключевые слова'); ?>:
                    <ul>
                        <?php $dataReader = GroupKeyword::model()->getGroupKeywords($model->id);
                        $i = 0;
						$keywords = '';
                        foreach($dataReader as $row) {
                            $keywordLink = Yii::app()->createUrl('site/index')
                                . '/?key=' . urlencode($row['name'])
                                . '&cat_id=' . urlencode($row['cat_id']);
                            echo '<li><a href="' . $keywordLink . '">'
                                . CHtml::encode($row['name']) . '</a></li>';
							if($row['name'])	
								$keywords .= $row['name'].', ';
							$i++;
                        } 
						if($keywords){
							$keywords = substr($keywords,0,-2);
							Yii::app()->clientScript->registerMetaTag(CHtml::encode($keywords).', '.Admin::getMainKeywords(), 'keywords');
						}
						?>
                    </ul>
                </div>
                <div class="organizers">
                    <?php echo Yii::t('var', 'Администратор') . ':';?>
                    <ul>
                        <li>
                            <div class="organizers__contact-button"
                                 id="adm-con-<?php echo CHtml::encode($mainAdmin['user_id']); ?>"></div>
                            <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($mainAdmin['id']); ?>"
                               class="admin-link-<?php echo CHtml::encode($mainAdmin['user_id']); ?>" target="_blank"><?php
                                echo CHtml::encode($mainAdmin['name']); ?></a>
                        </li>
                    </ul>
                    <?php
                    $cModerators = $model->getModerators($model->id);
                    if (!empty($cModerators)):
                        echo Yii::t('var', 'Модераторы') . ':';
                    ?>
                        <ul>
                            <?php foreach($cModerators as $cModerator): ?>
                                <li>
                                    <div class="organizers__contact-button"
                                         id="mod-con-<?php echo CHtml::encode($cModerator['user_id']); ?>"></div>
                                    <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($cModerator['user_id']); ?>"
                                       class="admin-link-<?php echo CHtml::encode($cModerator['user_id']); ?>" target="_blank"><?php
                                        echo CHtml::encode($cModerator['name']); ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <script>
            $('.organizers__contact-button').click(function(){
                var userId = $(this).attr('id').split('-')[2];
                var userName = $('.admin-link-' + userId).text();
                var href = '<?php echo Yii::app()->createUrl('user/view'); ?>/';
                $('.contact-admin__recipient')
                    .attr('href', href + userId)
                    .html(userName);
                $('.contact-admin__recipient-id').val(userId);
                $('#contact-admin').arcticmodal();
            });
        </script>
        <?php if (false): ?>
            <select id="сontact-us" style="display: none;">
                <option value="choose"><?php echo Yii::t('var', 'Свяжитесь с нами');?></option>
                <?
                $dataReader = Member::model()->getGroupAdmin($model->id);
                foreach ($dataReader as $val) {
                    if ($val['email']!='')
                    echo '<option value="'.CHtml::encode($val['email']).'">'.CHtml::encode($val['name']).'</option>';
                }
                $dataReader = Member::model()->getModeratorsList($model->id);
                foreach ($dataReader as $val) {
                    if ($val['email']!='')
                    echo '<option value="'.CHtml::encode($val['email']).'">'.CHtml::encode($val['name']).'</option>';
                }
                ?>
            </select>
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="sidebar-right" style="margin-top: 10px;">
            <h2 class="sidebar-right-title align-center sharp sharp-white">Случайные группы</h2>
            <div class="sidebar-right-content" style="text-align:center">
            <?
            $dataReader = Group::model()->getRandomGroup($model->id);
            if (count($dataReader)>0)
                foreach($dataReader as $row) {
                    $randomGroupPic = $row['picture'] != '' && $row['picture'] != NULL
                        ? '/images/group/mini_' . CHtml::encode($row['picture'])
                        : '/images/group/mini_no-pic.png';
                    echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']).'">'
                        . '<img style="width:187px;height:189px;" src="'.$randomGroupPic.'">'.CHtml::encode($row['name']).'</a>';
                }
            else 'Похожей группы не найдено';
            ?>
            </div>
        </div>
        <?php endif; ?>
    </aside>
    <section id="group-content" class="f-left">
        <div class="content-block-wrap">
            <?php
            $role = '';
            if ($isModerator) {
                $role = Yii::t('var', '(вы - модератор)');
            } elseif ($isgroupadmin) {
                $role = Yii::t('var', '(вы - администратор)');
            }
            ?>
            <h2 class="sharp align-center"><?php echo CHtml::encode($model->name . ' ' . $role); ?></h2>
            <div class="content-block">
                <p class="group-description"><?php echo CHtml::encode($model->description);?></p>
                <a href="#" title="<? echo Yii::t('var', 'читать далее');?>" class="f-right read-more"><? echo Yii::t('var', 'читать далее');?></a>
                <script>
                    $(document).ready(function(){
                        var text = $('p.group-description').html();
                        if (text.length < 100) $('a.read-more').remove();
                    });
                </script>
            </div>
        </div>
        <? if ($user_id!=0) { ?>
            <script>
                $(document).ready(function(){
                    if (!$('#myCarousel .carousel-inner').has('div'))
                    $('#myCarousel').parent().parent().parent().hide();
                });
            </script>
        <div class="content-block-wrap">
            <div class="content-block">
                <div class="slider align-center">
                    <h4 class="align-center"><? 
						$uid = User::model()->getUserServiceID($user_id);
						$uid = (int)$uid > 1 ? $uid : 0;
						$service = User::model()->getUserService($user_id);
						$serviceName = $service ? ucfirst($service) : 'Facebook/Vkontakte';
						//fb($uid,'$uid');
						//$uid = 0;
						//$service = '';
						echo 'Ваши друзья с Facebook/Vkontakte в данной группе';
					?></h4>
                    <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner social-carousel">

                            <?
							//проверять по сервису нельзя, потому что если пользователь залогинен в 
							//в двух соцсетях, то друзья будут распознаваться исходя из cookies
							//if ($uid > 0) {
                            if ($uid > 0 && $service =='facebook') {
                                try {
                                require 'facebook/src/facebook.php';

                                /* $facebook = new Facebook(array(
                                    'appId' => '303928756406490',
                                    'secret' => '8caedbb43189a170b926058cd3adb6bc',
                                    'cookie' => true,
                                )); */
                                $facebook = new Facebook(array(
                                    'appId' => '197822860407002',
                                    'secret' => '9d0b16a5dea1b22df0f25966f7a0aab1',
                                    'cookie' => true,
                                ));

                                $friends = $facebook->api('/'.$uid.'/friends');
								//fb($friends,'friends');
								//fb($uid,'uid');
                                $dataReader = Member::model()->getGroupMembersFacebook($model->id);
                                $members = array();
                                foreach ($dataReader as $val) {
                                    $members[] = $val['id'];
                                }
                                $res = array();
                                foreach ($friends['data'] as $val) {
                                    $res[] = $val['id'];
                                }
                                $result = array_intersect($members, $res);
                                if (count($result)>0) {
                                $ii = 0;
                                for ($i=0;$i<count($friends)-1;$i++) {
                                    if (in_array($friends['data'][$i]['id'],$members)) {
                                        $pic = $facebook->api('https://graph.facebook.com/'.$friends['data'][$i]['id'].'/picture');

                                        if ($ii==0) {
                                            echo '<div class="item active">';
                                        }
                                        $participantUrl = Yii::app()->createUrl('site/index') . '/user/'
                                            . urlencode(User::model()->getId($friends['data'][$i]['id'], 'facebook'));
                                        echo '<a class="group-participant" href="'.$participantUrl.'">'
                                            . '<img src="'.CHtml::encode($pic['id']).'" alt="participant" /><h5>'
                                            . CHtml::encode($friends['data'][$i]['name']).'</h5></a>';
                                        if ($ii%3 == 0) {
                                            echo '</div><div class="item">';
                                        }
                                        $ii++;
                                    }
                                }
                                echo '</div>';
                                }
                                } catch (FacebookApiException $e) {
                                    error_log($e);
                                }
                            }//endif

                            ?>
                        </div>
                        <!-- Carousel nav -->

                        <a class="carousel-control left prev" href="#myCarousel" data-slide="prev"></a>
                        <a class="carousel-control right next" href="#myCarousel" data-slide="next"></a>

                    </div>
                </div>
                <div id="photo"></div>
				<?php
					//if ($uid > 0 && $service =='vkontakte') {
					//проверять по сервису нельзя, потому что если пользователь залогинен в 
					//в двух соцсетях, то друзья будут распознаваться исходя из cookies
					//if ($uid > 0) {
						$cs = Yii::app()->clientScript;
						$cs->registerScriptFile("http://vkontakte.ru/js/api/openapi.js");
						
						$group_users_vk = array();
						$group_users = array();
						$dataReader = Member::model()->getGroupMembersVK($model->id);
						foreach ($dataReader as $val) {
							$group_users_vk[] = CHtml::encode($val['serv_id']);
							$group_users[] = CHtml::encode($val['id']);

						} 
						$group_users_vk_str = implode(',',$group_users_vk);
						$group_users_str = implode(',',$group_users);
						$cs->registerScript('vkontakteOpenApi', "
							 VK.init({
								//apiId: 3305436
								apiId: 4062059
							});

							function authInfo(response) {
								var i,id,ind;
								var uids = new  Array();
								var users = new  Array();


								var group_users_vk = [
									".$group_users_vk_str."
								];
								var group_users = [
									".$group_users_str."
								];

								if (response.session) {
									//VK.api('friends.get',{uid:5976499,fields:\"uid, first_name, last_name, nickname, sex, photo_medium\"},function(data) {
									VK.api('friends.get',{uid:".$uid.",fields:\"uid, first_name, last_name, nickname, sex, photo_medium\"},function(data) {
										//console.log(data,'data');
										//console.log(group_users_vk,'group_users_vk');
										if (data.response) {
											for (i=0;i<data.response.length;i++) {
												ind = $.inArray(data.response[i].uid, group_users_vk);
												if (ind != -1) {
													//console.log(data.response[i].uid,'9999');
													$(\".carousel-inner\").append('<div class=\"item active\"><a class=\"group-participant\" href=\"".Yii::app()->createUrl('site/index')."/user/'+parseInt(group_users[ind])+'\"><img src=\"'+data.response[i].photo_medium+'\" alt=\"participant\" /><h5>'+data.response[i].first_name+' '+data.response[i].last_name+'</h5></a></div>');
												}
											}
										}
									});
								}
							}

							VK.Auth.getLoginStatus(authInfo);
						", CClientScript::POS_END);
					//}endif;
				?>
            </div>
        </div>
        <? } ?>
        <?php if ( ! $hide ): ?>
        <div class="content-block-wrap">
            <div class="content-block">
                <a id="to-wall" style="cursor:pointer;"><? echo Yii::t('var', 'Стена группы');?></a>
                <script>
                    $(document).ready(function(){
                        $('#to-wall').on('click',function(){
                            $('html, body').animate({
                                scrollTop: $(".wall").offset().top
                            }, 1000);
                        });
                    });
                </script>
            </div>
        </div>
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="content-block-wrap calend" id="group-calendar">
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Календарь');?></h2>
            <div class="calend-close">[<?php echo Yii::t('var', 'закрыть');?>]</div>
            <div class="content-block">
            <?php
                Yii::import('application.extensions.calendar.classes.calendar', true);

                $month = isset($_GET['m']) ? $_GET['m'] : NULL;
                $year  = isset($_GET['y']) ? $_GET['y'] : NULL;

                $calendar = Calendar::factory($month, $year);

                $events = array();

                $dataReader = Group::model()->getGroupMeetsID($model->id);
                foreach ($dataReader as $val) {
                    $events[] = $calendar->event()
                        ->condition('timestamp', strtotime($val['date_start']))
                        ->title('Hello All')
                        ->output('<a href="'.Yii::app()->createUrl('site/index').'/group/meet/'.urlencode($model->id).'/?mid='.urlencode($val['id']).'">'.CHtml::encode(date('H:i',strtotime($val['date_start']))).'<br>'.CHtml::encode($val['name']).'</a>');
                }

                $calendar->standard('today')->standard('prev-next');

                foreach ($events as $val) {
                    $calendar->attach($val);
                }
            ?>
                <table class="calendar">
                    <thead>
                    <tr class="navigation">
                        <th class="prev-month"><a href="<?php echo $calendar->prev_month_url(); ?>"><?php echo $calendar->prev_month(); ?></a></th>
                        <th colspan="5" class="current-month"><?php echo $calendar->month(); ?> <?php echo $calendar->year; ?></th>
                        <th class="next-month"><a href="<?php echo $calendar->next_month_url(); ?>"><?php echo $calendar->next_month(); ?></a></th>
                    </tr>
                    <tr class="weekdays">
                        <?php foreach ($calendar->days() as $day): ?>
                            <th><?php echo $day ?></th>
                        <?php endforeach ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($calendar->weeks() as $week): ?>
                        <tr>
                            <?php foreach ($week as $day): ?>
                                <?php
                                list($number, $current, $data) = $day;

                                $classes = array();
                                $output  = '';

                                if (is_array($data))
                                {
                                    $classes = $data['classes'];
                                    $title   = $data['title'];
                                    $output  = empty($data['output']) ? '' : '<ul class="output"><li>'.implode('</li><li>', $data['output']).'</li></ul>';
                                }
                                ?>
                                <td class="day <?php echo implode(' ', $classes) ?>">
                                    <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                                    <div class="day-content">
                                        <?php echo $output ?>
                                    </div>
                                </td>
                            <?php endforeach ?>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <style>
                    .calendar {width:100%; border-collapse:collapse;}
                    .calendar tr.navigation th {padding-bottom:20px;}
                    .calendar th.prev-month {text-align:left;}
                    .calendar th.current-month {text-align:center; font-size:1.5em;}
                    .calendar th.next-month {text-align:right;}
                    .calendar tr.weekdays th {text-align:left;}
                    .calendar td {width:14%; height:73px; vertical-align:top; border:1px solid #CCC;}
                    .calendar td.today {background:#FFD;}
                    .calendar td.prev-next {background:#EEE;}
                    .calendar td.prev-next span.date {color:#9C9C9C;}
                    .calendar td.holiday {background:#DDFFDE;}
                    .calendar span.date {display:block; padding:4px; line-height:12px; background:#EEE;}
                    .calendar div.day-content {}
                    .calendar ul.output {margin:0; padding:0 4px; list-style:none;}
                    .calendar ul.output li {margin:0; padding:5px 0; line-height:1em; border-bottom:1px solid #CCC;}
                    .calendar ul.output li:last-child {border:0;}

                        /* Small Calendar */
                    .calendar.small {width:auto; border-collapse:separate;}
                    .calendar.small tr.navigation th {padding-bottom:5px;}
                    .calendar.small tr.navigation th a {font-size:1.5em;}
                    .calendar.small th.current-month {font-size:1em;}
                    .calendar.small tr.weekdays th {text-align:center;}
                    .calendar.small td {width:auto; height:auto; padding:4px 8px; text-align:center; border:0; background:#EEE;}
                    .calendar.small span.date {display:inline; padding:0; background:none;}
                </style>
            </div>
        </div>
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="content-block-wrap relative">
            <?php if ($showPastEvents): ?>
                <a href="<?php echo CHtml::encode($groupLink); ?>"
                   class="show-past-events">Скрыть прошедшие</a>
            <?php else: ?>
                <a href="<?php echo CHtml::encode($groupLink); ?>?show_past_events=1"
                   class="show-past-events">Показать прошедшие</a>
            <?php endif; ?>
            <h2 class="sharp align-center"><? echo Yii::t('var', 'Встречи');?></h2>
            <?php if ( $isgroupadmin == 1 ): ?>
                <button type="button" name="add-event" class="add-event" onclick="$('#meet-create').arcticmodal();">
                    <span><? echo Yii::t('var', 'Создать событие');?></span>
                </button>
            <?php elseif( $user_id != 0 && $ismember ): ?>
                <button type="button" name="add-event" class="add-event" onclick="$('#meet-proposal').arcticmodal();">
                    <span><? echo Yii::t('var', 'Предложить событие');?></span>
                </button>
            <?php endif; ?>

            <?
            $criteria = new CDbCriteria(array(
                'condition' => '`type`="meetup" AND `parent`='.(int)$model->id,
                'order' => 'date_start DESC',
            ));
            if (!$showPastEvents) {
                $criteria->addCondition('date_start > "' . date('Y-m-d H:i:s') . '"');
            }
            $dataProvider=new CActiveDataProvider('Group',array('criteria'=>$criteria));
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'application.views.group._meets',
                'template'=>'{items}',
                'itemsCssClass'=>'clearfix port-det port-thumb'
            ));
            ?>

        </div>
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="content-block-wrap wall" id="comments">
            <h2 class="sharp align-center"><?php echo Yii::t('var', 'Стена');?></h2>
            <div class="content-block">
                <?php
                $comments = Group::model()->getFormatedComments($model->id);
                if (count($comments)):
                ?>
                    <label for="comment" class="f-left"><h3 class="sharp"><?php echo Yii::t('var', 'Комментарии');?>:</h3></label>
                <?php else: ?>
                    <label for="comment" class="f-left">
                        <h3 class="sharp">
                            <?php echo Yii::t('var', 'Здесь пока никто ничего не писал.'); ?>
                            <br />
                            <?php echo Yii::t('var', 'Вы можете сделать это первым:'); ?>
                        </h3>
                    </label>
                <?php endif; ?>
                <div class="clear"></div>
                <?php

                showCommentsRecursive($comments);

                if ($user_id):
                ?>
                <form id="wall-form" action="<?php echo Yii::app()->request->baseUrl; ?>/group/sentComment"
                      name="sent-record" method="post">
                    <textarea class="b-comment__textarea f-left" name="text"></textarea><br>
                    <input class="f-right" type="image" src="/images/send-comment.png" />
                    <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>" />
                    <input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />
                    <input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />
                </form>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </section>
    <aside id="#SideRight" class="f-right" style="width: 187px; margin: 0 0 10px 7px;">
        <div class="sidebar-right" style="background: none;">
            <?php
            if ($user_id!=0) { ?>
                <?php if ($ismember == null) { ?>
                    <button onclick="$('#exampleModal').arcticmodal();" type="button" name="enter-group"
                            class="align-center white enter-group"><span><? echo Yii::t('var', 'Вступить в группу');?></span></button>
                <?php } elseif ($ismember == 1 && !$isgroupadmin) { ?>
                    <button onclick="leaveGroup(<? echo User::model()->getUserId().', '.(int)$model->id;?>)" type="button" name="enter-group"
                            class="align-center white enter-group"><span><? echo Yii::t('var', 'Покинуть группу');?></span></button>
                <?php } elseif ($ismember && $isModerator) { ?>
                    <a href="<?php echo Yii::app()->createUrl('group/moderatorretire') . '?group_id=' . urlencode($model->id); ?>"
                       class="align-center white enter-group retire"><span><? echo Yii::t('var', 'Сложить полномочия');?></span></a>
                <?php } elseif ($ismember == 1 && $isgroupadmin == 1) { ?>
                    <a href="<?php echo Yii::app()->createUrl('group/deletewarning') . '/' . urlencode($model->id); ?>"
                       class="align-center white enter-group"><span><? echo Yii::t('var', 'Удалить группу');?></span></a>
                <?php } elseif ($confirmType == 2) { ?>
                    <div class="b-group-ban-message">Вы в чёрном <br />списке группы.</div>
                <?php } elseif ($confirmType == 0) { ?>
                    <div class="b-group-enter-waiting">Заявка на вступление находится на расмотрении администратора группы.</div>
                <? } ?>
            <? } else { ?>
                <button type="button" name="enter-group"
                        class="align-center white enter-group enter-error"><span><?php echo Yii::t('var', 'Вступить в группу');?></span></button>
                <div id="enter-error" class="b-enter-error-message">
                    Чтобы вступить в группу нужно сначала <a href="<?php echo Yii::app()->createUrl('site/registration')?>">зарегистрироваться</a> или <a href="<?php echo Yii::app()->createUrl('site/login')?>">войти</a> в систему
                </div>
            <? } ?>
            <script>
                $('.enter-error').mouseover(function(){
                    $('#enter-error').show();
                }).mouseout(function(){
                    $('#enter-error').hide();
                });
                $('#enter-error').mouseover(function(){
                    $(this).css({'display':'block'});
                }).mouseout(function(){
                    $(this).css({'display':'none'});
                });
            </script>
            <?php if ($ismember):  ?>
                <button type="button" name="invite" class="align-center white enter-group invite" onclick="$('#mailto').arcticmodal();">
                    <span><? echo Yii::t('var', 'Пригласить друга');?></span>
                </button>
            <?php endif; ?>
            <?php if ($hide): ?>
                <div class="sidebar-right-content" style="background: #fff;">
                    Информация о группе и список встреч доступны только участникам группы.<br />
                    Подайте заявку, чтоб получить доступ к этой информации.
                </div>
            <?php endif; ?>
        </div>
        <?php if ($isgroupadmin != 1 && $user_id!=0): ?>
            <div class="sidebar-right">
                <a mid="<?php echo CHtml::encode($model->id); ?>" onclick="$('#complaint').arcticmodal();"
                   title="<?php echo Yii::t('var', 'пожаловаться на группу'); ?>"
                   class="complain align-center b-complain-button">
                    <?php echo Yii::t('var', 'пожаловаться на группу');?>
                </a>
            </div><!-- .sidebar-right-->
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp"><? echo Yii::t('var', 'Оплаченные группы');?></h2>
            <div class="sidebar-right-content">
                <?
                $dataReader = Group::model()->getRandomPaidGroup($model->id);
                if (count($dataReader)>0)
                    foreach($dataReader as $row) {
                        $randomPaidGroup = $row['picture'] != '' && $row['picture'] != NULL
                            ? CHtml::encode($row['picture'])
                            : 'images/group/mini_no-pic.png';
                        echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($row['id']).'">'
                            . '<img style="width: 100%;" src="'.CHtml::encode($randomPaidGroup).'">'
                            . CHtml::encode($row['name']).'</a>';
                    }
                else echo Yii::t('var', 'Проплаченные группы не найдены');
                ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if ( ! $hide ): ?>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Участники');?></h2>
            <div class="sidebar-right-content sidebar-right-content-participants">
                <div class="group-participants">
                    <?
                    $criteria = new CDbCriteria;
                    $criteria->condition = 'group_id = '.(int)$model->id.' AND confirm=1';

                    $dataProvider=new CActiveDataProvider('Member',array('criteria'=>$criteria));
                    $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'member',
                        'template'=>'{items}'
                    ));
                    ?>
                </div>
                <a href="<?php echo Yii::app()->createUrl('site/index').'/group/members/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><?php echo Yii::t('var', 'просмотреть всех');?></a>
            </div>
        </div>
        <?php endif; ?>
        <?php
        $dataReader = Photoalbum::model()->getRandomPhoto($model->id);
        if ($ismember):
        ?>
        <div class="sidebar-right">
            <h2 class="sidebar-right-title align-center sharp"><?php echo Yii::t('var', 'Фотографии группы');?></h2>
            <?php if($dataReader): ?>
                <div class="sidebar-right-content sidebar-right-content-photo-group">
                    <img src="/images/album/<?php echo CHtml::encode($dataReader['album_id']); ?>/<?php echo CHtml::encode($dataReader['link']); ?>" style="width:100%;" alt="<?=$dataReader['link'];?>">
                </div>
                <a href="<? echo Yii::app()->createUrl('site/index').'/group/photos/'.urlencode($model->id); ?>" title="просмотреть все фотографии группы" class="show-all-participants align-center"><?php echo Yii::t('var', 'просмотреть все фотографии группы');?></a>
                <?php // style="width: 160px; height: auto;" ?>
            <?php else: ?>
                <div class="sidebar-right-content sidebar-right-content-photo-group">
                    <?php echo Yii::t('var', 'Фотографий пока нет'); ?>
					<a href="<?php echo Yii::app()->createUrl('site/index').'/group/photos/'.urlencode($model->id); ?>" title="загрузить" class="show-all-participants align-center"><? echo Yii::t('var', 'загрузить');?></a>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </aside>
</div>





<script>
    function joinGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/joinGroup');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }
    function leaveGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/leaveGroup');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                window.location.reload();
            }
        });
    }

    function delGroup(user_id, group_id){
        $.ajax({
            type: 'POST',
            url: '<? echo Yii::app()->createUrl('group/delGroup');?>',
            data: { id: user_id, group_id: group_id,YII_CSRF_TOKEN: '<? echo Yii::app()->request->csrfToken;?>' },
            success: function(data){
                //window.location.reload();
                window.location.href = '/';
            }
        });
    }
</script>
<!--  <p>Ваши друзья из социальных сетей</p>
        --><?/*
        include_once 'facebook/facebook.php';
        $facebook = new Facebook(array(
            'appId' => '591018247595529',
            'secret' => '96f74c40b923be3eb8536699065f89d7',
            'cookie' => true
        ));

        $access_token = $facebook->getAccessToken();
        $friends = $facebook->api('/me/friends?token='.$access_token);*/

/*$session = $facebook->getSession();
if ($session) {
    $uid = $facebook->getUser();
    $me = $facebook->api('/me');
}
print_r($me);*/
?>


<div style="display: none;">
    <div class="box-modal" id="exampleModal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="addit-fields-form">
            <table>
            <?
            $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
            foreach($dataReader as $row) {
                echo "<tr><td>".CHtml::encode($row['name'])."</td>'
                . '<td><input type='text' value='' name='fields[".CHtml::encode($row['id'])."]'></td></tr>";
            }
            ?>
                <tr><td></td><td><input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

            $("#addit-fields-form").validate({
            rules: {
                <?
                    $i = 0;
                    $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                    foreach($dataReader as $row) {
                        if ($row['required']==1) {
                            echo (($i>0)?',':'').CHtml::encode($row['id']).': "required"';
                            $i++;
                        }
                     }
                ?>
            },
            messages: {
                <?
                        $i = 0;
                        $dataReader = AdditionalFields::model()->getGroupAdditionalFields($model->id);
                        foreach($dataReader as $row) {
                            if ($row['required']==1) {
                                echo (($i>0)?',':'').CHtml::encode($row['id']).': "Обязательное поле"';
                                $i++;
                            }
                         }
                    ?>
            },
                submitHandler: function(form) {
                    $.ajax({
                        type: 'GET',
                        url: '<? echo Yii::app()->createUrl('member/sendRequest'); ?>',
                        data: $('#addit-fields-form').serialize()+'&group_id=<?php echo (int)$model->id; ?>&user_id=<?php echo (int)$user_id; ?>',
                        success: function(data){
                            window.location.reload();
                        }
                    });
                }
            });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr>
                    <td><br /><?php echo Yii::t('var', 'Введите текст жалобы');?>:<br /><br /></td>
                </tr>
                <tr>
                    <td>
                        <textarea cols="72" rows="9" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>">
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mid" value="<?php echo (int)$model->id; ?>">
            <input type="hidden" name="text_id" value="group">
        </form>

        <script>
            $(document).ready(function() {
                $('.complain-event').on('click',function(){
                    $('input[name=mid]').val($(this).attr('mid'));
                });

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
						$.arcticmodal('close');
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize()+'&user_id=<?php echo (int)$user_id; ?>'+"&YII_CSRF_TOKEN=<?php echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите имя друга');?></td><td><input type="text" name="name" value=""></td></tr>
                <tr><td><? echo Yii::t('var', 'Введите email друга');?></td><td><input type="text" name="email" value=""></td></tr>
                <tr><td style="vertical-align: middle;"><?php echo Yii::t('var', 'Введите комментарий');?></td><td><textarea type="text" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $(document).ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        comment: "required"
                    },
                    messages: {
                        name: "<? echo Yii::t('var', 'Обязательное поле');?>",
                        email: "<? echo Yii::t('var', 'Введите правильный email');?>",
                        comment: "<? echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="meet-proposal">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="meet-proposal-form">
            <table>
                <tr>
                    <td><? echo Yii::t('var', 'Введите название события');?>:</td>
                    <td><input type="text" name="name" value="" /></td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Введите место');?>:</td>
                    <td><input type="text" name="place" value="" /></td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Количество мест');?>:</td>
                    <td><input type="text" name="seats" value="" /></td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Выберите дату');?></td>
                    <td>
                        <input type="text" name="date" placeholder="<? echo Yii::t('var', 'Нажмите, чтобы выбрать');?>"
                               value="" class="b-jui-datepicker" />
                    </td>
                </tr>
                <tr>
                    <td><? echo Yii::t('var', 'Выберите время');?></td>
                    <td>
                        <span>Час:</span>
                        <select name="time_hour" class="b-meet-time-hour">
                            <option value="0">0</option>
                        </select>
                        <span>Минуты:</span>
                        <select name="time_minute" class="b-meet-time-minute">
                            <option value="0">0</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <span>Любое время:</span>
                        <input name="any_time" class="b-any-time-input" type="checkbox" value="1" style="width: auto;"/>
                    </td>
                </tr>
                <tr><td><? echo Yii::t('var', 'Введите комментарий');?></td><td><textarea type="comment" name="comment" value=""></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td></tr>
            </table>
            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
        </form>
        <script>
            $().ready(function() {
                $('.b-any-time-input').change(function(){
                    var timeFields = $('.b-meet-time-hour, .b-meet-time-minute');
                    if ( $(this).prop('checked') ) {
                        timeFields.attr('disabled', 'disabled');
                    } else {
                        timeFields.removeAttr('disabled');
                    }
                });
                $("#meet-proposal-form").validate({
                    rules: {
                        name: "required",
                        place: "required",
                        seats: {
                            required: true,
                            digits: true,
                            min: 1,
                            max: 100
                        },
                        date: "required",
                        comment: "required"
                    },
                    messages: {
                        name   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        place  : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        seats  : "<?php echo Yii::t('var', 'Обязательное поле. Должно быть в пределах от 1 до 100') ?>",
                        date   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                        comment: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('group/meetproposal'); ?>',
                            data: $('#meet-proposal-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
    <?php if ( $isgroupadmin == 1 ): ?>
        <div class="box-modal" id="meet-create">
            <div class="box-modal_close arcticmodal-close"></div>
            <form action="" method="get" id="meet-create-form">
                <table>
                    <tr>
                        <td><? echo Yii::t('var', 'Введите название события');?>:</td>
                        <td><input type="text" name="name" value=""></td>
                    </tr>
                    <tr>
                        <td><? echo Yii::t('var', 'Введите место');?>:</td>
                        <td><input type="text" name="address" value=""></td>
                    </tr>
                    <tr>
                        <td><? echo Yii::t('var', 'Количество мест');?>:</td>
                        <td><input type="text" name="seats" value=""></td>
                    </tr>
                    <tr>
                        <td><? echo Yii::t('var', 'Выберите дату');?></td>
                        <td>
                            <input type="text" name="date" placeholder="<?php echo Yii::t('var', 'Нажмите, чтобы выбрать');?>"
                                   value="" class="b-jui-datepicker">
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Выберите время');?></td>
                        <td>
                            <span>Час:</span>
                            <select name="time_hour" class="b-meet-time-hour">
                                <option value="0">0</option>
                            </select>
                            <span>Минуты:</span>
                            <select name="time_minute" class="b-meet-time-minute">
                                <option value="0">0</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <span>Любое время:</span>
                            <input name="any_time" class="b-any-time-input" type="checkbox" value="1" style="width: auto;"/>
                        </td>
                    </tr>
                    <tr>
                        <td><? echo Yii::t('var', 'Введите комментарий');?></td>
                        <td><textarea name="description"></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td>
                    </tr>
                </table>
                <input type="hidden" name="city" value="<?php echo CHtml::encode($model->city_id); ?>">
                <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
                <input type="hidden" name="parent" value="<?php echo CHtml::encode($model->id); ?>">
            </form>
            <script>
                window.onload = function() {
                    $("#meet-create-form").validate({
                        rules: {
                            name   : "required",
                            address: "required",
                            seats  : {
                                required: true,
                                digits: true,
                                min: 1
                            },
                            date   : "required",
                            description: "required"
                        },
                        messages: {
                            name   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            address: "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            seats  : "<?php echo Yii::t('var', 'Обязательное поле. Должно быть в пределах от 1 до 100') ?>",
                            date   : "<?php echo Yii::t('var', 'Обязательное поле');?>",
                            description: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                        },
                        submitHandler: function(form) {
                            $.ajax({
                                type: 'GET',
                                url: '<?php echo Yii::app()->createUrl('group/creatingmeet'); ?>',
                                data: $('#meet-create-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                                success: function(data){
                                    window.location.reload();
                                }
                            });
                        }
                    });
                };
            </script>
        </div><!-- #meet-create-->
    <?php endif; ?>
</div>
<div style="display: none;">
    <?php if ( ! $hide ): ?>
        <div class="box-modal" id="contact-admin">
            <div class="box-modal_close arcticmodal-close"></div>
            <form action="" method="get" id="contact-admin-form">
                <table>
                    <tr>
                        <td>
                            <span>Получатель:</span>
                            <strong><a href="#" target="_blank"
                                class="contact-admin__recipient"><?php echo CHtml::encode($mainAdmin['name']); ?></a></strong>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Введите ваше сообщение');?></td>
                        <td><textarea type="comment" name="comment" value=""></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input class="submit" type="submit" value="<? echo Yii::t('var', 'Отправить');?>"></td>
                    </tr>
                </table>
                <input value="<?php echo CHtml::encode($mainAdmin['user_id']); ?>" name="admin_id"
                       type="hidden" class="contact-admin__recipient-id" />
                <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
            </form>
            <script>
                $().ready(function() {

                    $("#contact-admin-form").validate({
                        rules: {
                            place: "required",
                            date: "required",
                            comment: "required"
                        },
                        messages: {
                            place: "<? echo Yii::t('var', 'Обязательное поле');?>",
                            date: "<? echo Yii::t('var', 'Обязательное поле');?>",
                            comment: "<? echo Yii::t('var', 'Обязательное поле');?>"
                        },
                        submitHandler: function(form) {
                            $.ajax({
                                type: 'GET',
                                url: '<? echo Yii::app()->createUrl('group/contactadmin'); ?>',
                                data: $('#contact-admin-form').serialize()+"&YII_CSRF_TOKEN=<? echo Yii::app()->request->csrfToken;?>",
                                success: function(data){
                                    window.location.reload();
                                }
                            });
                        }
                    });
                });
            </script>
        </div>
    <?php endif; ?>
</div>
<?php if (0): ?>
<div style="display: none;">
    <div class="box-modal" id="del-group">
        <table>
            <tr><td colspan="2" style="text-align: center;">Вы уверены, что хотите удалить группу?</td></tr>
            <tr>
                <td>
                    <input type="submit" name="del-group-button"
                           class="submit del-group-button" value="<?php echo Yii::t('var', 'Удалить группу');?>" />
                </td>
                <td>
                    <input type="submit" name="del-group-button-cancel" style="float: left;"
                           class="submit del-group-button-cancel" value="<?php echo Yii::t('var', 'Отменить');?>">
                </td>
            </tr>
        </table>
    </div>
    <script>
        $().ready(function(){
            $('.del-group-button').click(function(){
                if (confirm('Вы уверены, что хотите удалить группу? Это действие нельзя отменить.')) {
                    delGroup(<? echo User::model()->getUserId().', '.(int)$model->id;?>);
                }
                return false;
            });
            $('.del-group-button-cancel').click(function(){
                window.location.href = '<?php echo Yii::app()->createUrl('group/view') . '/' . (int)$model->id; ?>';
                return false;
            });
        });
    </script>
</div>
<?php endif; ?>
<style>
    .box-modal {
        width:540px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
    #sidebar {
        display: none;
    }
    .span-5 {
        width:0px
    }
    .span-19 {
        width: 950px;
    }
</style>
<script>
    function closeAnswerForm(parentId)
    {
        $('#answer-' + parentId + ' > .b-comment__answer-form-block').slideUp('fast').html('');
        $('#answer-buttons-' + parentId).removeClass('active');

        return false;
    }
    function closeParentForm(){
		$('.prependBlock').remove();
	}
    function showAnswerForm(parentId, that)
    {
        /* parentId = parseInt(parentId);
        $('.b-comment__answer-form-block').slideUp('fast').html('');
        $('.b-comment__buttons').removeClass('active');

        var formHtml = '<form id="wall-form-' + parentId + '" action="<?php echo Yii::app()->request->baseUrl; ?>/group/sentComment"'
            + ' name="sent-record-' + parentId + '" method="post" class="">'
            + '<textarea class="b-comment__answer-textarea f-left" name="text"></textarea><br>'
            + '<input class="b-comment__answer-submit f-right" type="image" src="/images/send-comment.png" />'
            + '<input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>" />'
            + '<input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />'
            + '<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />'
            + '<input type="hidden" name="parent_id" value="' + parentId + '" /><div class="clear"></div></form>';

        $('#answer-' + parentId + ' > .b-comment__answer-form-block').html(formHtml).slideDown('fast');
        $('#answer-buttons-' + parentId).toggleClass('active');

        return false; */
		$('.prependBlock').remove();
		f = $(that).closest('.b-comment__content').find('.b-comment__author a').text();
		console.log($(that).parent().parent(),'888');
		$('.b-comment__textarea.f-left').focus();
		$('#wall-form').prepend("<div class='prependBlock' style='color:grey'>Ответ для "+f+" <span class='closeParentForm' style='cursor: pointer;' title='Закрыть' onclick='closeParentForm()'>(<b style='color:#4A9DD9;'>х</b>)<span></div><input class='prependBlock' type='hidden' name='parent_id' value='"+parentId+"' />");
    }
</script>
<script>
    $(document).ready(function(){
        $('#сontact-us').on('change',function(){
            $('#contact-admin').arcticmodal();
        });

        var height = $('.group-img img').height();
        var width = $('.group-img img').width();
        var left,top;
        if (width > height) {
            $('.group-img img').css({'height':'192px'});
            width = $('.group-img img').width();
            left = (width - 185.5)/2;
            $('.group-img img').css({'left':'-'+left+'px'});
        }
        else {
            $('.group-img img').css({'width':'185.5px'});
            height = $('.group-img img').height();
            top = (height - 192)/2;
            $('.group-img img').css({'top':'-'+top+'px'});
        }


    });
</script>
