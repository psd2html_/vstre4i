<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Category', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),
	array('label'=>'Update Category', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Category', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>(int)$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Category', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('var','View Category #')." ".CHtml::encode($model->id); ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
            'name' => 'name',
            'label' => Yii::t('var','name')
        ),
        array(
            'name' => 'ua',
            'label' => Yii::t('var','ua'),
        ),
        array(
            'name' => 'kz',
            'label' => Yii::t('var','kz'),
        ),
        array(
            'name' => 'be',
            'label' => Yii::t('var','be')
        ),
	),
)); ?>
