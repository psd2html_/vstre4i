<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','name')); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($trModels['ua'],Yii::t('var','ua')); ?>
        <?php echo $form->textField($trModels['ua'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[ua]')); ?>
        <?php echo $form->error($trModels['ua'],'value'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($trModels['kz'],Yii::t('var','kz')); ?>
        <?php echo $form->textField($trModels['kz'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[kz]')); ?>
        <?php echo $form->error($trModels['kz'],'value'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($trModels['be'],Yii::t('var','be')); ?>
        <?php echo $form->textField($trModels['be'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[be]')); ?>
        <?php echo $form->error($trModels['be'],'value'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('var','Create') : Yii::t('Сохранить','Save')); ?>
	</div>

<?php $this->endWidget(); ?>


</div><!-- form -->