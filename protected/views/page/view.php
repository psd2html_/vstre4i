<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	CHtml::encode($model->name),
);
$this->setPageTitle($model->name.' | '.$this->pagetitle);
$description = $model->description;
if($description){
	Yii::app()->clientScript->registerMetaTag($description, 'description');
}else{
	Yii::app()->clientScript->registerMetaTag(Admin::getMainDescription(), 'description');
}
$keywords = $model->keywords;
if($keywords){
	Yii::app()->clientScript->registerMetaTag($model->keywords.', '.Admin::getMainKeywords(), 'keywords');
}else{
	Yii::app()->clientScript->registerMetaTag(Admin::getMainKeywords(), 'keywords');
}
?>
<div id="blockOfPage">
	<h1><?= CHtml::encode($model->name); ?></h1>
	<br />
	<div id="tetxOfPages"><?= $model->text; ?></div>
</div>
