<?php
/* @var $this PageController */
/* @var $model Page */
$this->breadcrumbs=array(
	'Страницы'=>array('admin'),
	'Изменить',
);
?>

<h1>Изменить страницу</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
