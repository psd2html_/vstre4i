<?php
/* @var $this PageController */
/* @var $model Page */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>100, 'placeholder'=>'(по умолчанию будет заголовок)')); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'keywords'); ?>
		<?php echo $form->textField($model,'keywords',array('size'=>60,'maxlength'=>255, 'placeholder'=>'(через запятую)')); ?>
		<?php echo $form->error($model,'keywords'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Краткое описание'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>3, 'cols'=>58, 'maxlength'=>255, 'placeholder'=>'(по умолчанию первые 255 символов)')); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div id="replase1" class="row texteditor">
		<?php //echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>20, 'cols'=>60)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('var','Create') : Yii::t('var','Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
	CKEDITOR.config.width = '615px'; 
	CKEDITOR.config.height = '400px'; 
</script>
