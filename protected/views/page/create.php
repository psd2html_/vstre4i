<?php
/* @var $this PageController */
/* @var $model Page */

$this->breadcrumbs=array(
	'Страницы'=>array('admin'),
	'Создать',
);

?>

<h1><?php echo Yii::t('var','Create Page')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
