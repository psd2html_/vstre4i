<?php
/* @var $this UserController */
/* @var $model User */



//$this->setPageTitle($model->name . ' - '. Yii::app()->name);
$this->setPageTitle('Пользователь '.$model->name);

$this->breadcrumbs=array(
	$model->name,
);

$avatar = $model->avatar;
if ($model->sex == 1) $sex = Yii::t('var', 'Муж.');
if ($model->sex == 2) $sex = Yii::t('var', 'Жен.');


$email = User::model()->getUserEmail((int)$model->id);
?>

<div class="center">
    <aside id="#SideLeft" class="f-left" style="width: 237px; margin: 0 45px 10px 0;">
        <div class="avatar white-block">
            <img src="/images/user/mini_<?php if ($avatar && $avatar != '') echo CHtml::encode($avatar); else echo 'no-pic.png';?>" alt="<?=$model->name?>">
        </div>

        <!-- Только если не личная -->
        <?php if ($user_id != (int)$model->id) { ?>

            <?php if ($email != ''): ?>
                <input onclick="$('#mailto').arcticmodal();" type="image" src="/images/send-message.png" name="send-message" class="send-msg"/>
            <?php endif; ?>
			
            <?php if ($user_id): ?>
            <div class="sidebar-right">
                <a mid="<?php echo CHtml::encode($model->id); ?>" onclick="$('#complaint').arcticmodal();" title="пожаловаться на пользователя" class="complain align-center b-complain-button">
                    <?php echo 'Пожаловаться на пользователя';?>
                </a>
            </div>
            <?php endif; ?>

            <div class="sidebar-right">
                <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Участвует в');?>:</h2>
                <div class="sidebar-right-content sidebar-left-content-participants">
                    <div class="group-participants">
                        <?
                        $dataReader = Group::model()->getUserGroupList($model->id);
                        foreach ($dataReader as $value) {
                            if ($value['picture']) 
								$picture = '/images/group/mini_'.$value['picture'];
							else 
								$picture = '/images/group/mini_no-pic.png';
                            echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($value['id']).'" title="'.CHtml::encode($value['name']).'">
                                <img src="'.CHtml::encode($picture).'" alt="'.CHtml::encode($value['name']).'" style="width: 60px; height: 50px;"/>
                            </a>';
                        }
                        ?>
                    </div>

                    <a href="<? echo Yii::app()->createUrl('site/index').'/user/groups/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><? echo Yii::t('var', 'просмотреть всех');?></a>
                </div>
            </div>

            <div class="sidebar-right">
                <h2 class="sidebar-right-title align-center sharp" style="margin-bottom: 0;"><? echo Yii::t('var', 'Создатель групп');?>:</h2>
                <div class="sidebar-right-content sidebar-left-content-participants">
                    <div class="group-participants">
                        <?php
                        $dataReader = Group::model()->getUserAdminGroupList($model->id);
                        foreach ($dataReader as $value) {
                            if ($value['picture']) 
								$picture = '/images/group/mini_'.$value['picture'];
							else 
								$picture = '/images/group/mini_no-pic.png';
                            echo '<a href="'.Yii::app()->createUrl('site/index').'/group/'.urlencode($value['id']).'" title="'.CHtml::encode($value['name']).'">
                                <img src="'.CHtml::encode($picture).'" alt="'.CHtml::encode($value['name']).'" style="width: 60px; height: 50px;"/>
                            </a>';
                        }
                        ?>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('site/index').'/user/cgroups/'.urlencode($model->id); ?>" title="посмотреть всех участников группы" class="show-all-participants align-center"><?php echo Yii::t('var', 'просмотреть все');?></a>
                </div>
            </div>
        <?php } else { //title="пожаловаться на группу"?>
            <div class="sidebar-right">
				<a class="complain align-center b-settings-button" title="<?php echo Yii::t('var', 'Настройки'); ?>" onclick="window.location.href='<? echo Yii::app()->createUrl('site/index').'/user/update/'.urlencode($user_id); ?>'"><?php echo Yii::t('var', 'Настройки');?></a>
            </div>
            <div class="sidebar-right">
                <a href="<?php echo  Yii::app()->createUrl('user/complaints',array('id'=>$user_id)); ?>" title="жалобы" class="complain align-center b-complain-button">
                    <?php echo 'Жалобы';?>
                </a>
            </div>
            <div id="eventCalendarDefault"></div>
            <script>
                $(document).ready(function() {
                    $("#eventCalendarDefault").eventCalendar({
                        eventsjson: '<?php echo Yii::app()->createUrl('group/getusermeetsjson',array('user_id'=>$user_id)); ?>' // link to events json
                    });
                });
            </script>
        <?php } ?>

    </aside>

    <?php
		if($this->route == 'user/view'){
			$this->renderPartial('_wall', array(
				'user_id'=>$user_id,
				'model'=>$model,
				'friends'=>$friends,
				'user_name' => User::model()->getUserName($user_id),
			));
		}elseif($this->route == 'user/complaints'){
			$this->renderPartial('complaints_list', array(
				'user_id'=>$user_id,
				'model'=>$model,
				'complaints'=>$complaints
			));
		}elseif($this->route == 'user/mycomplaints'){
			$this->renderPartial('mycomplaints_list', array(
				'user_id'=>$user_id,
				'model'=>$model,
				'complaints'=>$complaints
			));
		}elseif($this->route == 'user/answer'){
			$this->renderPartial('_complaints', array(
				'user_id'=>$user_id,
				'model'=>$model,
				'complaint' => $complaint
			));
		}elseif($this->route == 'user/checkAnswer'){
			$this->renderPartial('_complaints', array(
				'user_id'=>$user_id,
				'model'=>$model,
				'complaint' => $complaint
			));
		}
	?>

</div>



<style>
    .box-modal {
        width:500px;
        margin: 0 auto;
        background-color: #ffffff;
        padding: 20px;
    }
</style>
<script>
    window.yii_csrf_token = '<?php echo Yii::app()->request->csrfToken ?>';
    function closeAnswerForm(parentId)
    {
        $('#answer-' + parentId + ' > .b-comment__answer-form-block').slideUp('fast').html('');
        $('#answer-buttons-' + parentId).removeClass('active');

        return false;
    }
    /* function showAnswerForm(parentId)
    {
        var complaintField = '';
		if('<?= $this->route ?>' == 'user/answer'){
			var text_id = '<?= isset($complaint[$_GET['complaint']]['text_id']) ? CHtml::encode($complaint[$_GET['complaint']]['text_id']) : 0; ?>';
			complaintField = '<input type="hidden" name="text_id" value="' + text_id + '" />';
		}
		
        parentId = parseInt(parentId);
		$('.b-comment__answer-form-block').slideUp('fast').html('');
        $('.b-comment__buttons').removeClass('active');

        var formHtml = '<form id="wall-form-' + parentId + '" action="<?php echo $this->route == 'user/view' ? Yii::app()->request->baseUrl.'/wall/sentRecord' : Yii::app()->request->baseUrl.'/complaint/sendRecord'; ?>"'
            + ' name="sent-record-' + parentId + '" method="post" class="">'
            + '<textarea class="b-comment__answer-textarea f-left" name="text"></textarea><br>'
            + '<input class="b-comment__answer-submit f-right" type="image" src="/images/send-comment.png" />'
            + '<input type="hidden" name="<?php echo $this->route == 'user/answer' ? 'item_id' : 'user_id' ?>" value="<?php echo $this->route == 'user/answer' && isset($complaint[$_GET['complaint']]['text_id']) ? CHtml::encode($complaint[$_GET['complaint']]['item_id']) : CHtml::encode($model->id); ?>" />'
            + '<input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />'
            + '<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />'
            + '<input type="hidden" name="parent_id" value="' + parentId + '" />'+ complaintField +'<div class="clear"></div></form>';

        $('#answer-' + parentId + ' > .b-comment__answer-form-block').html(formHtml).slideDown('fast');
        $('#answer-buttons-' + parentId).toggleClass('active');

        return false;
    } */
    function closeParentForm(){
		$('.prependBlock').remove();
	}
    function showAnswerForm(parentId, that)
    {
        /* parentId = parseInt(parentId);
        $('.b-comment__answer-form-block').slideUp('fast').html('');
        $('.b-comment__buttons').removeClass('active');

        var formHtml = '<form id="wall-form-' + parentId + '" action="<?php echo Yii::app()->request->baseUrl; ?>/group/sentComment"'
            + ' name="sent-record-' + parentId + '" method="post" class="">'
            + '<textarea class="b-comment__answer-textarea f-left" name="text"></textarea><br>'
            + '<input class="b-comment__answer-submit f-right" type="image" src="/images/send-comment.png" />'
            + '<input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>" />'
            + '<input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />'
            + '<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />'
            + '<input type="hidden" name="parent_id" value="' + parentId + '" /><div class="clear"></div></form>';

        $('#answer-' + parentId + ' > .b-comment__answer-form-block').html(formHtml).slideDown('fast');
        $('#answer-buttons-' + parentId).toggleClass('active');

        return false; */
		$('.prependBlock').remove();
		f = $(that).closest('.b-comment__content').find('.b-comment__author a').text();
		console.log($(that).parent().parent(),'888');
		$('.b-comment__textarea.f-left').focus();
		$('#wall-form').prepend("<div class='prependBlock' style='color:grey'>Ответ для "+f+" <span class='closeParentForm' style='cursor: pointer;' title='Закрыть' onclick='closeParentForm()'>(<b style='color:#4A9DD9;'>х</b>)<span></div><input class='prependBlock' type='hidden' name='parent_id' value='"+parentId+"' />");
    }
    $(document).ready(function(){
        $('.remove-interest').on('click',function(){
            $.ajax({
                type: 'GET',
                url: '<?php echo Yii::app()->createUrl('keyword/RemoveUserInterest');?>',
                data: {
                    user_id: <?php echo (int)$model->id; ?>,
                    kid: $(this).parent().attr('kid')
                },
                success: function(data){
                    window.location.reload();
                }
            });
        });
    });

</script>
<div style="display: none;">
    <div class="box-modal" id="mailto">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="mailto-form">
            <table>
                <tr><td><? echo Yii::t('var', 'Введите Ваше сообщение');?>:</td><td><textarea name="message"></textarea></td></tr>
                <tr><td></td><td><input class="submit" type="submit" value="Отправить"></td></tr>
            </table>
            <input type="hidden" name="group_id" value="<?php echo CHtml::encode($model->id); ?>">
            <input type="hidden" name="user_id" value="<?php echo CHtml::encode($user_id); ?>">
            <input type="hidden" name="email" value="<?php echo CHtml::encode($email); ?>">
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#mailto-form").validate({
                    rules: {
                        message: "required"
                    },
                    messages: {
                        message: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    },
                    submitHandler: function(form) {
                        $.ajax({
                            type: 'GET',
                            url: '<? echo Yii::app()->createUrl('ajax/mailto'); ?>',
                            data: $('#mailto-form').serialize(),
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
<div style="display: none;">
    <div class="box-modal" id="interest">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="interest-form">
            <span class="all-cats"><?php echo Yii::t('var', 'Все категории');?></span>
            <ul class="keywords-list">
                <?php echo Keyword::model()->getInterestsCategory(); ?>
            </ul>
            <script>
                $(document).ready(function(){

                    var height = $('.group-img img').height();
                    var width = $('.group-img img').width();
                    var left,top;
                    if (width > height) {
                        $('.group-img img').css({'height':'192px'});
                        width = $('.group-img img').width();
                        left = (width - 185.5)/2;
                        $('.group-img img').css({'left':'-'+left+'px'});
                    }
                    else {
                        $('.group-img img').css({'width':'185.5px'});
                        height = $('.group-img img').height();
                        top = (height - 192)/2;
                        $('.group-img img').css({'top':'-'+top+'px'});
                    }

                    $(document).on('click','.keywords-list-cat',function(){
                        var name = $(this).html();
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('keyword/getcategoryinterestslist'); ?>',
                            data: {
                                cat: $(this).html()
                            },
                            success: function(data){
                                $('.keywords-list').html(data);
                            }
                        });
                    });
                    $(document).on('click','.all-cats',function(){
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('keyword/getcategorylist'); ?>',
                            data: {
                                cat: $(this).html()
                            },
                            success: function(data){
                                $('.keywords-list').html(data);
                            }
                        });
                    });
                    $(document).on('click','.keywords-list-keyword',function(){
                        $.ajax({
                            type: 'GET',
                            url: '<?php echo Yii::app()->createUrl('keyword/insertuserinterest'); ?>',
                            data: {
                                user_id: <?php echo (int)$user_id; ?>,
                                keyword: $(this).html()
                            },
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    });
                });
            </script>
        </form>

    </div>

    <div class="box-modal" id="complaint">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="" method="get" id="complaint-form">
            <table>
                <tr>
                    <td><br /><?php echo Yii::t('var', 'Введите текст жалобы');?>:<br /><br /></td>
                </tr>
                <tr>
                    <td>
                        <textarea cols="66" rows="9" name="text"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
						<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />
                        <input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отправить');?>">
                    </td>
                </tr>
            </table>
            <input type="hidden" name="mid" value="<?php echo (int)$model->id; ?>">
            <input type="hidden" name="text_id" value="user">
        </form>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js"></script>
        <script>
            $().ready(function() {

                $("#complaint-form").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "Обязательное поле"
                    },
                    submitHandler: function(form) {
						$.arcticmodal('close');
                        $.ajax({
                            type: 'POST',
                            url: '<? echo Yii::app()->createUrl('complaint/sentComplaint'); ?>',
                            data: $('#complaint-form').serialize() + '&user_id=<?php echo urlencode(User::model()->getUserId()); ?>',
                            success: function(data){
                                window.location.reload();
                            }
                        });
                    }
                });
            });
        </script>
    </div>
</div>
