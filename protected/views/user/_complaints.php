<?php
function showCommentsRecursive($comments, $ets = true)
{
    static $user_id = -1;
    if ($user_id == -1) {
        $user_id = User::model()->getUserId();
    }

    foreach($comments as $comment):
        if (empty($comment['text'])) {
            continue;
        }
        $date = explode(' ', CHtml::encode($comment['date']));
        $date[0] = '<strong>' . CHtml::encode(date('d.m.Y', strtotime($date[0]))) . '</strong> ';
        $date = implode($date);

        $answerModificator = empty($comment['children'])
            ? ''
            : 'b-comment__answer_border';
        ?>
        <div class="b-comment" id="comment-<?php echo CHtml::encode($comment['id']); ?>">
            <div class="b-comment__content">
                <div class="b-comment__head">
                    <div class="b-comment__author">
                        <a href="<?php echo Yii::app()->createUrl('user/view') . '/' . urlencode($comment['sender_id']); ?>"
                           target="_blank"><?php echo CHtml::encode($comment['sender_name']); ?></a>
                    </div>
                    <div class="b-comment__date">[<?php echo $date; ?>]</div>
                </div><!-- .b-comment__head-->
                <div class="b-comment__body"><?php echo CHtml::encode($comment['text']); ?></div>
            </div><!-- .b-comment__content-->
            <div class="b-comment__answer <?php echo $answerModificator; ?>" id="answer-<?php echo (int)$comment['id']; ?>">
                <div class="b-comment__answer-form-block"></div>
                <?php if (!empty($comment['children'])): ?>
                    <div class="b-comment__answer-body">
                        <?php showCommentsRecursive($comment['children'], false); ?>
                    </div><!-- .b-comment__answer-body-->
                <?php endif; ?>
            </div><!-- .b-comment__answer-->
			
        </div><!-- .b-comment-->
                <?php if ($user_id && $ets): ?>
					<br>
					<a href="#" class="c-comment__answer-open" onclick="return false;">Ответить</a>
					<form action="<?= Yii::app()->request->baseUrl.'/complaint/sendRecord'; ?>" method="post">
						<textarea class="b-comment__answer-textarea f-left" name="text"></textarea><br>
						<input class="b-comment__answer-submit f-right" type="image" src="/images/send-comment.png" />
						<input type="hidden" name="item_id" value="<?= CHtml::encode($comment['item_id']) ?>" />
						<input type="hidden" name="sender_id" value="<?= CHtml::encode($user_id); ?>" />
						<input type="hidden" name="YII_CSRF_TOKEN" value="<?= Yii::app()->request->csrfToken ?>" />
						<input type="hidden" name="parent_id" value="<?= CHtml::encode($comment['id']) ?>" />
						<input type="hidden" name="text_id" value="<?= CHtml::encode($comment['text_id']) ?>" />
						<div class="clear"></div>
					</form>
                <?php endif; ?>
    <?php
    endforeach;
}
$this->setPageTitle('Ответ на жалобу');
$listUrl = $this->route == 'user/complaints' ? Yii::app()->createUrl('user/complaints', array('id'=>$model->id)) : Yii::app()->createUrl('user/mycomplaints');
$listName = $this->route == 'user/complaints' ? 'Поступившие жалобы' : 'Мои жалобы';
$this->breadcrumbs=array(
	$model->name => Yii::app()->createUrl('user/view', array('id'=>$model->id)),
	$listName => $listUrl,
	'Ответ на жолобу',
);
$compid = $_GET['complaint'];
if($complaint[$compid]['text_id'] == 'group' || $complaint[$compid]['text_id'] == 'meet'){
	$groupModel = Group::model();
	$accusedName = $groupModel->getGroupName($complaint[$compid]['item_id']);
	if($complaint[$compid]['text_id'] == 'group'){
		$accusedLink = Yii::app()->createUrl('group/view', array('id'=>$complaint[$compid]['item_id']));
		$compstr = "группу <a href=\"$accusedLink\">$accusedName</a>";
	}else{
		$accusedLink = Yii::app()->createUrl('group/meet', array('id'=>$complaint[$compid]['parent_id'], 'mid' => $complaint[$compid]['item_id']));
		$compstr = "встречу <a href=\"$accusedLink\">$accusedName</a>";
	}
}else{
	//$userModel = Group::model();
	//$accusedName = $groupModel->getUserName($complaint['item_id']);
	if($complaint[$compid]['user_id'] == $user_id)
		$compstr = ' <a href="'. Yii::app()->createUrl('user/view', array('id'=>$complaint[$compid]['item_id'])).'">'.User::model()->getName($complaint[$compid]['item_id']).'</a>';
	else	
		$compstr = ' Вас';
}
$h2 = $user_id != $complaint[$compid]['user_id'] || $this->route == 'user/checkAnswer' ? 'Жалоба на ' . $compstr . ' от <a href="'.Yii::app()->createUrl('user/view', array('id'=>$complaint[$compid]['user_id'])).'">'.$complaint[$compid]['sender_name'].'</a> ' : 'Жалоба на '.$compstr;
?>
<section id="group-content" class="f-left">
	<div class="content-block-wrap">
		<h2 class="sharp" style="padding-left: 30px;" id="comments"><?=$h2?></h2>
		<div class="content-block">
			<?php
			echo $this->route == 'user/checkAnswer' ? '<a href="'.Yii::app()->createUrl('complaint/resolve', array('id'=>$complaint[$compid]['id'])).'" style="float:right;"> [<b>Пометить как "решено"</b>]</a>' : '';
			showCommentsRecursive($complaint);

			if ($user_id):
			?>
				<!--<form id="wall-form" action="<?php echo Yii::app()->request->baseUrl; ?>/wall/sentRecord"
					  name="sent-record" method="post">
					<textarea class="b-comment__textarea f-left" name="text"></textarea><br>
					<input class="f-right" type="image" src="/images/send-comment.png" />
					<input type="hidden" name="user_id" value="<?php echo CHtml::encode($model->id); ?>" />
					<input type="hidden" name="sender_id" value="<?php echo CHtml::encode($user_id); ?>" />
					<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />-->
				</form>
			<?php endif; ?>
		</div>
	</div>
</section>
