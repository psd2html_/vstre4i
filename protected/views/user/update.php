<?php
/* @var $this UserController */
/* @var $model User */

$this->setPageTitle('Настройки - ' . $model->name . ' - '. Yii::app()->name);
$this->breadcrumbs=array(
	CHtml::encode($model->name)=>array('view','id'=>(int)$model->id),
	'Настройки',
);
?>

<div class="center">
    <div class="white-block">
        <h1 class="align-center"><?php echo CHtml::encode($model->name); ?></h1>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>
