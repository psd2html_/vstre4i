<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);

?>


<div class="center">
    <div class="white-block">
        <h1 class="align-center">Создать пользователя</h1>
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>
