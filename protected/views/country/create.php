<?php
/* @var $this CountryController */
/* @var $model Country */
$this->breadcrumbs=array(
	'Страны'=>array('admin'),
	'Добавить',
);
?>

<h1><?php echo Yii::t('var','Create Country'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels' => $trModels)); ?>
