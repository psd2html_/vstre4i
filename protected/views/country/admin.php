
<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('country/admin')?>" title="<?php echo Yii::t('var','Страны')?>"><?php echo Yii::t('var','Страны')?></a></li>
    <li><a href="<?php echo CController::createUrl('city/admin')?>" title="<?php echo Yii::t('var','Города')?>"><?php echo Yii::t('var','Города')?></a></li>
    <li><a href="<?php echo CController::createUrl('region/admin')?>" title="<?php echo Yii::t('var','Регионы')?>"><?php echo Yii::t('var','Регионы')?></a></li>
</ul>
<div class="b-admin-content">
<?php
/* @var $this CountryController */
/* @var $model Country */

$this->menu=array(
	array('label'=>Yii::t('var','Create Country'), 'url'=>array('create')),
);
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'country-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
			'name' => 'name',
            'value' => 'CHtml::link($data->name, Yii::app()->createUrl(\'city/admin\', array(\'id\'=>$data->id)))',
			'type' => 'html',
		),
        array(
            'name'=>'ua',
            'value'=> '@$data->translate[0]->language == "ua"?@$data->translate[0]->value:(@$data->translate[1]->language == "ua"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','ua')
        ),
        array(
            'name'=>'kz',
            'value'=> '@$data->translate[0]->language == "kz"?@$data->translate[0]->value:(@$data->translate[1]->language == "kz"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','kz')
        ),
        array(
            'name'=>'be',
            'value'=> '@$data->translate[0]->language == "be"?@$data->translate[0]->value:(@$data->translate[1]->language == "be"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','be')

        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
