<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('admin/index')?>" title="Настройки">Настройки</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('admin/metaTags')?>" title="Метатеги">Метатеги</a></li>
</ul>
<div class="b-admin-content">
    <?php
    /* @var $this UserController */
    /* @var $model User */

    /* $this->breadcrumbs=array(
        'Users'=>array('index'),
        'Manage',
    ); */
    ?>

    <?php echo CHtml::form('','POST')?>
    <table class="b-form-table metaTags">
        <tr>
            <td valign="top"><?php echo CHtml::label('Описание','description'); ?></td>
            <td><?php echo CHtml::textArea('default[description]',$data['description']['value'], array('maxlength'=>255)); ?></td>
        </tr>
        <tr>
            <td valign="top"><?php echo CHtml::label('Ключевые слова','keywords'); ?></td>
            <td>
				<?php echo CHtml::textArea('default[keywords]',$data['keywords']['value'], array('maxlength'=>255)); ?>
				<div class="row submit">
					<?php echo CHtml::submitButton('Сохранить')?>
				</div>
			</td>
        </tr>
    </table>
    <?php CHtml::endForm(); ?>
</div><!-- .b-admin-content-->
