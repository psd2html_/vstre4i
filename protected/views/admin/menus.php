<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('admin/menus')?>" title="Верхнее меню">Верхнее</a></li>
    <li><a href="<?php echo CController::createUrl('bottomMenus/admin')?>" title="Нижнее меню">Нижнее</a></li>
</ul>
	<?php
	$columnTitles = array(
		'id'      => 'ID',
		'menu_id' => 'ID меню',
		'type'    => 'Тип',
		'title'   => 'Заголовок',
		'params'  => 'Параметры',
		'order_number' => 'Порядок',
	);
	?>
	<div class="b-admin-content">
		<?php if(!empty($menus)): ?>
			<?php echo CHtml::beginForm('saveMenus'); ?>
				<?php foreach($menus as $menu): ?>
					<?php if(!empty($menu)): ?>
						<table class="b-admin-table">
							<thead>
								<tr>
									<?php foreach(array_keys(current($menu)) as $title): 
										if(!$columnTitles[$title]) continue;
									?>
										<th>
											<?php
											if ( isset($columnTitles[$title]) ):
												echo CHtml::encode($columnTitles[$title]);//Yii::t('var', $columnTitles[$title]);
											else:
												echo CHtml::encode($title);
											endif;
											?>
										</th>
									<?php endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach($menu as $element): ?>
									<tr>
										<?php foreach($element as $key => $field):
										if(!$columnTitles[$key]) continue; ?>
											<td>
												<?php
												if($key == 'id'):
													echo CHtml::encode($field);
												else:
												?>
													<input type="text"
														   name="elements[<?php echo CHtml::encode($element['id']); ?>][<?php echo CHtml::encode($key); ?>]"
														   value="<?php echo CHtml::encode($field); ?>" />
												<?php endif;?>
											</td>
										<?php endforeach; ?>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php endif; ?>
				<?php endforeach; ?>
				<div class="row submit">
					<?php echo CHtml::submitButton('Сохранить');//Yii::t('var', 'Сохранить'); ?>
				</div>
			<?php echo CHtml::endForm(); ?>
		<?php endif; ?>
	</div>
