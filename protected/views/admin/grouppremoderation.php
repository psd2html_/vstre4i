<?php
Yii::app()->clientScript
    ->registerCoreScript('jquery')
    ->registerCssFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css') //css
    ->registerScriptFile(Yii::app()->request->baseUrl . 'http://code.jquery.com/ui/1.10.3/jquery-ui.js');

$this->setPageTitle('Премодерация групп - ' . Yii::app()->name);

$this->breadcrumbs=array(
    'Премодерация групп',
);
?>

<div class="b-admin-content">
    <?php
    $premoderation = Group::model()->getPremoderationSettings();
    if (!empty($premoderation)):
    ?>
        <h2>Настройки премодерации</h2>
        <form name="group_premoderation" method="post" class="b-premoderation-settings"
              action="<?php echo Yii::app()->createUrl('group/savePremoderationSettings')?>">
            <table class="b-form-table" style="max-width: 400px;">
                <tr>
                    <td colspan="2">
                        <input type="checkbox" value="1" <?php echo $premoderation['mode'] ? 'checked' : ''; ?>
                               name="group_premoderation_mode" id="group-premoderation-mode-input" />
                        <label for="group-premoderation-mode-input" class="checkbox-label">Режим премодерации групп</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="group-premoderation-limit-input">Предел количества групп <br />в очереди для рассмотрения</label>
                    </td>
                    <td>
                        <input name="group_premoderation_limit" value="<?php echo CHtml::encode($premoderation['limit']); ?>"
                               type="text" id="group-premoderation-limit-input" size="5" style="width: 20px;"/>
                    </td>
                </tr>
            </table><!-- .b-form-table-->
            <span class="b-little-notice">(по заполнению очереди, группы будут публиковаться автоматически; 0 - без ограничения)</span>
            <input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken;; ?>" />
            <div class="row submit">
                <input type="submit" value="Сохранить" name="" />
            </div>
        </form>
    <?php endif;?>
    <hr />
    <?php
    $groups = $model->getPremoderationQueue();
    $admins = array();
	$countGroups = count($groups);
   if ($countGroups) {
        echo '<br /><h4><strong>Группы, требующие проверку: '.$countGroups.'</strong></h4><br />';
    } else {
        echo '<h4>Групп, требующих проверку нет.</h4><br />';
    }
    foreach ($groups as $group):
        $admins[$group['id']] = array(
            'admin_id'   => $group['admin_id'],
            'admin_name' => $group['admin_name']
        );
        $groupPic = $group['picture']
            ? '/images/group/mini_' . $group['picture']
            : '/images/group/mini_no-pic.png';
        ?>
        <div class="b-premoderation">
            <?php
            $groupLink = Yii::app()->createUrl('group/view') . '/' . urlencode($group['id']);
            $approveLink = Yii::app()->createUrl('admin/approvegroup') . '?group_id=' . urlencode($group['id']);
            $adminLink = Yii::app()->createUrl('user/view') . '/' . urlencode($group['admin_id']);
            ?>
            <div class="b-premoderation__group-info">
                <a href="<?php echo $groupLink; ?>" target="_blank">
                    <img src="<?php echo CHtml::encode($groupPic); ?>" class="b-premoderation__pic" />
                    <br />
                    <span><?php echo CHtml::encode($group['name']); ?></span>
                </a>
            </div><!-- .b-moderator__info-->
            <div class="b-premoderation__buttons">
                <div><a href="<?php echo $groupLink; ?>" target="_blank">Предпросмотр группы</a></div>
                <a href="<?php echo $approveLink; ?>" class="b-premoderation__approve"
                     id="approve-<?php echo CHtml::encode($group['id']); ?>">Одобрить</a>
                &nbsp;|&nbsp;
                <span class="pseudo-link b-premoderation__discard"
                     id="discard-<?php echo CHtml::encode($group['id']); ?>">Отказать и указать причину</span>
                <div class="b-premoderation__group-admin"
                     id="group-admin-<?php echo CHtml::encode($group['id']); ?>">
                    <span class="b-premoderation__admin-detail"
                          id="admin-<?php echo CHtml::encode($group['admin_id']); ?>">Администратор: </span>
                    <a href="<?php echo $adminLink; ?>" target="_blank"><?php echo CHtml::encode($group['admin_name']); ?></a>
                </div>
            </div><!-- .b-moderator__button-->
            <div style="clear: both;"></div>
        </div><!-- .b-moderator-->
    <?php endforeach; ?>
</div><!-- .b-admin-content-->
<script>
    $().ready(function(){
        $('.b-premoderation__discard').click(function(){
            var groupId = $(this).attr('id').split('-')[1];
            var adminId = $('#group-admin-' + groupId + ' .b-premoderation__admin-detail')
                .attr('id').split('-')[1];
            $('#message-to-group-admin').arcticmodal();
            $('.b-discard-message__recipient').html(
                $('#group-admin-' + groupId + '.b-premoderation__group-admin').html()
            );
            $('#message-to-group-admin-form input[name="group_id"]').val(groupId);
            $('#message-to-group-admin-form input[name="admin_id"]').val(adminId);
        });
    });
</script>
<div style="display: none;">
    <div class="box-modal" id="message-to-group-admin">
        <div class="box-modal_close arcticmodal-close"></div>
        <form action="<?php echo Yii::app()->createUrl('admin/discardgroup'); ?>"
              method="post" id="message-to-group-admin-form">
            <table>
                <tr>
                    <td colspan="2">Вы можете отправить сообщение с причиной отказа администратору группы:</td>
                </tr>
                <tr>
                    <td class="b-discard-message__recipient">

                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td><?php echo Yii::t('var', 'Введите ваше сообщение');?></td>
                    <td><textarea name="text" ></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input class="submit" type="submit" value="<?php echo Yii::t('var', 'Отказать');?>"></td>
                </tr>
            </table>
            <input value="0" name="group_id" type="hidden"/>
            <input value="0" name="admin_id" type="hidden" />
            <input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken;?>" />
        </form>
        <script>
            $().ready(function() {
                $("#message-to-group-admin").validate({
                    rules: {
                        text: "required"
                    },
                    messages: {
                        text: "<?php echo Yii::t('var', 'Обязательное поле');?>"
                    }
                });
            });
        </script>
    </div>
</div>
