<div class="">
    <table class="b-form-table">
        <tr>
            <td>
                <?php echo CHtml::label('Номер кошелька',$data['webmoney']['key'])?>
            </td>
            <td>
                <?php echo CHtml::textField($data['webmoney']['key'],$data['webmoney']['value'])?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo CHtml::label('Секретный ключ','webmoney_secret_key')?>
            </td>
            <td>
                <?php echo CHtml::textField('webmoney_secret_key',$data['webmoney_secret_key']['value'])?>
            </td>
        </tr>
    </table><!-- .b-form-table-->
</div>