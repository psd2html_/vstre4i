<?php
/* @var $this CommentController */
/* @var $model Comment */

$this->breadcrumbs=array(
	'Комментарии'=>array('admin'),
	'Обновить',
);

$this->menu=array(
	array('label'=>'List Comment', 'url'=>array('index')),
	array('label'=>'Create Comment', 'url'=>array('create')),
	array('label'=>'View Comment', 'url'=>array('view', 'id'=>(int) $model->id)),
	array('label'=>'Manage Comment', 'url'=>array('admin')),
);
?>
<h1>Обновление комментария</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
