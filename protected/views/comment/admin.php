<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('wall/admin')?>" title="<?php echo Yii::t('var','Стены')?>"><?php echo Yii::t('var','Стены')?></a></li>
    <li <?php if(isset($_GET['type']))echo 'class="active"' ?>><a href="<?php echo CController::createUrl('comment/admin',array('type'=>'meet'))?>" title="<?php echo Yii::t('var','Встречи')?>"><?php echo Yii::t('var','Встречи')?></a></li>
    <li <?php if(!isset($_GET['type']))echo 'class="active"' ?> ><a href="<?php echo CController::createUrl('comment/admin')?>" title="<?php echo Yii::t('var','Группы')?>"><?php echo Yii::t('var','Группы')?></a></li>
</ul>
<div class="b-admin-content">
    <?php
    /* @var $this WallController */
    /* @var $model Wall */

?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comment-grid',
	'summaryText'=>'Всего: {count}',
	'dataProvider'=>$model->search($isMeet),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name'=>'user_id',
            'value' => '@CHtml::encode($data->user->name)."(".CHtml::encode($data->user_id).")"',
            'header' => Yii::t('var','user_id')
        ),

		array(
            'name'=>'meet_id',
            'value' => '@CHtml::encode($data->group->name)."(".CHtml::encode($data->meet_id).")"',
            'header' => Yii::t('var','meet_id')
        ),
		array(
            'name' => 'text',
            'header' => Yii::t('var','text')
        ),
		array(
            'name' => 'date',
            'header' => Yii::t('var','date')
        ),
		array(
			'class'=>'CButtonColumn',
            'template'=>'{move}{update}{delete}',
            'buttons'=>array
            (
                'move' => array
                (
                    'label'=>'alias',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/ban.png',
                    'visible' => '1',
                    'url'=>'Yii::app()->createUrl("group/view",array("id"=>$data->meet_id))',
                ),
            )
		),
	),
)); ?>


</div>
