<?php
/* @var $this ComplaintController */
/* @var $model Complaint */
$this->breadcrumbs=array(
	'Жалобы'=>array('admin'),
	'Обновить',
);
?>

<h1><?php echo Yii::t('var','Update Complaint')." ".CHtml::encode($model->id); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
