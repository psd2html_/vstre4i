<?php 
$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Написать',
);
?>

<div class="form">
	<?php echo CHtml::beginForm(CController::createUrl('mail/sendToAdress',array('id'=>$user_id)),'POST',array('id'=>'send-email-form'))?>
		<div>
		<?php echo CHtml::label('Заголовок','subject')?>
		<?php echo CHtml::textField('subject',$letter['name'])?>
		</div>
		<br />
		<div>
		<?php echo CHtml::label('Адресс','last_email')?>
		<?php echo CHtml::textField('last_email',$letter['last_email'])?>
		</div>
		<br />
		<?php echo CHtml::textArea('message',$letter['text'],array('id'=>'Mail_text'))?>
		<br />
		<?php echo CHtml::submitButton('Отправить'); ?>
	<?php echo CHtml::endForm();?>
</div>
