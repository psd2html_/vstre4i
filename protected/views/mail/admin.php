<?php
/* @var $this MailController */
/* @var $model Mail */
?>
<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('email/sendEmailToAll'); ?>" title="Рассылка">Рассылка</a></li>
    <li class="active"><a href="<?php echo CController::createUrl('mail/admin')?>" title="Архив сообщений">Архив </a></li>
</ul>
<div class="b-admin-content">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mail-grid',
	'summaryText'=>'Всего: {count}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'text',
		'last_email',
		'timestamp',
		array(
			'class'=>'CButtonColumn',
            'template'=>'{mail}{delete}',
            'buttons'=>array
            (
                'mail' => array
                (
                    'label'=>'mail',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/mail.png',
                    'click'=>"function(){
                                    var id = $(this).parent().parent().find(':first-child').html();
                                    var url = '".Yii::app()->getBaseUrl(true)."/mail/GetArchiveEmailForm?id='+id;
                                    $('.email-form').load(url,{},function(cont){
                                    try{
                                        CKEDITOR.replace( 'Mail_text');
                                        } catch (e){

                                        }
                                        $(this).show();
                                    });
                                    return false;
                              }
                     ",
                    'url'=>'Yii::app()->controller->createUrl("mail/getArchiveEmailForm",array("id"=>$data->id))',
                ),
            )
		),
	),
)); ?>
</div>
