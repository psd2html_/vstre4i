<?php
/* @var $this MenuController */
/* @var $model Menu */
$this->breadcrumbs=array(
	'Меню'=>array('admin'),
	'Создать',
);
?>

<h1>Создать пункт нижнего меню</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
