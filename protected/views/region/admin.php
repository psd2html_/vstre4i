
<ul class="tab-container">
    <li><a href="<?php echo CController::createUrl('country/admin')?>" title="<?php echo Yii::t('var','Страны')?>"><?php echo Yii::t('var','Страны')?></a></li>
    <li><a href="<?php echo CController::createUrl('city/admin')?>" title="<?php echo Yii::t('var','Города')?>"><?php echo Yii::t('var','Города')?></a></li>
    <li class="active"><a href="<?php echo CController::createUrl('region/admin')?>" title="<?php echo Yii::t('var','Регионы')?>"><?php echo Yii::t('var','Регионы')?></a></li>
</ul>
<div class="b-admin-content">
    <?php
/* @var $this regionController */
/* @var $model region */

$this->menu=array(
	array('label'=>Yii::t('var','Create region'), 'url'=>array('create')),
);
    $this->widget('zii.widgets.CMenu', array(
        'items'=>$this->menu,
        'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
    ));

?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'region-grid',
	'summaryText'=>'Всего: {count}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array('name'=>'country_id',
            'value'=>'$data->country->name',
            'header' => Yii::t('var','country'),
			'filter' => Country::getStatCountries()
		),
		array('name'=>'name',
              'value' => '$data->name',
              'header' => Yii::t('var','name')
        ),
        array(
            'name'=>'ua',
            'value'=>'$data->translate?$data->translate[0]->value:NULL',
            'header' => Yii::t('var','ua')
        ),
        array(
            'name'=>'kz',
            'value'=>'$data->translate?$data->translate[1]->value:NULL',
            'header' => Yii::t('var','kz')
    ),
        array(
            'name'=>'be',
            'value'=>'$data->translate?$data->translate[2]->value:NULL',
            'header' => Yii::t('var','be')
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>
