<?php
/* @var $this regionController */
/* @var $model region */

$this->breadcrumbs=array(
	'Регионы'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'=>'List region', 'url'=>array('index')),
	array('label'=>'Create region', 'url'=>array('create')),
	array('label'=>'View region', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage region', 'url'=>array('admin')),
);
?>

<h1>Обновить регион: <?php echo CHtml::encode($model->name); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels' => $trModels)); ?>
