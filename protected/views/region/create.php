<?php
/* @var $this regionController */
/* @var $model region */
$this->breadcrumbs=array(
	'Регионы'=>array('admin'),
	'Добавить',
);
?>

<h1><?php echo Yii::t('var','Create region'); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels'=>$trModels)); ?>
