<?php
/* @var $this CityController */
/* @var $model City */
$this->breadcrumbs=array(
	'Города'=>array('admin'),
	'Добавить',
);
?>

<h1><?php echo Yii::t('var','Create City'); ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels'=>$trModels)); ?>
