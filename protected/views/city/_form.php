<?php
/* @var $this CityController */
/* @var $model City */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'city-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
        <?php echo $form->labelEx($model,'country_id'); ?>
        <?php //echo $form->dropDownList($model, 'country_id',  CHtml::listData(Country::model()->findAll(),'id', 'name'), array('empty'=>'Выберите страну'));?>
        <?php 
			if (Yii::app()->language != 'ru') {
				$criteria = new CDbCriteria();
				$criteria->alias = 'country';
				$criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
				$criteria->join = 'LEFT JOIN `translate` ON `country`.`id` = `translate`.`row_id`';
				$criteria->condition = "`translate`.`table_name`='country' AND `translate`.`language`='".Yii::app()->language."'";
				$models = Country::model()->findAll($criteria);
			} else {
				$models = Country::model()->findAll();
			}

			$list = CHtml::listData($models, 'id', 'name');
			
			echo CHtml::DropDownList(
            'City[country_id]',$model->country_id,$list,
            array('prompt'=>'Выберите страну',
                'onchange'=>
                    CHtml::ajax(
                        array(
                            'type'=>'GET',
                            'url'=>Yii::app()->createUrl('region/getRegions'),
                            'data' => array('country_id' => 'js:$(this).val()'),
                            'success'=> 'function(html, data){
								$("#City_region_id").html(html).show();
								//$("#subcat_1").html(html).hide();
								//$("#new-city").hide();
								//$("a#step1_b").hide();
								//var str1 = userRegion.split(" ")[0];
								/* $("#subcat_0 option").each(function(index,regOption){
									var str2 = $(regOption).text().split(" ")[0];
									if (str1 == str2) {
										$(regOption).prop("selected", true);
										$("#subcat_0").change();
									}
								}); */
                            }'
                        )
                    )
            )
        );
		
		?>
        <?php echo $form->error($model,'country_id'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'region_id'); ?>
        <?php //echo $form->dropDownList($model, 'region_id',  CHtml::listData(Region::model()->findAll(),'id', 'name'), array('empty'=>'Выберите регион'));?>
        <?php 
		$regAttrList = array('prompt'=>'Выберите регион',);
		if(!$model->isNewRecord){
			$regAttrList = array('prompt'=>'Выберите регион',
                'onchange'=>CHtml::ajax(
					array(
						'type'=>'GET',
						'url'=>Yii::app()->createUrl('city/getCities'),
						'data' => array('region_id' => 'js:$(this).val()'),
						'success'=> 'function(html){
							$("#City_name").html(html).show();
							//$(\'div#step1 select[name="city"]\').trigger(\'change\');
							//$("#new-city").show();
							/*if(userCity){
								$("#subcat_1 option").each(function(index,cityOption){
									if (userCity == $(cityOption).text()) {
										$(cityOption).prop("selected", true);
									}
								});
							} */
						}'
					)
				)
            );
		}
		echo CHtml::DropDownList(
            'City[region_id]',$model->region_id, Region::regionsList($model->country_id),
            $regAttrList
        );
		?>
        <?php echo $form->error($model,'region_id'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,Yii::t('var','name')); ?>
		<?php 
		if($model->isNewRecord){
			echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); 
		}else{
			echo CHtml::DropDownList(
				'City[other_id]',$model->id, City::cityList($model->region_id),
				array('prompt'=>'Выберите город','onchange'=>"
					$(this).next('input').val($(this).find('option:selected').text());
				")
			);
			if($model->approved == City::STATUS_NOT_APPROVED){
				echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100));
			}
		}
		?>
		<?php echo $form->error($model,'name'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,Yii::t('var','approved')); ?>
        <?php echo $form->dropDownList($model,'approved', array(
				City::STATUS_APPROVED => Yii::t('var', 'Подтверждено'),
				City::STATUS_NOT_APPROVED => Yii::t('var', 'Не подтверждено'),
			)); ?>
        <?php echo $form->error($model,'approved'); ?>
    </div>



    <div class="row">
        <?php echo $form->labelEx($trModels['ua'],Yii::t('var','ua')); ?>
        <?php echo $form->textField($trModels['ua'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[ua]')); ?>
        <?php echo $form->error($trModels['ua'],'value'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($trModels['kz'],Yii::t('var','kz')); ?>
        <?php echo $form->textField($trModels['kz'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[kz]')); ?>
        <?php echo $form->error($trModels['kz'],'value'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($trModels['be'],Yii::t('var','be')); ?>
        <?php echo $form->textField($trModels['be'],'value',array('size'=>60,'maxlength'=>100,'name'=>'translate[be]')); ?>
        <?php echo $form->error($trModels['be'],'value'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('var','Create') : Yii::t('var','Сохранить')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
