<?php
/* @var $this KeywordController */
/* @var $model Keyword */
$this->breadcrumbs=array(
	'Ключевые слова'=>array('admin'),
	'Обновить',
);
?>

<h1><?php echo Yii::t('var','Update Keyword')." ".CHtml::encode($model->name); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels'=>$trModels,'categories'=>$categories)); ?>
