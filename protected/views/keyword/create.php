<?php
/* @var $this KeywordController */
/* @var $model Keyword */

$this->breadcrumbs=array(
	'Ключевые слова'=>array('admin'),
	'Создать',
);

$this->menu=array(
	array('label'=>'List Keyword', 'url'=>array('index')),
	array('label'=>'Manage Keyword', 'url'=>array('admin')),
);
?>

<h1>Создать ключевое слово</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'trModels'=>$trModels,'categories'=> $categories)); ?>
