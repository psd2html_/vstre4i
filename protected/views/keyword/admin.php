<ul class="tab-container">
    <li class="active"><a href="<?php echo CController::createUrl('keyword/admin')?>" title="<?php echo Yii::t('var','Слова')?>"><?php echo Yii::t('var','Слова')?></a></li>
    <li ><a href="<?php echo CController::createUrl('category/admin')?>" title="<?php echo Yii::t('var','Категории')?>"><?php echo Yii::t('var','Категории')?></a></li>
</ul>
<div class="b-admin-content">
    <?php
/* @var $this KeywordController */
/* @var $model Keyword */


$this->menu=array(
	array('label'=>Yii::t('var','Create Keyword'), 'url'=>array('create')),
);


$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations b-admin-buttons'),
));

 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'keyword-grid',
	'summaryText'=>'Всего: {count}',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name'=>'category',
            'value' => '$data->category->name',
            'header' => Yii::t('var','category'),
			'filter'=>CHtml::listData(Category::model()->findAll(), 'id', 'name'),
        ),
        array(
		    'name' => 'name',
            'header' => Yii::t('var','name')
        ),
        array(
			'filter'=>false,
            'name'=>'ua',
            'value'=> '@$data->translate[0]->language == "ua"?@$data->translate[0]->value:(@$data->translate[1]->language == "ua"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','ua')
        ),
        array(
			'filter'=>false,
            'name'=>'kz',
            'value'=> '@$data->translate[0]->language == "kz"?@$data->translate[0]->value:(@$data->translate[1]->language == "kz"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','kz')
        ),
        array(
			'filter'=>false,
            'name'=>'be',
            'value'=> '@$data->translate[0]->language == "be"?@$data->translate[0]->value:(@$data->translate[1]->language == "be"?@$data->translate[1]->value:@$data->translate[2]->value)',
            'header' => Yii::t('var','be')
        ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
    </div>
