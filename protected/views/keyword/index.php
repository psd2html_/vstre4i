<?php
/* @var $this KeywordController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ключевые слова',
);

$this->menu=array(
	array('label'=>'Create Keyword', 'url'=>array('create')),
	array('label'=>'Manage Keyword', 'url'=>array('admin')),
);
?>

<h1>Ключевые слова</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'summaryText'=>'Всего: {count}',
	'itemView'=>'_view',
)); ?>
<br>
<br>
