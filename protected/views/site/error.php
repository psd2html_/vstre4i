<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - '.Yii::t('var', 'Ошибка');
$this->breadcrumbs=array(
    Yii::t('var', 'Ошибка'),
);
?>

<div class="center">
    <h2><? echo Yii::t('var', 'Ошибка');?> <?php echo $code; ?></h2>
<?php echo Yii::t('var', 'Страница не найдена'); ?>
</div>