<?php

$user_id = User::model()->getUserId();
$user = User::model()->findByPk($user_id);
if($user===null){
	$user = new stdClass();
	$user->country_id = 0;
	$user->region_id = 0;
	$user->city = '';
}

$this->setPageTitle('Создать группу - ' . Yii::app()->name);
$this->breadcrumbs = array(
    'Создать группу',
);
 
$cs = Yii::app()->clientScript;
$cs->registerScriptFile('/js/jquery.leanModal.min.js');
$cs->registerScriptFile('/js/chosen.jquery.min.js');
$cs->registerScriptFile('https://api-maps.yandex.ru/1.1/index.xml');
	$cs->registerScript('varYMap', "
		var map = new YMaps.Map();
		var userCountry = '';
		var userRegion = '';
		var userCity = '';
		var userProfileCountry = '".Country::model()->getCountryName($user->country_id)."';
		var userProfileRegion = '".Region::model()->getRegionName($user->region_id)."';
		var userProfileCity = '".$user->city."';
		
		if(userProfileCountry){
			userCountry = userProfileCountry;
		}else{
			userCountry = YMaps.location ? YMaps.location.country : '';
		}
		
		if(userProfileRegion){
			userRegion = userProfileRegion;
		}else{
			userRegion = YMaps.location ? YMaps.location.region : '';
		}
		
		if(userProfileCity){
			userCity = userProfileCity;
		}else{
			userCity = YMaps.location ? YMaps.location.city : '';
		}
	", CClientScript::POS_END);
	$cs->registerScript('stepClick', "
		$('div#step1').on('change', 'select[name=\"city\"]', function() {
			if ($(this).val() && !$(this).prop('disabled'))
				$('a#step1_b').css('display', 'inline-block');
			else $('a#step1_b').hide();
		});

		$(document).on('click','div#step2 li.keyword-item',function(){
			$('a#step2_b').css('display', 'inline-block');
		});

		$(document).on('keyup','div#step1 input[name=\"city_new\"]',function(){
			if ($(this).val() != '') {
				$('a#step1_b').css('display', 'inline-block');
			} else {
				$('a#step1_b').hide();
			}
		});

		$(document).on('keyup','div#step3 input[name=name]',function(){
			if ($('div#step3 input[name=name]').val() != '') {
				$('a#step3_b').css('display', 'inline-block');
			} else {
				$('a#step3_b').hide();
			}
		});

		$(document).on('keyup','div#step4 textarea[name=description]',function(){
			if ($('div#step4 textarea[name=description]').val() != '') {
				$('a#step4_b').css('display', 'inline-block');
			} else {
				$('a#step4_b').hide();
			}
		});
	", CClientScript::POS_READY);
$cs->registerScriptFile(Yii::app()->request->baseUrl."/js/functions_load_groaps.js");

?>
<div class="center">
<form id="create-group-form" class="b_input" method="POST" action="<?php echo Yii::app()->createUrl('group/createGroup'); ?>">
    <div class="step rounded" id="step1">

        <h2 class="b-create-group__caption"><?php echo Yii::t('var', 'Шаг 1: Укажите страну и город, где будут проходить встречи.');?></h2>
        <?php

        if (Yii::app()->language != 'ru') {
            $criteria = new CDbCriteria();
            $criteria->alias = 'country';
            $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
            $criteria->join = 'LEFT JOIN `translate` ON `country`.`id` = `translate`.`row_id`';
            $criteria->condition = "`translate`.`table_name`='country' AND `translate`.`language`='".Yii::app()->language."'";
            $models = Country::model()->findAll($criteria);
        } else {
            $models = Country::model()->findAll();
        }

        $list = CHtml::listData($models, 'id', 'name');
        echo CHtml::DropDownList(
            'country','',$list,
            array('prompt'=>'Выберите страну',
                'onchange'=>
                    CHtml::ajax(
                        array(
                            'type'=>'GET',
                            'url'=>Yii::app()->createUrl('region/getRegions'),
                            'data' => array('country_id' => 'js:$(this).val()'),
                            'success'=> 'function(html, data){
								$("#subcat_0").html(html).show();
								$("#subcat_1").html(html).hide();
								$("#new-city").hide();
								$("a#step1_b").hide();
								var str1 = userRegion.split(" ")[0];
								$("#subcat_0 option").each(function(index,regOption){
									var str2 = $(regOption).text().split(" ")[0];
									if (str1 == str2) {
										$(regOption).prop("selected", true);
										$("#subcat_0").change();
									}
								});
                            }'
                        )
                    )
            )
        ); ?>
        <?php
        echo CHtml::DropDownList(
            'region','',array(),
            array('id' => 'subcat_0', 'style' => 'display:none',
                'onchange'=>
                    CHtml::ajax(
                        array(
                            'type'=>'GET',
                            'url'=>Yii::app()->createUrl('city/getCities'),
                            'data' => array('region_id' => 'js:$(this).val()'),
                            'success'=> 'function(html){
								$("#subcat_1").html(html).show();
								$(\'div#step1 select[name="city"]\').trigger(\'change\');
								$("#new-city").show();
								if(userCity){
									$("#subcat_1 option").each(function(index,cityOption){
										if (userCity == $(cityOption).text()) {
											$(cityOption).prop("selected", true);
										}
									});
								}
                            }'
                        )
                    )
            )
        );
        ?>
        <select id="subcat_1" name="city" style="display: none"></select>
        <input name="city_new" type="text" placeholder="Название города" style="display: none"/>
        <a id="new-city" style="cursor: pointer; display:none"><span><?php echo Yii::t('var', 'Моего города нет в списке')?></span></a>
        <a id="list-city" style="cursor: pointer; display:none"><span><?php echo Yii::t('var', 'Отмена')?></span></a>
        <script>
			yii_csrf_token = '<?php echo Yii::app()->request->csrfToken; ?>';
            $("#new-city").on('click', function(){
                $("#subcat_1").prop('disabled', true).hide();
                $("input[name=city_new]").show();
                $('a#step1_b').hide();
                $(this).hide();
                $('#list-city').show();
            });
            $("#list-city").on("click", function () {
                $(this).hide();
                $("#new-city").show();
                $('a#step1_b').hide();
                $("#subcat_1").prop('disabled', false).show();
                $("input[name=city_new]").hide();
                $("#subcat_0").trigger("change");
            })
        </script>
        <div class="break"></div>
        <a id="step1_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>

    <div class="step" id="step2">

        <h2 class="b-create-group__caption"><?php echo Yii::t('var', 'Шаг 2: Укажите категорию и ключевые слова.');?></h2>
        <?php echo Yii::t('var', 'Вы можете выбрать <strong>до 10 ключевых слов</strong>, которые лучшим образом описывают Вашу группу.');?>
        <div class="break"></div>
        <br />
        <div style="float:right;width: 380px;" class="item">
            <span><? echo Yii::t('var', 'Ваши ключевые слова');?></span>
            <div class="keywords" style="min-height: 50px;height: auto;"></div>
        </div>


        <div class="item" style="width: 400px;">
            <span><? echo Yii::t('var', 'Выберите категорию');?></span>
            <?php

            if (Yii::app()->language != 'ru') {
                $criteria = new CDbCriteria();
                $criteria->alias = 'category';
                $criteria->select = '`translate`.`row_id` as `id`, `translate`.`value` as `name`';
                $criteria->join = 'LEFT JOIN `translate` ON `category`.`id` = `translate`.`row_id`';
                $criteria->condition = "`translate`.`table_name`='category' AND `translate`.`language`='".Yii::app()->language."'";
                $models = Category::model()->findAll($criteria);
            } else {
                $models = Category::model()->findAll();
            }

            $list = CHtml::listData($models, 'id', 'name');
            echo CHtml::DropDownList('category','',$list,
                array('prompt'=>'Выберите категорию',
                    'onchange'=> CHtml::ajax(array('type'=>'GET',
                        'url'=>Yii::app()->createUrl('keyword/getKeywords'),
                        'data' => array('cat_id' => 'js:$(this).val()'),
                        'update'=> '#keywords'          ))
                )
            );
            ?>
        </div>
        <div class="clear"></div>
        <div class="arrow-up"></div>
        <div class="list_container" id="keywords">

        </div>
        <input type="hidden" name="keywords" value="">
        <div class="break"></div>
        <a id="step2_b" class="button button-blue step_b">
            <span><? echo Yii::t('var', 'Я закончил выбор');?></span>
        </a>
    </div>

    <div class="step" id="step3">
        <h2 class="b-create-group__caption"><?php echo Yii::t('var', 'Шаг 3: Укажите название вашей группы.');?></h2>
        <div class="item">
            <span><?php echo Yii::t('var', 'Как будет называться группа');?></span>
            <input type="text" name="name">
        </div>

        <div class="break"></div>
        <a id="step3_b" class="button button-blue step_b">
            <span><?php echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>

    <div class="step" id="step4">
        <h2 class="b-create-group__caption"><?php echo Yii::t('var', 'Шаг 4: Введите описание группы.');?></h2>
        <div class="item">
            <span style="float: left; margin-top: 6px;"><?php echo Yii::t('var', 'Какие цели встреч, кому интересно и т.д.');?></span>
            <textarea cols="71" rows="10" name="description"></textarea>
        </div>

        <div class="break"></div>
        <a id="step4_b" class="button button-blue step_b">
            <span><?php echo Yii::t('var', 'Далее');?></span>
        </a>
    </div>

    <div class="step" id="step6">
        <?php if ($user_id==0) { ?>
        <h2 class="b-create-group__caption"><?php echo Yii::t('var', 'Шаг 5: Зарегистрируйтесь или войдите в систему.');?></h2>

        <div class="sep"></div>
        <?php if (Yii::app()->user->isGuest) { ?>
        <div class="item">
            <div id="log" style="display: none">
                <div style="float: right;">
                    <?php
                    $this->widget(
                        'ext.eauth.EAuthWidget',
                        array('action' => 'site/login', )//'popup' => true
                    );
                    ?>
                </div>
                <table>
                    <tbody><tr>
                        <td class="info" colspan="2"><?php echo Yii::t('var', 'Вход в систему. Ещё не зарегистрированы?');?>
                            <a id="reg_b"><?php echo Yii::t('var', 'Создать аккаунт!');?></a>
                        </td>
                    </tr>
                    <tr>
                        <td width="90"><?php echo Yii::t('var', 'Имя');?></td>
                        <td><input type="text" name="username"></td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Пароль');?></td>
                        <td><input type="password" name="password"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="checkbox"><?php echo Yii::t('var', 'Запомнить меня');?></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
							<!--<input type="button" value="<?php //echo Yii::t('var', 'Войти');?>" onclick="authorization()" style="cursor:pointer">-->
							<a class="button button-blue" style="display: inline-block;" onclick="authorization()">
								<span><?php echo Yii::t('var', 'Войти');?></span>
							</a>
						</td>
                    </tr>
                    </tbody></table>
                </div><!-- #log-->
            <div id="reg">
                <table>
                    <tbody><tr>
                        <td class="info" colspan="2"><?php echo Yii::t('var', 'Регистрация в системе. Уже зарегистрированы?');?>
                            <a id="log_b"><?php echo Yii::t('var', 'Войти');?></a></td>
                    </tr>
                    <tr>
                        <td width="90"><?php echo Yii::t('var', 'Имя');?></td>
                        <td><input type="text" name="username"></td>
                    </tr>
                    <tr>
                        <td width="90"><?php echo Yii::t('var', 'Email');?></td>
                        <td><input type="text" name="email"></td>
                    </tr>
                    <tr>
                        <td><?php echo Yii::t('var', 'Пароль');?></td>
                        <td><input type="password" class="error_input" name="password"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><!--<input class="regFromCreate" type="button" value="<?php //echo Yii::t('var', 'Регистрация');?>" onclick="registration()" style="cursor:pointer">-->
						
						<a class="button button-blue" style="display: inline-block;" onclick="registration()">
							<span><?php echo Yii::t('var', 'Регистрация');?></span>
						</a>
						</td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- #reg-->
        </div>
        <?php } ?>
        <div class="break"></div>
        <?php
        }
        ?>
		<input type="hidden" name="YII_CSRF_TOKEN" value="<?php echo Yii::app()->request->csrfToken; ?>" />
        <div id="create-button">
            <?php if ($user_id > 0) { ?>
                <a class="button button-blue">
                    <span class="create-group" style="margin-top: 0px;"><?php echo Yii::t('var', 'Создать группу'); ?></span>
                </a>
            <? } ?>
        </div>
    </div>
</form>
</div>
<script>
    $(document).ready(function(){
        $('.auth-link').click(function(){
            $('#create-button').html('<a class="button button-blue"><span class="create-group" style="margin-top: 0px;"><? echo Yii::t('var', 'Создать группу'); ?></span></a>');
            $('#reg, #log').delay(1000).html('');
        });
    });
    function registration(){
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('user/registration');?>',
            data: 'name='+$('#reg input[name=username]').val()+'&password='+$('#reg input[name=password]').val()+'&email='+$('#reg input[name=email]').val(),
            success: function(data){
                $('#create-button').html(data);
                if (data != '')
                $('div#reg').html('');
            }
        });
    }

    function authorization(){
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl('user/authorization');?>',
            data: 'name='+$('#log input[name=username]').val()+'&password='+$('#log input[name=password]').val(),
            success: function(data){
                $('#create-button').html(data);
                if (data != '')
                    $('div#log').html('');
            }
        });
    }

    window.onload = function() {
        /* $(document).on('click','span.create-group',function(){
            $.ajax({
                type: 'GET',
                url: $('#create-group-form').attr('action'),
                data: $('#create-group-form').serialize()<?php if ($user_id>0) echo "+'&user_id=".(int)$user_id."'";?>,
             });
        }); */
        $(document).on('click','span.create-group',function(){
            $('#create-group-form').submit();
        });

        $(document).on('click','.del-item',function(){
            var keys = $('input[name=keywords]').val();
            var arr_key = keys.split(',');
            var i;
            var arr_keys = [];
            for (i=0;i<arr_key.length;i++) {
                if (arr_key[i]==$(this).attr('key'))
                    delete arr_key[i];
            }
            var j = 0;
            for (i=0;i<arr_key.length;i++) {
                if (arr_key[i]!=undefined) {
                    arr_keys[j] = arr_key[i];
                    j++;
                }
            }
            $(this).parent().fadeOut();
            $('input[name=keywords]').val(arr_keys.join(','));
        });


        $('#keywords').on('click','.keyword-item',
            function(){
                var text = $('.keywords').html();
                if (text == '') {
                    $('.keywords').html($('.keywords').html()+'<div><div>'+$(this).text()+'</div>'+'<div class="del-item" key="'+$(this).attr('kid')+'">&nbsp[x]</div></div>');
                    $('input[name=keywords]').val($('input[name=keywords]').val()+$(this).attr('kid'));
                } else {
                    $('.keywords').html($('.keywords').html()+'<div><div>'+$(this).text()+'</div>'+'<div class="del-item" key="'+$(this).attr('kid')+'">&nbsp[x]</div></div>');//'<div>,&nbsp&nbsp</div>'+
                    $('input[name=keywords]').val($('input[name=keywords]').val()+','+$(this).attr('kid'));
                }
                $(this).fadeOut();
            });

        $('#loginza_auth_form').click(function(){
            alert("dsads");
        });

        $(".drop_down").chosen();
        $('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
        $(document).on('click', '.step_b', function(){
            var nextStep = $(this).parent('.step').next('.step');
            nextStep.show();
            //$("#lean_overlay").fadeOut(200);
            $(this).hide();
            var targetOffset = nextStep.offset().top;
            $('html,body').animate({scrollTop: targetOffset}, 1000);
        });

        $("#reg_b").click(function(){
            $("#log").hide();
            $("#reg").show();
        });

        $("#log_b").click(function(){
            $("#reg").hide();
            $("#log").show();
        });

        $(".anystring").keyup(function(){
            var val = $(this).val();
            if(val.length > 2)
            {
                $(this).removeClass("error_input");
                $(this).addClass("good_input");
            }
            else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }
        });

        $(".numstring").keyup(function(){
            var val = $(this).val();
            if(val.length > 6)
            {
                if(isStrinValid(val))
                {
                    $(this).removeClass("error_input");
                    $(this).addClass("good_input");
                }
                else {
                    $(this).removeClass("good_input");
                    $(this).addClass("error_input");
                }
            }
            else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }
        });

        function isStrinValid(string)
        {
            var pattern = new RegExp(/^[A-Za-z][A-Za-z0-9]+$/);
            return pattern.test(string);
        }

        $(".email").keyup(function(){
            var email = $(this).val();
            if(email != 0)
            {
                if(isValidEmailAddress(email))
                {
                    $(this).removeClass("error_input");
                    $(this).addClass("good_input");
                }
                else {
                    $(this).removeClass("good_input");
                    $(this).addClass("error_input");
                }
            } else {
                $(this).removeClass("good_input");
                $(this).addClass("error_input");
            }
        });

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            return pattern.test(emailAddress);
        }

    };
</script>
