<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Group';
$this->breadcrumbs=array(
    'Group',
);
?>
<h1>Group</h1>
<?php

$criteria = new CDbCriteria(/*array('condition' => "ID=2",)*/);

$dataProvider = new CActiveDataProvider('Group', array('criteria' => $criteria));

foreach ($dataProvider->data as $data) {?>
    <div class="blue-rounded-reserved item">
        <small class="date-reserved"><?php echo Yii::t('main_template', 'Дата бронирования:') ?> <?php echo CHtml::encode($data->id); ?></small>
    </div>
<?php } ?>
