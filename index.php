<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(-1);
ini_set('display_errors', true);

// change the following paths if necessary
$yii=dirname(__FILE__).'/protected/vendors/yii/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

defined('YII_ENV') or define('YII_ENV', isset($_SERVER['YII_ENV']) ? $_SERVER['YII_ENV'] : 'dev');

/*production*/
if (YII_ENV === 'prod')
{
    $yii = dirname(__FILE__) . '/protected/vendors/yii/yiilite.php';
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 1);
    error_reporting(0);
}
else
{
    $yii = dirname(__FILE__) . '/protected/vendors/yii/yii.php';
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
	error_reporting(E_ALL & ~E_NOTICE);
}
require_once($yii);

//Yii::createWebApplication($config)->run();
$app = Yii::createWebApplication($config);
if (YII_DEBUG){
    Yii::import("application.components.FirePHPCore.fb", true); 
} 
$app->run();