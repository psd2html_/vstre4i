$(document).ready(function () {
    $( '.b-jui-datepicker' ).datepicker( { minDate: 0, maxDate: "+3Y" } );
    //.datepicker( $.datepicker.regional[ "ru" ])

    var hourInput = $('.b-meet-time-hour');
    hourInput.empty();
    var hours = '';
    for ( var i = 0; i < 24; i++ ) {
        var number =  i < 10 ? '0' + i : i;
        hours += '<option value="' + number + '">' + number + '</option>';
    }
    hourInput.append( hours );

    var minuteInput = $('.b-meet-time-minute');
    minuteInput.empty();
    var minutes = '';
    for ( i = 0; i < 60; i++ ) {
        number =  i < 10 ? '0' + i : i;
        minutes += '<option value="' + number + '">' + number + '</option>';
    }
    minuteInput.append( minutes );

});
