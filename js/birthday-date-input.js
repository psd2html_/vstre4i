$(document).ready(function () {
    var dateInputs = $( '.b-jui-datepicker' ).datepicker({
        minDate: "-99Y",
        maxDate: "-9Y",
        changeMonth: true,
        changeYear: true,
        yearRange: "-99Y:-9Y",
        defaultDate: "-20Y"
    });
    $.each(dateInputs, function(index, input){
        var inputValue = $(input).val();
        if (!inputValue.match(/\d\d\d\d-\d\d-\d\d/)
            || inputValue == '0000-00-00'
        ) {
            $(input).val('');
            return;
        }
        var defaultDate = $.datepicker.parseDate("yy-mm-dd", inputValue);
        var reformatedDate = $.datepicker.formatDate( "dd-mm-yy", defaultDate );
        $(input).val(reformatedDate);
    });
});
