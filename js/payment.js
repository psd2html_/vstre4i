jQuery(document).ready(function(){
    $('.payment-methods').on('change',function(){
        var method = $(this).find(":selected").attr('value');
        $('.payment-page-container').load('/group/GetPaymentTemplate?name='+method+'&group_id='+group_id,function(){
            $('#FIELD_GROUP_ID').attr('value',group_id);
        });
    });
    $('#activate-group').on('click',function(){
        $('.payment-page-container').load('/group/activateGroup?code='+$('#confirmation-code').attr('value')+'&id='+group_id);
        return false;
    });
    try{

    CKEDITOR.replace( 'Page_text');
    } catch (e){

    }

    try{

        CKEDITOR.replace( 'Mail_text');
    } catch (e){

    }
    $(document).on("click", function() {
        $('.email-form').hide();
    });

    $('.email-form').on("click", function(evt) {
        evt.stopPropagation();
    });

});