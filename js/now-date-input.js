$(document).ready(function () {
    var dateInputs = $( '.b-jui-datepicker' ).datepicker({
        minDate: "d",
        maxDate: "+366d",
        changeMonth: true,
        changeYear: true,
        yearRange: "d:+1Y",
        defaultDate: "d"
    });
    $.each(dateInputs, function(index, input){
        var inputValue = $(input).val();
        if (!inputValue.match(/\d\d\d\d-\d\d-\d\d/)
            || inputValue == '0000-00-00'
        ) {
            $(input).val('');
            return;
        }
        var defaultDate = $.datepicker.parseDate("yy-mm-dd", inputValue);
        var reformatedDate = $.datepicker.formatDate( "dd-mm-yy", defaultDate );
        $(input).val(reformatedDate);
    });
});
