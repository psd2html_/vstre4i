    <?
    //$obj = new vk('login', 'pass');
    //echo $obj->request_api('wall.post', array('owner_id' => '-16690000', 'message' => 'Ололо'))."\n";
    //$obj->wall_post('message', '100500')."\n";
    //$obj->messages_send('message', '100500')."\n";
    //$obj->friends_add('178297985')."\n";
     
    class vk{
     
            var $cookie;
            var $access_token;
            var $uid;
           
            ### Авторизация ###
            function __construct($login, $pass){
                    ### Обычная авторизация ###
                    $auth = $this->curl('http://login.vk.com/?act=login&email='.$login.'&pass='.$pass);
                    if(preg_match('/hash=([a-z0-9]{1,32})/', $auth, $hash)){
                    $auth = $this->curl('http://vk.com/login.php?act=slogin&fast=1&hash='.$hash[1].'&s=1');
                    preg_match('/remixsid=(.*?);/', $auth, $sid);
                    $this->cookie = 'remixsid='.$sid[1];
                   
                    ### API-авторизация ###
                    $auth = $this->curl('https://oauth.vk.com/token?grant_type=password&client_id=2274003&client_secret=hHbZxrka2uZ6jB1inYsH&scope=8192&username='.$login.'&password='.$pass);
                    preg_match('/{"access_token":"(.*?)","expires_in":(.*?),"user_id":(.*?)}/', $auth, $data);
                    $this->access_token = $data[1];
                    $this->uid = $data[3];
                    } else {
                            echo 'Authorization failed';
                            exit;
                    }
            }
           
            ### Метод для запросов к API ###
            function request_api($method, $parameters /* array */){
                    foreach($parameters as $key => $value){
                            $params[] = $key.'='.$value;
                    }
                    $params = implode('&', $params);
                   
                    $res = $this->curl('https://api.vk.com/method/'.$method.'?'.$params.'&access_token='.$this->access_token);
                    return $res;
            }
           
            ### Метод для постинга на стену ###
            function wall_post($message, $id){
                    if(!isset($id)){
                            $id = $this->uid;
                    }
                   
                    $res = $this->curl('http://vk.com/wall'.$id, $this->cookie);
                    if(preg_match('/"post_hash":"(.*?)"/', $res, $hash)){
                            $postdata = array(
                                    'act' => 'post',
                                    'al' => 1,
                                    'facebook_export' => '',
                                    'fixed' => '', 
                                    'friends_only' => '',
                                    'from' => '',  
                                    'hash' => $hash[1],
                                    'message' => $message,
                                    'official' => '',
                                    'signed'  => '',
                                    'status_export' => '',
                                    'to_id' => $id,
                                    'type' => 'all'
                            );
                            $res = $this->curl('http://vk.com/al_wall.php', $this->cookie, $postdata);
                            preg_match('/"post(.*?)"/', $res, $post_id);
                            return $post_id[1];
                    }
            }
           
            ### Метод для отправки сообщения ###
            function messages_send($message, $id){
                    if(!isset($id)){
                            $id = $this->uid;
                    }
                   
                    $res = $this->curl('http://m.vk.com/write'.$id, $this->cookie);
                    if(preg_match('/action="(.*?)"/', $res, $action)){
                            $postdata = array(
                                    '_ajax' => 1,
                                    'message' => $message
                            );
                            $res = $this->curl('http://m.vk.com'.$action[1], $this->cookie, $postdata);
                            return res;
                    }
            }
           
            ### Метод для добавления в друзья ###
            function friends_add($id){
                    $res = $this->curl('http://vk.com/id'.$id, $this->cookie);
                    if(preg_match("/return Profile.toggleFriend\(this, '(.*?)', 1, event\)/", $res, $hash)){
                            $postdata = array(
                                    'act' => 'add',
                                    'al' => 1,
                                    'from' => 'profile',
                                    'hash' => $hash[1],
                                    'mid' => $id
                            );
                            $res = $this->curl('http://vk.com/al_friends.php', $this->cookie, $postdata);
                            return $res;
                    }
            }
           
            ### cUrl ###
            function curl($url, $cookie = null, $post = null){
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                    curl_setopt($ch, CURLOPT_HEADER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    if(isset($cookie)){
                            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
                    }
                    if(isset($post)){
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    }
                   
                    $response = curl_exec($ch);
                    curl_close($ch);
                    return $response;
            }
    }
    ?>